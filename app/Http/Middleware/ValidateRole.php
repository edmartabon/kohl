<?php

namespace App\Http\Middleware;

use Config;
use Closure;
use Sentinel;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\Account;

class ValidateRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$args)
    {
        if (Sentinel::check()) {
            $user = Sentinel::getUser();
            $sentinel = Sentinel::findById($user->id);
            
            if (!Sentinel::hasAnyAccess($args)) {
                return $this->requestHandler(new Response(view('Vetlify::pages.errors.401')), $request->ajax());
            }
            return $next($request);
        }
        return $this->requestHandler(redirect(Config::get('vetlify.app_route_prefix').'/login'), $request->ajax());
    }

    /**
     * Check if request is ajax
     *
     * @param mixed $options
     * @return bool $isAjax
     * @return mixed
     */
    private function requestHandler($option, $isAjax)
    {
        if ($isAjax) {
            return Response::json(array(
                'code'    =>  401,
                'message' =>  'Unauthorized'
            ), 401);
        }
        return $option;
    }
}
