<?php

return [

    'page_title' => 'Vetlify',

    'app_route_prefix' => 'app',

    'main_sub_url' => env('VETLIFY_SUB_PAGE_URL', null),

    'main_page_url' => env('VETLIFY_MAIN_PAGE_URL', null),

    'base_image_url' => env('VETLIFY_BASE_PAGE_URL', null),

    'aws_public_url' => env('VETLIFY_PUBLIC_AWS_URL', null),

    'DISPLAY_ERROR_REFRESH_MODAL' => 'F32FFM203'
    
];
