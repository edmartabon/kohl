jQuery(window).on('load', function(){
    $('.grid').masonry({
      itemSelector: '.grid-item',
    });

    $(".intro-container").backgroundCycle({
        imageUrls: [
            '../images/intro-bg-retina-desktop.jpg',
            '../images/intro-bg-retina-desktop2.jpg',
            '../images/intro-bg-retina-desktop3.jpg'
        ],
        fadeSpeed: 2000,
        duration: 5000,
        backgroundSize: SCALING_MODE_COVER
    });

    $(document).on('mouseover', '.feature-1, .feature-2, .feature-3', function(){
        $(this).children('.media-left').addClass('animated shake');
    });

    $(document).on('mouseout', '.feature-1, .feature-2, .feature-3', function(){
        $(this).children('.media-left').removeClass('animated shake');
    });

    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        nav: true,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            },
            640: {
                items: 2,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            },
            768: {
                items: 2,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            },
            1024:{
                items:2,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            },
            1920:{
                items:3,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            },
            2560:{
                items:5,
                nav:true,
                navText: ['<i class="icon-android-arrow-back"></i>', '<i class="icon-android-arrow-forward"></i>'],
                loop: true
            }
        }
    });

    // Send e-mail message
    $(document).on('click', '.send-btn', function(e){
        e.preventDefault();
        $.ajax({
           type: "POST",
           url: "send-mail",
           dataType: "json",
           data: $('#email-form').serialize(),
           success: function (response) {
                $('.alert').remove();
                if(response['0'] == 'success') {
                    $('#email-form').before('<div class="alert alert-info" role="alert"><p><i class="icon-check-1"></i>Your message was sent!</p></div>');
                    $('input[type=text], input[type=email], textarea').val('');
                    $('.name').focus();
                } else {
                    var x = '<div class="alert alert-danger" role="alert">';
                       x += '<h3>Oops, something went wrong.</h3>';
                       $.each( response, function( key, value ) {
                          x += '<p> <i class="icon-stop"></i> ' + value + '</p>'; 
                       });
                    x += '</div>';
                    $('#email-form').before(x);
                }
           }
       });
    });

    // Mobile Nav Animation
    $(document).on('click', '.hamburger--slider', function(){
        $('.header-nav').stop().animate({
            left: '0'
        }, 300, 'easeInOutCubic');
        $(this).addClass('is-active');
    });

    $(document).on('click', '.is-active', function(){
        $('.header-nav').stop().animate({
            left: '-250px'
        }, 300, 'easeInOutCubic');
        $(this).removeClass('is-active');
    });
    
    var introDiv      = jQuery('#intro').offset().top;
    var whyDiv        = jQuery('#why').offset().top;
    var reportDiv     = jQuery('#reporting').offset().top;
    var screenshotDiv = jQuery('.screenshots-wrapper').offset().top;
    var contactDiv    = jQuery('#contact').offset().top;

    jQuery('#why-link').click(function() {

        jQuery('html, body').animate({
            scrollTop: whyDiv
        }, 'slow');
    });

    jQuery('#intro-link').click(function() {
        // Scroll down to 'catTopPosition'
        jQuery('html, body').animate({
            scrollTop: introDiv
        }, 'slow');
    });

    jQuery('#reporting-link').click(function() {
        jQuery('html, body').animate({
            scrollTop: reportDiv
        }, 'slow');
    });

    jQuery('#screenshot-link').click(function() {
        jQuery('html, body').animate({
            scrollTop: screenshotDiv
        }, 'slow');
    });

    jQuery('.contact-link').click(function() {
        jQuery('html, body').animate({
            scrollTop: contactDiv
        }, 'slow');
    });

    jQuery('#top').click(function() {
        jQuery('html, body').animate({
            scrollTop: introDiv
        }, 'slow');
    });

    function skew_position() {
        var window_width = jQuery(window).width();
        var skew_square_height = window_width / 18.5;
        jQuery(".skew-appended").height(skew_square_height + "px").css("bottom", "-" + skew_square_height / 2 + "px").css("-webkit-backface-visibility", "hidden !important");
        jQuery(".skew-prepended").height(skew_square_height + "px").css("top", "-" + (skew_square_height / 2 + 1) + "px").css("-webkit-backface-visibility", "hidden !important");
    }

    skew_position();

    var windos = $(window);

    windos.resize(function() {
        skew_position();
    });

    var intro_waypoint = new Waypoint({
        element: document.getElementById('introducing'),
        handler: function() {
            $('#main-nav ul li a').removeClass('current');
            $('#main-nav ul li a#intro-link').addClass('current');
        }
    });

    var why_waypoint = new Waypoint({
        element: document.getElementById('why'),
        handler: function() {
            $('#main-nav ul li a').removeClass('current');
            $('#main-nav ul li a#why-link').addClass('current');
        }
    });

    var report_waypoint = new Waypoint({
        element: document.getElementById('reporting'),
        handler: function() {
            $('#main-nav ul li a').removeClass('current');
            $('#main-nav ul li a#reporting-link').addClass('current');
        }
    });

    var screenshot_waypoint = new Waypoint({
        element: document.getElementById('screenshots'),
        handler: function() {
            $('#main-nav ul li a').removeClass('current');
            $('#main-nav ul li a#screenshot-link').addClass('current');
        }
    });

    var contact_waypoint = new Waypoint({
        element: document.getElementById('contact'),
        handler: function() {
            $('#main-nav ul li a').removeClass('current');
            $('#main-nav ul li a#contact-link').addClass('current');
        }
    });

    // Show back to top
    var back_top = new Waypoint({
        element: document.getElementById('three-col-container'),
        handler: function() {
            $('#top').fadeIn();
        }
    });

    // Hide back-to-top link when on top of the page.
    var hide_back_top = new Waypoint({
        element: document.getElementById('introducing'),
        handler: function() {
            $('#top').fadeOut();
        }
    });

    window.sr = ScrollReveal({ reset: false, mobile: false });

    sr.reveal('#introducing, .p-intro, #intro img', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 200,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-btn', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.signup-link', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-1 i', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-1 .media-right', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-2 i', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 800,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-2 .media-right', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1000,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-3 i', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1200,
            useDelay: 'always',
        },
    );

    sr.reveal('.feature-3 .media-right', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1400,
            useDelay: 'always',
        },
    );

    sr.reveal('.why header h2', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.why header h2 + hr', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.why header p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.why .features', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 800,
            useDelay: 'always',
        },
    );

    sr.reveal('.reporting-contents h2, .reporting-contents h2 + hr', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.reporting-contents hr + p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.reporting-contents .image-holder img', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 800,
            useDelay: 'always',
        },
    );

    sr.reveal('.reporting-contents .report-summary', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1000,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper h2', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper h2 + hr', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper hr + p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-1', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 800,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-2', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1000,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-3', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1200,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-4', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1400,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-5', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1600,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-6', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1800,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-7', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 2000,
            useDelay: 'always',
        },
    );

    sr.reveal('.screenshots-wrapper .grid-8', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 2200,
            useDelay: 'always',
        },
    );

    sr.reveal('#contact h2, #contact h2 + hr', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('#contact hr + p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-wrapper h4, .contact-wrapper h4 + p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 400,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-wrapper .media-middle', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 600,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-wrapper #email-form', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 800,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-infos h4, .contact-infos h4 + p', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1600,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-infos .location-block', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 1800,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-infos .email-block', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 2000,
            useDelay: 'always',
        },
    );

    sr.reveal('.contact-infos .phone-block', 
        { 
            duration: 400,
            origin: 'top',
            easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
            distance: '100px',
            delay: 2200,
            useDelay: 'always',
        },
    );

    $(document).ready(function() {
        $('.grid-item a').magnificPopup(
            { 
                type:'image',
                gallery: {
                  enabled: true,

                  preload: [0,2],
                  navigateByImgClick: true,
                  arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',

                  tPrev: 'Previous (Left arrow key)',
                  tNext: 'Next (Right arrow key)',
                  tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
                },
                zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                opener: function(openerElement) {
                  return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
  }
            }
        );
    });
});