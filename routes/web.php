<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/assets', function () {
    $path = 'clients/vet/client/avatar/original/958e9978-56c2-49f1-9056-c4ed9a8b472e.jpg';
    $filePath = Storage::disk('spaces')->getDriver()->getAdapter()->applyPathPrefix($path);
    return response()->file($filePath);
});
