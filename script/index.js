import dotenv from 'dotenv'
import Notification from './services/notification'

dotenv.config({ path: '../.env' })

Notification.init()
