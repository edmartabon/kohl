import { Client } from 'pg'

module.exports = {
  
  adminConnection() { 
    const client = new Client({
      user: process.env.DB_USERNAME,
      host: process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      password: process.env.DB_PASSWORD,
      port: process.env.DB_PORT,
    })
    
    client.connect()
    return client    
  },

  clientConnection(credential) {
    const client = new Client(credential)
    client.connect()
    return client
  },

  getAccount(conn) {
    return conn.query(`
      SELECT 
      t2.host,
      t1.name AS database,
      t2.port
      FROM accounts AS t1
      LEFT JOIN databases AS t2 ON t1.database_id = t2.id
    `)
  },

  getAppointment(conn, dateNow) {
    return conn.query(`
      SELECT 
      t1.id,
      t2.name,
      t3.first_name,
      t3.last_name,
      t1.phone_no,
      t1.notify_at
      FROM appointments AS t1
      LEFT JOIN branches AS t2 ON t1.branch_id = t2.id
      LEFT JOIN patients AS t3 ON t1.patient_id = t3.id
      WHERE t1.notify_at::date = date '${dateNow}' AND t1.appointment_status_id = 1
    `)
  },

  updateAppointment(conn, data) {
    return conn.query(`
      UPDATE appointments SET appointment_status_id = 2
      WHERE appointments.id = ${data.id}
    `);
  }

  

}