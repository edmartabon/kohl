import axios from 'axios'
import Moment from 'moment'
import Schedule from 'node-schedule'
import Database from './database'

const Notification = {

  async init() {
    const database = Database.adminConnection()
    const accounts = await Database.getAccount(database)
    const compareDate = Moment().add(6, 'hours').format('YYYY-MM-DD')
    const conns = []

    for (let account of accounts.rows) {
      const accountInfo = Object.assign({
        user: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
      }, account)

      let conn = Database.clientConnection(accountInfo)
      conns.push(conn)
    }

    const connsList = await Promise.all(conns)

    for(let conn of connsList) {
      Database.getAppointment(conn, compareDate)
        .then(data => {
          for(let row of data.rows) {
            Notification.runJob(conn, row)
          }
        })
    }
  },

  runJob(conn, row) {
    const appointment = Moment(row.notify_at).subtract(8, 'hours').format()
    Schedule.scheduleJob(appointment, function () {
      axios.post(`${process.env.SMS_PROVIDER}/api/v4/messages`, {
        apikey: process.env.SMS_PROVIDER_TOKEN,
        number: row.phone_no,
        message: 'Hello baby',
        sendername: 'Vetlify'
      })
        .then(data => Database.updateAppointment(conn, row))
        .catch(err => console.log('testset'))     
    })
  }

}

module.exports = Notification