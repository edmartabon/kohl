<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'server', 'database_id', 'account_type_id', 'account_status_id', 'expired_at'];

    /**
     * Set the account's server name.
     *
     * @param  string  $value
     * @return void
     */
    public function setServerAttribute($value)
    {
        $this->attributes['server'] = strtolower($value);
    }
}
