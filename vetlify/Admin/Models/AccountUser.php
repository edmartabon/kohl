<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AccountUser extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['email', 'account_id'];
}
