<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class SpamEntry extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spam_entries';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['ip_addr', 'spam_type_id', 'expired_at'];
}
