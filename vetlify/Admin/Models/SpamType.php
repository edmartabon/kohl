<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class SpamType extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
