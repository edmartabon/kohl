<?php

namespace Vetlify\Admin\Models;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends EloquentUser
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'first_name', 'last_name', 'address_one',
        'address_two', 'phone_no', 'telephone_no', 'avatar', 'branch_id', 'user_status_id'
    ];

}
