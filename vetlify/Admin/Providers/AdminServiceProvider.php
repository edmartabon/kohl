<?php

namespace Vetlify\Admin\Providers;

use Route;
use Config;
use Sentinel;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Sentinel::setModel('Vetlify\Admin\Models\User');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
