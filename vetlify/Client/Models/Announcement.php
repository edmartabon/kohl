<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['title', 'address', 'body', 'start_at', 'end_at', 'active'];

}
