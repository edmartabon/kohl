<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['phone_no', 'email', 'description', 'veterinarian_id', 'appointment_status_id', 'patient_id', 'branch_id', 'notify_at', 'start_at', 'end_at'];
}
