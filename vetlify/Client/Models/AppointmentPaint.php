<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentPaint extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['branch_id', 'color'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['branch_id'];
}
