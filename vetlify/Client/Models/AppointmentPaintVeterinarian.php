<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentPaintVeterinarian extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['user_id', 'appointment_paint_id'];
}
