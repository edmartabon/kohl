<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentStatus extends Model
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'appointment_status';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'color'];
}
