<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class BillingType extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
