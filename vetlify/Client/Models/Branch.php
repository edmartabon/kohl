<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'address_one', 'address_two', 'contact_no', 'phone_no', 'is_primary', 'logo', 'working_start_day', 'working_end_day', 'working_start_at', 'working_end_at'];
}
