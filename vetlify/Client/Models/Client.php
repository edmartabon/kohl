<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'patient_ids' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['first_name', 'last_name', 'phone_no', 'email', 'telephone_no', 'patient_ids', 'patient_gender_id', 'address_one', 'address_two', 'rfid', 'avatar', 'branch_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['branch_id'];

    /**
     * Get the client's patient_id
     *
     * @return array
     */
    public function getPatientIdsAttribute()
    {
        $patientId = $this->attributes['patient_ids'];
        $item = str_replace(['{', '}'], '', $patientId);

        if ($item == '') {
            return [];
        }
        return explode(',', $item);
    }

    /**
     * Set the client's patient_id
     *
     * @return string
     */
    public function setPatientIdsAttribute($value)
    {
        $this->attributes['patient_ids'] = gettype($value) == 'string' ? '{}' : '{'.join(',', $value).'}';
    }
}
