<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['patient_record_id', 'first_name', 'last_name', 'email', 'phone_no', 'telephone_no', 'address_one', 'address_two', 'rfid', 'avatar', 'color', 'weight', 'markings', 'species_id', 'breed_id', 'branch_id', 'patient_gender_id', 'patient_blood_id', 'birth_date', 'client_id', 'weight_unit', 'age'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['branch_id'];
}
