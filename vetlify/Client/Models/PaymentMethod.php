<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
