<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Procedure extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['compliants', 'investigations', 'diagnoses', 'notes', 'image_mappings', 'recommendations', 'prescriptions', 'laboratories', 'type_animal', 'veterinarian', 'patient_id', 'branch_id'];

    /**
     * Set the compliant array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setCompliantsAttribute($value)
    {
        $this->attributes['compliants'] = json_encode($value);
    }

    /**
     * Get the compliant array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getCompliantsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setInvestigationsAttribute($value)
    {
        $this->attributes['investigations'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getInvestigationsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setDiagnosesAttribute($value)
    {
        $this->attributes['diagnoses'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getDiagnosesAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setNotesAttribute($value)
    {
        $this->attributes['notes'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getNotesAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setPrescriptionsAttribute($value)
    {
        $this->attributes['prescriptions'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getPrescriptionsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setRecommendationsAttribute($value)
    {
        $this->attributes['recommendations'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getRecommendationsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setLaboratoriesAttribute($value)
    {
        $this->attributes['laboratories'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getLaboratoriesAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the investigations array string.
     *
     * @param  string  $value
     * @return void
     */
    public function setImageMappingsAttribute($value)
    {
        $this->attributes['image_mappings'] = json_encode($value);
    }

    /**
     * Get the investigations array string.
     *
     * @param  string  $value
     * @return string
     */
    public function getImageMappingsAttribute($value)
    {
        return json_decode($value);
    }
}
