<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Species extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'species';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
