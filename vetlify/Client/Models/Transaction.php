<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['reference_no', 'patient_id', 'veterinarian', 'user_id', 'branch_id'];
}
