<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['price', 'quantity', 'discount', 'tax', 'product_id', 'transaction_id'];
}
