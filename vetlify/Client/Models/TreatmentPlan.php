<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentPlan extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['reference_no', 'patient_id', 'user_id', 'branch_id', 'transaction_id'];
}
