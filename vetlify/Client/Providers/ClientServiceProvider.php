<?php

namespace Vetlify\Client\Providers;

use Illuminate\Support\ServiceProvider;
use Vetlify\Client\Services\Appointment;

class ClientServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindAppointment();
        $this->bindAppointmentContract();
    }

    /**
     * Bind the appointment service
     *
     * @return void
     */
    private function bindAppointment()
    {
        $appointment = new Appointment;
        $this->app->bind('Appointment', function () use ($appointment) {
            return $appointment;
        });
    }

    /**
     * Bind the appointment contract
     *
     * @return void
     */
    private function bindAppointmentContract()
    {
        $this->app->bind('Vetlify\Client\Contracts\AppointmentContract', 'Vetlify\Client\Services\Appointment');
    }
}
