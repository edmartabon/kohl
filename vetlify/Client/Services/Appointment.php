<?php

namespace Vetlify\Client\Services;

use DB;
use Vetlify\Client\Models\Appointment as AppointmentModel;
use Vetlify\Client\Contracts\AppointmentContract;

class Appointment implements AppointmentContract
{
    public function show($params = [])
    {
        $appointment = AppointmentModel::leftJoin('users', 'users.id', '=', 'appointments.veterinarian_id')
            ->leftJoin('patients', 'patients.id', '=', 'appointments.patient_id')
            ->leftJoin('appointment_status', 'appointments.appointment_status_id', '=', 'appointment_status.id')
            ->where('appointments.branch_id', '=', $params['branch_id']);

        if (isset($params['start']) && isset($params['end'])) {
            $appointment = $appointment
                ->where('appointments.start_at', '>=', $params['start'])
                ->where('appointments.start_at', '<=', $params['end'].' 23:59:59');
        }

        if (isset($params['patient_id'])) {
            $appointment = $appointment
                ->where('appointments.patient_id', '=', $params['patient_id']);
        }
        
        return $appointment->select(
            'appointments.*',
            'patients.first_name',
            'patients.last_name',
            'patients.avatar',
            'appointment_status.color',
            DB::raw('INITCAP(users.first_name || \' \' || users.last_name) AS title'),
            DB::raw('(TO_CHAR(appointments.start_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS start'),
            DB::raw('(TO_CHAR(appointments.end_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS end')
        )
        ->get();
    }
}
