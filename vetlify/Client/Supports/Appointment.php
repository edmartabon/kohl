<?php

namespace Vetlify\Client\Supports;

use Illuminate\Support\Facades\Facade;

class Appointment extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Appointment';
    }
}
