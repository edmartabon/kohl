<?php

namespace Vetlify\Database\Consoles;

use Illuminate\Console\Command;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyCreateAdmin extends Command
{
    
    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vetlify:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add admin database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    public function handle(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->createAdminDatabase();
        $vetlifyDatabase->migrateAdminMigration();
    }
}
