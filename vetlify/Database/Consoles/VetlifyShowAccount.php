<?php

namespace Vetlify\Database\Consoles;

use Illuminate\Console\Command;
use Vetlify\Admin\Models\Account;
use Vetlify\Admin\Models\AccountUser;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyShowAccount extends Command
{
    
    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vetlify:account 
        { --type= : Type of an action ( show, delete --name= ) }
        { --name= : Name of an account }
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add admin database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    public function handle(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $acceptType = ['show', 'delete'];

        if (!is_null($this->option('type')) && in_array($this->option('type'), $acceptType)) {
            $method = $this->option('type');
            if ($this->option('type') == 'delete') {
                return $this->$method($this->option('name'));
            }
            $this->$method();
        }
    }

    /**
     * Show all the list of accounts
     *
     * @return void
     */
    private function show()
    {
        $account = Account::select('id', 'name', 'server')
            ->get();

        $this->table(['ID', 'Name', 'Server'], $account);
    }

    /**
     * Delete the account
     *
     * @param void
     */
    private function delete($name)
    {
        $account = Account::where('server', $name)
            ->select('id', 'name', 'server')
            ->first();

        if ($account) {
            AccountUser::where('account_id', $account->id)
                ->delete();
            Account::where('id', $account->id)
                ->delete();
            \DB::statement("DROP DATABASE IF EXISTS ".$account->server);

            $this->info('Account '.$account->server.' has been deleted');
        }
    }
}
