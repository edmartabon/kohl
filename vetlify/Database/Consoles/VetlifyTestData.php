<?php

namespace Vetlify\Database\Consoles;

use Faker;
use Sentinel as UserSentinel;
use Illuminate\Console\Command;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Patient;
use Vetlify\Client\Models\Category;
use Vetlify\Client\Models\Supplier;
use Vetlify\Client\Models\Product;
use Vetlify\Client\Models\Compliant;
use Vetlify\Client\Models\Diagnose;
use Vetlify\Client\Models\Investigation;
use Vetlify\Client\Models\Note;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyTestData extends Command
{
    
    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vetlify:test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    public function handle(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $this->registerNewUser($vetlifyDatabase);
        $this->createFakerClientAndPatient($vetlifyDatabase);
        $this->createFakeInventory($vetlifyDatabase);
        $this->createFakeProcedures($vetlifyDatabase);

        $this->line('');
        $this->info('Finished...');
        $this->line('');
    }

    /**
     * Register new user for new test account
     *
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract
     * @return void
     */
    private function registerNewUser(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $branch = Branch::create([
            'name' => 'Bizwex',
            'address_one' => 'Balanga, Bataan',
            'is_primary' => 1
        ]);

        $user = UserSentinel::registerAndActivate([
            'email' 	 => 'bizwex@gmail.com',
            'password'   => '123456789',
            'first_name' => 'bizwex',
            'last_name'  => 'vetlify',
            'branch_id'  => $branch->id,
            'user_status_id' => 1
        ]);

        $adminRole = UserSentinel::findRoleByName('Admin');
        $adminRole->users()->attach($user);
    }

    /**
     * Create fake client and patient
     *
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract $vetlifyDatabaseContract
     * @return void
     */
    private function createFakerClientAndPatient(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $faker = Faker\Factory::create();
        $totalData = 1000;
        $this->info('Seeding Client and Patient...');
        $bar = $this->output->createProgressBar($totalData);

        for ($i=0;$i<$totalData;$i++) {
            $patient = Patient::create([
                'patient_record_id' => $faker->numberBetween(100000, 999999),
                'first_name' => $faker->name,
                'last_name' => $faker->lastName,
                'rfid' => $this->randomKey(20),
                'branch_id' => 1,
                'patient_blood_id' => 1,
                'birth_date' => date('Y-m-d H:i:s'),
                'patient_gender_id' => 1,
            ]);

            $client = Client::create([
                'first_name' => $faker->name,
                'last_name' => $faker->lastName,
                'address_one' => $faker->streetAddress,
                'phone_no' => $faker->phoneNumber,
                'telephone_no' => $faker->phoneNumber,
                'rfid' => $this->randomKey(20),
                'patient_ids' => [$patient->id],
                'branch_id' => 1,
                'patient_gender_id' => 1,
            ]);

            $bar->advance();
        }

        $bar->finish();
    }

    private function createFakeInventory(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $faker = Faker\Factory::create();
        $totalData = 1000;

        $this->line('');
        $this->info('Seeding Supplier...');
        $supplierProg = $this->output->createProgressBar($totalData);

        for ($i=0;$i<$totalData;$i++) {
            Supplier::create([
                'branch_id' => 1,
                'company_name' => $faker->company,
                'code' => $this->randomKey(10),
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'first_name' => $faker->name,
                'last_name' => $faker->lastName,
                'address' => $faker->streetAddress,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
            ]);
            $supplierProg->advance();
        }

        $this->line('');
        $this->info('Seeding Category...');
        $categoryProg = $this->output->createProgressBar(4);
        foreach (['Milk', 'Powder', 'Cologne', 'Eye Drop'] as $value) {
            Category::create([
                'branch_id' => 1,
                'name' => $value
            ]);
            $categoryProg->advance();
        }

        Category::create([
            'branch_id' => 1,
            'name' => 'System',
            'is_active' => 0
        ]);

        $this->line('');
        $this->info('Seeding Product...');
        $productProg = $this->output->createProgressBar($totalData);
        for ($i=0;$i<$totalData;$i++) {
            Product::create([
                'branch_id' => 1,
                'name' => $faker->company,
                'item_code' => $this->randomKey(10),
                'reorder_point' => rand(1, 100),
                'retail_price' => rand(1, 100),
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'inventory_count' => rand(1, 100),
                'category_id' => rand(1, 4),
            ]);
            $productProg->advance();
        }
    }

    /**
     * Create fake procedures information
     */
    private function createFakeProcedures(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $faker = Faker\Factory::create();
        $this->line('');
        $this->info('Seeding Supplier...');
        $bar = $this->output->createProgressBar(1000);

        for ($i=0;$i<1000;$i++) {
            Compliant::create(['name' => $faker->name]);
            $bar->advance();
        }

        $this->line('');
        $this->info('Seeding Diagnose...');
        $diagnoseBar = $this->output->createProgressBar(1000);

        for ($i=0;$i<1000;$i++) {
            Diagnose::create(['name' => $faker->name]);
            $diagnoseBar->advance();
        }

        $this->line('');
        $this->info('Seeding Diagnose...');
        $invetigationBar = $this->output->createProgressBar(1000);
        for ($i=0;$i<1000;$i++) {
            Investigation::create(['name' => $faker->name]);
            $invetigationBar->advance();
        }

        $this->line('');
        $this->info('Seeding Diagnose...');
        $noteBar = $this->output->createProgressBar(1000);
        for ($i=0;$i<1000;$i++) {
            Note::create(['name' => $faker->name]);
            $noteBar->advance();
        }
        
        $bar->finish();
    }

    /**
     * Generate ramdom key
     */
    private function randomKey($length)
    {
        $pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $key = '';
        for ($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }
}
