<?php

use Vetlify\Admin\Models\Database;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('databases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('host');
            $table->string('port');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        $this->seeder();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('databases');
    }

    /**
     * Seed latest Database
     *
     * @return void
     */
    private function seeder()
    {
        $databases = $this->databaseList();
        foreach ($databases as $database) {
            Database::create($database);
        }
    }
    
    /**
     * list of all database
     *
     * @return void
     */
    private function databaseList()
    {
        return [
            [
                'name' => 'main',
                'host' => '127.0.0.1',
                'port' => '5432'
            ]
        ];
    }
}
