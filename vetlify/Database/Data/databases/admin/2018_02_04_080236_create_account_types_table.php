<?php

use Vetlify\Admin\Models\AccountType;
use Vetlify\Admin\Models\AccountStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('limit_branch');
            $table->integer('limit_product');
            $table->integer('limit_transaction');
        });

        Schema::create('account_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_types');
        Schema::dropIfExists('account_status');
    }

    /**
     * Seed the account type and account status
     *
     * @return void
     */
    private function seed()
    {
        $this->seedAccountType();
        $this->seedAccountStatus();
    }

    /**
     * Seed account type
     *
     * @return void
     */
    private function seedAccountType()
    {
        $types = [
            [
                'name' => 'Free',
                'limit_branch' => 1,
                'limit_product' => 10,
                'limit_transaction' => 100,
            ],
            [
                'name' => 'Standard Package',
                'limit_branch' => 1,
                'limit_product' => 10000,
                'limit_transaction' => 10000,
            ],
            [
                'name' => 'Business Package',
                'limit_branch' => 99999,
                'limit_product' => 10000,
                'limit_transaction' => 10000,
            ]
        ];

        foreach ($types as $type) {
            AccountType::create($type);
        }
    }

    /**
     * Seed Account Status
     *
     * @return void
     */
    private function seedAccountStatus()
    {
        $statusList = [
            [ 'name' => 'enable' ],
            [ 'name' => 'disable' ],
        ];

        foreach ($statusList as $value) {
            AccountStatus::create($value);
        }
    }
}
