<?php

use Vetlify\Admin\Models\SpamType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spam_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('spam_entries', function (Blueprint $table) {
            $table->string('ip_addr');
            $table->integer('spam_type_id')->unsigned();
            $table->timestamp('expired_at')->nullable();

            $table->foreign('spam_type_id')->references('id')->on('spam_types');
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spam_entries');
        Schema::dropIfExists('spam_types');
    }

    /**
     * Create seed for the Spam Type
     *
     * @return void
     */
    private function seed()
    {
        SpamType::create([
            'name' => 'Account Registration'
        ]);
    }
}
