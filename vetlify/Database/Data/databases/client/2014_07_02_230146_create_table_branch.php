<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address_one')->nullable();
            $table->string('address_two')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('phone_no')->nullable();
            $table->boolean('is_primary')->default(0);
            $table->string('logo')->nullable();
            $table->string('working_start_day')->nullable();
            $table->string('working_end_day')->nullable();
            $table->timestamp('working_start_at')->nullable();
            $table->timestamp('working_end_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
