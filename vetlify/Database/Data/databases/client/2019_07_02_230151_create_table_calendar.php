<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
        });

        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_no');
            $table->string('email');
            $table->text('description')->nullable();
            $table->integer('appointment_status_id')->unsigned()->default(1);
            $table->integer('veterinarian_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->timestamp('notify_at');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('veterinarian_id')->references('id')->on('users');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('appointment_status_id')->references('id')->on('appointment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appointments');
    }
}
