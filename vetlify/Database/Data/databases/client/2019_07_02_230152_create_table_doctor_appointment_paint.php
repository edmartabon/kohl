<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoctorAppointmentPaint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_paints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id')->unsigned();
            $table->string('color');

            $table->foreign('branch_id')->references('id')->on('branches');
        });
        
        Schema::create('appointment_paints_veterinarian', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('appointment_paint_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('appointment_paint_id')->references('id')->on('appointment_paints');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appointment_paints');
    }
}
