<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_no');
            $table->integer('patient_id')->nullable()->unsigned();
            $table->integer('veterinarian')->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('veterinarian')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('branch_id')->references('id')->on('branches');
        });

        Schema::create('transaction_items', function (Blueprint $table) {
            $table->increments('id');
            $table->double('price')->default(0);
            $table->double('quantity')->default(0);
            $table->double('discount')->default(0);
            $table->double('tax')->default(0);
            $table->integer('product_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });

        Schema::create('billing_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reciept_no');
            $table->string('notes');
            $table->integer('patient_id')->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->integer('billing_type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('billing_type_id')->references('id')->on('billing_types');
            $table->foreign('branch_id')->references('id')->on('branches');
        });

        Schema::create('billing_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->integer('billing_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('billing_id')->references('id')->on('billings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_items');
        Schema::drop('billing_types');
        Schema::drop('billings');
        Schema::drop('billing_transactions');
        Schema::drop('transactions');
    }
}
