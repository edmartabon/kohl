<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProcedures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedures', function (Blueprint $table) {
            $table->increments('id');
            $table->text('compliants');
            $table->text('investigations');
            $table->text('diagnoses');
            $table->text('notes');
            $table->text('recommendations');
            $table->text('prescriptions');
            $table->text('laboratories');
            $table->text('image_mappings');
            $table->integer('veterinarian')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->integer('transaction_id')->nullable()->unsigned();
            $table->timestamp('appointment')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('veterinarian')->references('id')->on('users');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });

        Schema::create('compliants', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('investigations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('recommendations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('laboratories', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('prescriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('diagnoses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('procedures');
        Schema::drop('compliants');
        Schema::drop('investigations');
        Schema::drop('recommendations');
        Schema::drop('laboratories');
        Schema::drop('prescriptions');
        Schema::drop('investigations');
        Schema::drop('diagnoses');
        Schema::drop('notes');
    }
}
