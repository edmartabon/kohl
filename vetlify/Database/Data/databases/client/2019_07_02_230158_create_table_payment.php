<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cost')->nullable()->default(0);
            $table->double('tax')->nullable()->default(0);
            $table->double('discount')->nullable()->default(0);
            $table->integer('payment_method_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prescriptions');
    }
}
