<?php

namespace Vetlify\Database\Services;

use DB;
use App;
use Config;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyDatabase implements VetlifyDatabaseContract
{

    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * Set the default name of postgres database
     */
    private $dbDefaultDatabase = 'postgres';

    /**
     * Set the default admin database of vetlify
     */
    private $adminDatabase = 'vetlify';

    /**
     * Set the default config
     */
    private $defaultDatabaseConfig = [];

    /**
     * Configure the database connections
     *
     * @return void
     */
    public function __construct()
    {
        $this->dbDefaultDriver = Config::get('database.default');
        $this->defaultDatabaseConfig = Config::get('database.connections.'.$this->dbDefaultDriver);
        $this->createConnectionMain();
        $this->createConnectionClient();
    }

    /**
     * Create admin database
     *
     * @return void
     */
    public function createAdminDatabase()
    {
        if (!$this->checkAdminDatabaseExist()) {
            DB::connection($this->dbDefaultDatabase)
                ->select("CREATE DATABASE {$this->adminDatabase} WITH OWNER=postgres ENCODING='UTF-8'");

            $this->changeDefaultConfigConnection(['database' => $this->adminDatabase]);
            $this->migration();
        }
    }

    /**
     * Create admin database
     *
     * @return void
     */
    public function createClientDatabase($config)
    {
        $this->changeClientConfigConnection(array_merge($config, ['database' => 'postgres']));

        if (!$this->checkClientDatabaseExist($config['database'])) {
            DB::connection('client')->select("CREATE DATABASE ".$config['database']." WITH OWNER=postgres ENCODING='UTF-8'");
            $this->changeDefaultConfigConnection($config);
            $this->migration();
        }
    }

    /**
     * Migrate admin migration
     *
     * @return void
     */
    public function migrateAdminMigration()
    {
        $this->changeDefaultConfigConnection(['database' => $this->adminDatabase]);
        $this->migrator($this->basePath . 'Data/databases/admin/');
    }

    /**
     * Migrate client migration
     *
     * @return void
     */
    public function migrateClientMigration($config)
    {
        $this->changeDefaultConfigConnection($config);
        $this->migrator($this->basePath . 'Data/databases/client/');
    }

    /**
     * Create connection for main
     *
     * @return Void
     */
    private function createConnectionMain()
    {
        $connection = $this->defaultDatabaseConfig;
        $connection['database'] = $this->dbDefaultDatabase;

        Config::set('database.connections.'.$this->dbDefaultDatabase, $connection);
    }

    /**
     * Create connection for main
     *
     * @return Void
     */
    private function createConnectionClient()
    {
        $connection = $this->defaultDatabaseConfig;
        $connection['database'] = $this->dbDefaultDatabase;

        Config::set('database.connections.client', $connection);
    }

    /**
     * Create connection for admin
     *
     * @return Void
     */
    public function changeDefaultConfigConnection($config)
    {
        $dbConfig = array_merge($this->defaultDatabaseConfig, $config);
        Config::set('database.connections.'.$this->dbDefaultDriver, $dbConfig);
        DB::reconnect($this->dbDefaultDriver);
    }

    /**
     * Create connection for client
     *
     * @return Void
     */
    public function changeClientConfigConnection($config)
    {
        $dbConfig = array_merge($this->defaultDatabaseConfig, $config);
        Config::set('database.connections.client', $dbConfig);
        DB::reconnect($this->dbDefaultDriver);
    }

    /**
     * Check if admin database is exist
     *
     * @return mixed
     */
    public function checkAdminDatabaseExist()
    {
        return DB::connection($this->dbDefaultDatabase)
            ->select("SELECT 1 FROM pg_database WHERE datname = '{$this->adminDatabase}'");
    }

    /**
     * Check if admin database is exist
     *
     * @return mixed
     */
    public function checkClientDatabaseExist($dbName)
    {
        return DB::connection('client')
            ->select("SELECT 1 FROM pg_database WHERE datname = '$dbName'");
    }

    /**
     * Set the path for migration
     *
     * @param String $migrationPath
     * @return void
     */
    public function migrator($migrationPath)
    {
        $migrator = App::make('migrator');
        $migrator->run($migrationPath);
    }

    /**
     * Add migration table
     *
     * @return void
     */
    private function migration()
    {
        $migrations = App::make('migration.repository');
        $migrations->createRepository();
    }

    /**
     * Show the admin database name
     *
     * @return string
     */
    public function getAdminDatabase()
    {
        return $this->adminDatabase;
    }
}
