<?php

namespace Vetlify\Database\Supports\Facades;

use Illuminate\Support\Facades\Facade;

class VetlifyDatabase extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'VetlifyDatabase';
    }
}
