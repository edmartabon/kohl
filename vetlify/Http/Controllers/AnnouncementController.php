<?php

namespace Vetlify\Http\Controllers;

use DB;
use Sentinel;
use Appointment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Announcement;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Settings\BranchStoreRequest;
use Vetlify\Http\Requests\Settings\BranchUpdateRequest;
use Vetlify\Client\Contracts\AppointmentContract;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();

        return Announcement::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (!in_array('start_at', $data)) {
            $data['start_at'] = date('Y-m-d H:i:s');

        }

        if (!in_array('end_at', $data)) {
            $data['end_at'] = date('Y-m-d H:i:s');
        }

        if (!in_array('active', $data)) {
            $data['active'] = true;
        }
        return Announcement::create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return Announcement::where('id', $request->route('id'))->first();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->route('id');
        return tap(Announcement::find($request->route('id')))
            ->update($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->route('id');
        return Announcement::destroy($id);
    }

}
