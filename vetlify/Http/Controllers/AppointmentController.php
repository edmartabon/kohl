<?php

namespace Vetlify\Http\Controllers;

use DB;
use Sentinel;
use Appointment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Appointment as AppointmentModel;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Settings\BranchStoreRequest;
use Vetlify\Http\Requests\Settings\BranchUpdateRequest;
use Vetlify\Client\Contracts\AppointmentContract;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AppointmentContract $appointment)
    {
        $user = Sentinel::getUser();
        $data = $request->all();

        return $appointment->show(array_merge($data, [
            'branch_id' => $user->branch_id
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $data = $request->all();

        $data['start_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['start_at']));
        $data['end_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['end_at']));
        $data['notify_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['notify_at']));
        $data['patient_id'] = $request->route('id');
        $data['branch_id'] = $user->branch_id;
        
        $appointment = Appointment::create($data);

        return Appointment::leftJoin('users', 'users.id', '=', 'appointments.veterinarian_id')
            ->leftJoin('appointment_status', 'appointments.appointment_status_id', '=', 'appointment_status.id')
            ->where('appointments.id', $appointment->id)
            ->select(
                'appointments.*',
                'appointment_status.color',
                DB::raw('INITCAP(users.first_name || \' \' || users.last_name) AS title'),
                DB::raw('(TO_CHAR(appointments.start_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS start'),
                DB::raw('(TO_CHAR(appointments.end_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS end')
            )
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $data = $request->all();

        $data['start_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['start_at']));
        $data['end_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['end_at']));
        $data['notify_at'] = date('Y-m-d H:i:s', strtotime($request['date_start'].' '.$request['notify_at']));

        $appointment = Appointment::where([
            ['id', '=', $request->route('appointmentId')],
            ['branch_id', '=', $user->branch_id],
        ])->first();

        $appointment->fill($data);
        $appointment->save();
        
        return Appointment::leftJoin('users', 'users.id', '=', 'appointments.veterinarian_id')
            ->leftJoin('appointment_status', 'appointments.appointment_status_id', '=', 'appointment_status.id')
            ->where('appointments.id', $request->route('appointmentId'))
            ->select(
                'appointments.*',
                'appointment_status.color',
                DB::raw('INITCAP(users.first_name || \' \' || users.last_name) AS title'),
                DB::raw('(TO_CHAR(appointments.start_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS start'),
                DB::raw('(TO_CHAR(appointments.end_at, \'YYYY-MM-DD"T"HH24:MI:SS"\')) AS end')
            )
            ->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Appointment::where([
            ['id', '=', $request->route('appointmentId')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
