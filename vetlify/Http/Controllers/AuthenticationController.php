<?php

namespace Vetlify\Http\Controllers;

use Config;
use Sentinel;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthenticationController extends Controller
{
    /**
     * Show the login page
     *
     * @return void
     */
    public function loginPage()
    {
        return view('Vetlify::pages.authentication.login');
    }

    /**
     * Show the registration page
     *
     * @return void
     */
    public function registerPage()
    {
        return view('Vetlify::pages.authentication.register');
    }

    /**
     * Authenticate the user
     *
     * @param Request $request
     */
    public function loginProcess(Request $request)
    {
        $auth = false;

        if (is_null($request['remember'])) {
            $auth = Sentinel::authenticate($request->all());
        } else {
            $auth = Sentinel::authenticateAndRemember($request->all());
        }

        if ($auth) {
            return $this->successAuthenticate();
        }

        return $this->failedAuthenticate();
    }

    /**
     * Process if user authentication is success
     *
     * @param array $auth
     * @return void
     */
    private function successAuthenticate()
    {
        // Add logic when different role
        return redirect(Config::get('vetlify.app_route_prefix').'/appointments');
    }

    /**
     * Process if user authentication is failed
     *
     * @param void
     */
    private function failedAuthenticate()
    {
        return Redirect::back()->withErrors(['msg', 'Username or password is incorrect']);
    }

    /**
     * Logout the current session
     *
     * @return void
     */
    public function logout()
    {
        Sentinel::logout();
        return redirect(Config::get('vetlify.app_route_prefix').'/login');
    }
}
