<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Branch;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Settings\BranchStoreRequest;
use Vetlify\Http\Requests\Settings\BranchUpdateRequest;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branches = Branch::query();

        if ($request->search) {
            $search = strtolower($request->search);

            $branches = $branches->where(function ($query) use ($search) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(address_one) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(address_two) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(contact_no) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(phone_no) LIKE ?', "%$searchItem%");
                }
            });
        }

        if ($request->all) {
            return $branches->get();
        }
        return $branches->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BranchStoreRequest $branchStoreRequest)
    {
        $branch = Branch::create($request->all());
        return Branch::find($branch->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, IdCheckerRequest $idCheckerRequest)
    {
        return Branch::find($request->route('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, BranchUpdateRequest $branchUpdateRequest)
    {
        $branch = Branch::find($request->route('id'));
        $branch->fill($request->all());
        $branch->save();
        return $branch;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $branch = Branch::where([
            'id' => $request->route('id'),
            'is_primary' => 0
        ]);

        if ($branch->first()) {
            $branch->delete();
        } else {
            return response()->json([
                'message' => 'The given data was invalid',
                ['id' => ['The id must not a primary branch']]
            ], 422);
        }
    }
}
