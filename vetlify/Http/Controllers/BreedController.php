<?php

namespace Vetlify\Http\Controllers;

use DB;
use File;
use Image;
use Storage;
use Sentinel;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Vetlify\Client\Models\File as ClientFile;
use Vetlify\Client\Models\Breed;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Patients\ClientIndexRequest;
use Vetlify\Http\Requests\Patients\PatientStoreRequest;
use Vetlify\Http\Requests\Patients\PatientUpdateRequest;

class BreedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $breed = Breed::query();

        if ($request->search) {
            $search = strtolower($request->search);
            $breed = $breed->whereRaw('LOWER(name) LIKE ?', "%$search%");
        }

        return $breed->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PatientStoreRequest $PatientStoreRequest)
    {
        $user = Sentinel::getUser();
        $data = $request->all();
        
        $data['branch_id'] = $user->branch_id;
        $client = Client::find($data['client_id']);
        $patient = Patient::create($data);

        if ($client) {
            $clientPatientIds = $client->patient_ids;
            array_push($clientPatientIds, $patient->id);

            $client->update(['patient_ids' => $clientPatientIds]);
        }
        
        return Patient::where('patients.id', $patient->id)
            ->leftJoin('clients', 'clients.id', '=', 'patients.client_id')
            ->select([
                'patients.id AS patient_id',
                'clients.id AS client_id',
                'patients.patient_record_id',
                'patients.first_name AS patient_first_name',
                DB::raw("COALESCE(patients.last_name, '') AS patient_last_name"),
                'clients.first_name AS client_first_name',
                'clients.last_name AS client_last_name'
            ])
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, PatientUpdateRequest $patientUpdateRequest)
    {
        $user = Sentinel::getUser();
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);
        if ($patient->count()) {
            $patient = $patient->first();
            $patient->fill($request->all());
            $patient->save();
            return $patient;
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
