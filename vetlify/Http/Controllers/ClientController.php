<?php

namespace Vetlify\Http\Controllers;

use DB;
use File;
use Image;
use Storage;
use Sentinel;
use Response;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Patient;
use Vetlify\Client\Models\Payment;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\Transaction;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Patients\ClientIndexRequest;
use Vetlify\Http\Requests\Patients\PatientStoreRequest;
use Vetlify\Http\Requests\Clients\ClientStoreRequest;
use Vetlify\Http\Requests\Clients\ClientUpdateRequest;
use Vetlify\Http\Requests\Clients\ClientAvatarRequest;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        $user = Sentinel::getUser();
        
        $client = Client::query();

        if ($request->search) {
            $search = strtolower($request->search);
            $client = $client->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(last_name) LIKE ?', "%$searchItem%");
                }
            });
        }

        return $client->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ClientStoreRequest $clientStoreRequest)
    {
        $user = Sentinel::getUser();
        $data = $request->all();
        
        $data['branch_id'] = $user->branch_id;
        $data['patient_ids'] = [];
            
        return Client::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, ClientUpdateRequest $clientUpdateRequest)
    {
        $user = Sentinel::getUser();
        $patient = Client::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);
        if ($patient->count()) {
            $patient = $patient->first();
            $patient->fill($request->all());
            $patient->save();
            return $patient;
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }

    /**
     * Update the information of patient
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function avatar(Request $request, IdCheckerRequest $idCheckerRequest, ClientAvatarRequest $clientAvatarRequest)
    {
        $subDomain = $request->route('account');
        $avatar = $request->file('avatar');
        $client = Client::find($request->route('id'));
        $deleteAvatar = [];
        
        $name = $avatar->getClientOriginalName();
        $extension = File::extension($name);
        $uuidName = Uuid::uuid4().'.'.$extension;

        $sizes = [
            'thumbnail' => ['width' => 50, 'height' => 50],
            'medium' => ['width' => 300, 'height' => 300]
        ];

        foreach ($sizes as $fileFolderSize => $size) {
            $img = Image::make($avatar->getPathName());
            $canvas = Image::canvas($size['width'], $size['height']);
            $newImage = $img->resize($size['width'], $size['height'], function ($constraint) {
                $constraint->aspectRatio();
            });

            $canvas->insert($newImage, 'center');
            $canvas->stream();

            $folderPath = 'clients/'.$subDomain.'/client/avatar/'.$fileFolderSize.'/';
            $upload = Storage::disk('spaces')->put($folderPath.$uuidName, $canvas->__toString(), 'public');

            if ($client->avatar) {
                $originalName = $folderPath.$client->avatar;
                array_push($deleteAvatar, $originalName);
            }
        }

        $folderPath = 'clients/'.$subDomain.'/client/avatar/original/';
        $upload = Storage::disk('spaces')->put($folderPath.$uuidName, file_get_contents($avatar), 'public');

        if ($client->avatar) {
            $originalName = $folderPath.$client->avatar;
            array_push($deleteAvatar, $originalName);
        }

        Storage::disk('spaces')->delete($deleteAvatar);
        
        return tap($client)->update(['avatar' => $uuidName]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoice(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $branch = Branch::find($user->branch_id);

        $invoice = [];

        $invoiceInfo = Transaction
            ::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('products', 'transaction_items.product_id', '=', 'products.id')
            ->leftJoin('patients', 'transactions.patient_id', '=', 'patients.id')
            ->leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transaction_id')
            ->where('clients.id', $request->id)
            ->whereNull('payments.id')
            ->select(
                'clients.id',
                'clients.first_name',
                'clients.last_name',
                'patients.id AS patient_id',
                'patients.first_name AS patient_first_name',
                'patients.last_name AS patient_last_name',
                'products.name AS product_name',
                'transactions.id AS transaction_id',
                'transaction_items.price',
                'transaction_items.discount',
                'transaction_items.quantity',
                'transaction_items.tax'
            );

        $invoice['branch'] = $branch;
        $invoice['items'] = [];
        $invoice['client'] = 0;
        $invoice['sub_total'] = 0;
        $invoice['total'] = 0;
        $invoice['tax'] = 0;
        $invoice['discount'] = 0;
            
        foreach ($invoiceInfo->get() as $invoiceItem) {
            $fullname = $invoiceItem->patient_first_name.' '.$invoiceItem->patient_last_name;
            if (!array_key_exists($fullname, $invoice['items'])) {
                $invoice['items'][$fullname] = [];
            }

            array_push($invoice['items'][$fullname], [
                'patient_id' => $invoiceItem->patient_id,
                'transaction_id' => $invoiceItem->transaction_id,
                'name' => $invoiceItem->product_name,
                'price' => $invoiceItem->price,
                'discount' => $invoiceItem->discount,
                'quantity' => $invoiceItem->quantity,
                'tax' => $invoiceItem->tax,
                'total' => ($invoiceItem->price * $invoiceItem->quantity) - $invoiceItem->discount,
            ]);

            $invoice['client'] = $invoiceItem->id;
            $invoice['sub_total'] = $invoice['sub_total'] + ($invoiceItem->price * $invoiceItem->quantity);
            $invoice['discount'] = $invoice['discount'] + $invoiceItem->discount;
        }

        $invoice['total'] = $invoice['sub_total'] - $invoice['discount'];
        $invoice['sub_total'] = number_format($invoice['sub_total'], 2);
        $invoice['total'] = number_format($invoice['total'], 2);
        $invoice['tax'] = number_format($invoice['tax'], 2);
        $invoice['discount'] = number_format($invoice['discount'], 2);
        $invoice['client'] = Client::find($invoice['client']);

        if ($invoice['total'] == $request['total']) {
            $paymentLists = [];
            foreach ($invoice['items'] as $items) {
                $newPayment = [
                    'cost' => 0,
                    'tax' => 0,
                    'discount' => 0,
                    'transaction_id' => 0,
                    'branch_id' => $user->branch_id,
                    'payment_method_id' => $request['payment_method'],
                ];
                
                foreach ($items as $item) {
                    $newPayment['cost'] += $item['price'];
                    $newPayment['tax'] += $item['tax'];
                    $newPayment['discount'] += $item['discount'];
                    $newPayment['transaction_id'] = $item['transaction_id'];
                }
                array_push($paymentLists, $newPayment);
                Payment::create($newPayment);
            }
            return $paymentLists;
        }

        return Response::json([
            'message' => 'Invalid Transaction try again.',
            'code' => config('vetlify.DISPLAY_ERROR_REFRESH_MODAL')
        ], 422);
    }
}
