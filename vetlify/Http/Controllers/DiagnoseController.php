<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Diagnose;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;

class DiagnoseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $diagnose = Diagnose::query();

        if ($request->search) {
            $search = strtolower($request->search);
            $diagnose->whereRaw('LOWER(name) LIKE ?', "%$search%");
        }


        return $diagnose->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Diagnose::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, BranchUpdateRequest $branchUpdateRequest)
    {
        $branch = Branch::find($request->route('id'));
        $branch->fill($request->all());
        $branch->save();
        return $branch;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $branch = Branch::where([
            'id' => $request->route('id'),
            'is_primary' => 0
        ]);

        if ($branch->first()) {
            $branch->delete();
        } else {
            return response()->json([
                'message' => 'The given data was invalid',
                ['id' => ['The id must not a primary branch']]
            ], 422);
        }
    }
}
