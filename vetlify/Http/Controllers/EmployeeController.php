<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\User;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();

        $veterinarian = User::leftJoin('role_users', 'users.id', '=', 'role_users.user_id')
            ->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
            ->where('users.branch_id', $user->branch_id);
        
        if ($request->search) {
            $search = strtolower($request->search);
            $veterinarian = $veterinarian->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(users.first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(users.last_name) LIKE ?', "%$searchItem%");
                }
            });
        }

        $veterinarian = $veterinarian->select(\DB::raw('users.*'));

        if ($request->all) {
            return $veterinarian->get();
        }
        return $veterinarian->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BranchStoreRequest $branchStoreRequest)
    {
        $branch = Branch::create($request->all());
        return Branch::find($branch->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $branch = Branch::find($request->route('id'));
        $branch->fill($request->all());
        $branch->save();
        return $branch;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
    }
}
