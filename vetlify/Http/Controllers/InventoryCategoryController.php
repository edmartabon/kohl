<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Category;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Inventory\CategoryCreateRequest;
use Vetlify\Http\Requests\Inventory\CategoryUpdateRequest;

class InventoryCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $patient = Category::where('categories.branch_id', $user->branch_id)
            ->where('categories.is_active', 1);

        return $patient
            ->orderBy('categories.id', 'desc')
            ->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CategoryCreateRequest $categoryCreateRequest)
    {
        $user = Sentinel::getUser();
        return Category::create([
            'name' => $request['name'],
            'branch_id' => $user->branch_id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, CategoryUpdateRequest $categoryUpdateRequest)
    {
        $user = Sentinel::getUser();
        $category = Category::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);

        if ($category->count()) {
            $category = $category->first();
            $category->fill($request->all());
            $category->save();
            return $category;
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Category::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
