<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Product;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Inventory\ProductCreateRequest;
use Vetlify\Http\Requests\Inventory\CategoryUpdateRequest;

class InventoryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $products = Product::leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->where('products.branch_id', $user->branch_id);

        if (!$request->procedure) {
            $products = $products->where('categories.is_active', '=', 1);
        }

        if ($request->input('category')) {
            $products = $products->where('categories.id', $request->category);
        }

        if ($request->search) {
            $search = strtolower($request->search);
            $products = $products->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(products.name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(products.item_code) LIKE ?', "%$searchItem%");
                }
            });
        }

        return $products
            ->select('products.*', 'categories.name AS category_name')
            ->orderBy('products.id', 'desc')
            ->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ProductCreateRequest $productCreateRequest)
    {
        $user = Sentinel::getUser();
        $data = array_merge($request->all(), ['branch_id' => $user->branch_id]);
        $product = Product::create($data);

        return Product::leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->where([
                ['products.branch_id', '=', $user->branch_id],
                ['products.id', '=', $product->id]
            ])
            ->select('products.*', 'categories.name AS category_name')
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, CategoryUpdateRequest $categoryUpdateRequest)
    {
        $user = Sentinel::getUser();
        $product = Product::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);
        
        $productData = array_merge($request->all(), ['branch_id' => $user->branch_id]);

        if ($product->count()) {
            $product = $product->first();
            $product->fill($productData);
            $product->save();

            return Product::leftJoin('categories', 'categories.id', '=', 'products.category_id')
                ->where('products.id', $request->route('id'))
                ->select('products.*', 'categories.name AS category_name')
                ->first();
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Product::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
