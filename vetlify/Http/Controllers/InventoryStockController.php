<?php

namespace Vetlify\Http\Controllers;

use DB;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\Product;
use Vetlify\Client\Models\ProductStock;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Inventory\SupplierCreateRequest;

class InventoryStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $stocks = ProductStock
            ::leftJoin('products', 'products.id', '=', 'product_stocks.product_id')
            ->leftJoin('suppliers', 'suppliers.id', '=', 'product_stocks.supplier_id')
            ->where('products.branch_id', $user->branch_id);

        if ($request->search) {
            $search = strtolower($request->search);
            $stocks = $stocks->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(products.name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(suppliers.company_name) LIKE ?', "%$searchItem%");
                }
            });
        }

        return $stocks
            ->select('products.name', 'suppliers.company_name as supplier_name', 'product_stocks.*', DB::raw("to_char(product_stocks.created_at, 'Month DD, YYYY') as created_date"))
            ->orderBy('suppliers.id', 'desc')
            ->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $stockData = array_merge($request->all(), ['branch_id' => $user->branch_id]);
        $stock = ProductStock::create($stockData);
        $product = Product::find($request['product_id'])
            ->increment('inventory_count', $request['quantity']);


        return ProductStock::leftJoin('products', 'products.id', '=', 'product_stocks.product_id')
            ->leftJoin('suppliers', 'suppliers.id', '=', 'product_stocks.supplier_id')
            ->where('products.branch_id', $user->branch_id)
            ->where('product_stocks.id', $stock->id)
            ->select('products.name', 'suppliers.company_name as supplier_name', 'product_stocks.*', DB::raw("to_char(product_stocks.created_at, 'Month DD, YYYY') as created_date"))
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return ProductStock::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
