<?php

namespace Vetlify\Http\Controllers;

use DB;
use Sentinel;
use Appointment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Branch;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Payment;
use Vetlify\Client\Models\Transaction;
use Vetlify\Client\Models\TransactionItem;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Client\Contracts\AppointmentContract;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $data = $request->all();

        $invoice = Transaction
            ::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('patients', 'transactions.patient_id', '=', 'patients.id')
            ->leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transaction_id')
            ->whereNull('payments.id')
            ->select(
                'clients.id',
                'clients.first_name',
                'clients.last_name',
                DB::raw('SUM(transaction_items.price) AS total_price'),
                DB::raw('SUM(transaction_items.discount) AS total_discount')
            )
            ->groupBy('clients.id');

        return $invoice->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $branch = Branch::find($user->branch_id);

        $invoice = [];

        $invoiceInfo = Transaction
            ::leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->leftJoin('products', 'transaction_items.product_id', '=', 'products.id')
            ->leftJoin('patients', 'transactions.patient_id', '=', 'patients.id')
            ->leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transaction_id')
            ->whereNull('payments.id')
            ->select(
                'clients.id',
                'clients.first_name',
                'clients.last_name',
                'patients.first_name AS patient_first_name',
                'patients.last_name AS patient_last_name',
                'products.name AS product_name',
                'transaction_items.price',
                'transaction_items.discount',
                'transaction_items.quantity',
                'transaction_items.tax'
            );

        $invoice['branch'] = $branch;
        $invoice['items'] = [];
        $invoice['client'] = 0;
        $invoice['sub_total'] = 0;
        $invoice['total'] = 0;
        $invoice['tax'] = 0;
        $invoice['discount'] = 0;
            
        foreach ($invoiceInfo->get() as $invoiceItem) {
            $fullname = $invoiceItem->patient_first_name.' '.$invoiceItem->patient_last_name;
            if (!array_key_exists($fullname, $invoice['items'])) {
                $invoice['items'][$fullname] = [];
            }

            array_push($invoice['items'][$fullname], [
                'name' => $invoiceItem->product_name,
                'price' => $invoiceItem->price,
                'discount' => $invoiceItem->discount,
                'quantity' => $invoiceItem->quantity,
                'total' => ($invoiceItem->price * $invoiceItem->quantity) - $invoiceItem->discount,
            ]);

            $invoice['client'] = $invoiceItem->id;
            $invoice['sub_total'] = $invoice['sub_total'] + ($invoiceItem->price * $invoiceItem->quantity);
            $invoice['discount'] = $invoice['discount'] + $invoiceItem->discount;
        }

        $invoice['total'] = $invoice['sub_total'] - $invoice['discount'];
        $invoice['sub_total'] = number_format($invoice['sub_total'], 2);
        $invoice['total'] = number_format($invoice['total'], 2);
        $invoice['tax'] = number_format($invoice['tax'], 2);
        $invoice['discount'] = number_format($invoice['discount'], 2);

        $invoice['client'] = Client::find($invoice['client']);

        return $invoice;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
    }
}
