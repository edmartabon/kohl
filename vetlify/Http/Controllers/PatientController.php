<?php

namespace Vetlify\Http\Controllers;

use DB;
use File;
use Image;
use Storage;
use Sentinel;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Vetlify\Client\Models\File as ClientFile;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Patient;
use Vetlify\Client\Models\Procedure;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Patients\ClientIndexRequest;
use Vetlify\Http\Requests\Patients\PatientStoreRequest;
use Vetlify\Http\Requests\Patients\PatientUpdateRequest;
use Vetlify\Http\Requests\Patients\PatientAvatarRequest;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        $user = Sentinel::getUser();
        $patient = Patient::leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->where('patients.branch_id', $user->branch_id);

        if ($request->start && $request->end) {
            $patient = $patient->where([
                ['patients.created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->start))],
                ['patients.created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->end))]
            ]);
        }

        if ($request->gender) {
            $patient = $patient->where([
                'patients.patient_gender_id' => $request->gender
            ]);
        }

        if ($request->search) {
            $search = strtolower($request->search);
            $patient = $patient->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(patients.first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(patients.last_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(clients.first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(clients.last_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(patients.patient_record_id) LIKE ?', "%$searchItem%");
                }
                $query->orWhere('patients.rfid', '=', $request->search);
            });
        }

        $patient = $patient
            ->select([
                'patients.id AS patient_id',
                'clients.id AS client_id',
                'patients.patient_record_id',
                'patients.first_name AS patient_first_name',
                'patients.last_name AS patient_last_name',
                'patients.avatar AS patient_avatar',
                'clients.first_name AS client_first_name',
                'clients.last_name AS client_last_name',
                'clients.avatar AS client_avatar',
                DB::raw('CASE when clients.email = \'\' OR clients.email IS NULL then patients.email else clients.email end as email'),
                DB::raw('CASE when clients.phone_no = \'\' OR clients.phone_no IS NULL then patients.phone_no else clients.phone_no end as phone_no'),
                DB::raw("TO_CHAR(patients.created_at, 'dd/mm/yyyy') AS date_created")
            ])
            ->orderBy('patients.id', 'desc')
            ->paginate(20);

        return $patient;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PatientStoreRequest $PatientStoreRequest)
    {
        $user = Sentinel::getUser();
        $data = $request->all();
        $client = null;

        $data['branch_id'] = $user->branch_id;

        $patient = Patient::create($data);

        if (!empty($data['client_id'])) {
            $client = Client::find($data['client_id']);
        }

        if ($client) {
            $clientPatientIds = $client->patient_ids;
            array_push($clientPatientIds, $patient->id);

            $client->update(['patient_ids' => $clientPatientIds]);
        }
        
        return Patient::where('patients.id', $patient->id)
            ->leftJoin('clients', 'clients.id', '=', 'patients.client_id')
            ->select([
                'patients.id AS patient_id',
                'clients.id AS client_id',
                'patients.patient_record_id',
                'patients.first_name AS patient_first_name',
                DB::raw("COALESCE(patients.last_name, '') AS patient_last_name"),
                'clients.first_name AS client_first_name',
                'clients.last_name AS client_last_name',
                'patients.created_at'
            ])
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, PatientUpdateRequest $patientUpdateRequest)
    {
        $user = Sentinel::getUser();
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);
        if ($patient->count()) {
            $patient = $patient->first();
            $patient->fill($request->all());
            $patient->save();
            return $patient;
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }

    /**
     * Update the information of patient
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function avatar(Request $request, IdCheckerRequest $idCheckerRequest, PatientAvatarRequest $patientAvatarRequest)
    {
        $subDomain = $request->route('account');
        $avatar = $request->file('avatar');
        $patient = Patient::find($request->route('id'));
        $deleteAvatar = [];

        $name = $avatar->getClientOriginalName();
        $extension = File::extension($name);
        $uuidName = Uuid::uuid4().'.'.$extension;

        $sizes = [
            'thumbnail' => ['width' => 50, 'height' => 50],
            'medium' => ['width' => 300, 'height' => 300]
        ];

        foreach ($sizes as $fileFolderSize => $size) {
            $img = Image::make($avatar->getPathName());
            $canvas = Image::canvas($size['width'], $size['height']);
            $newImage = $img->resize($size['width'], $size['height'], function ($constraint) {
                $constraint->aspectRatio();
            });

            $canvas->insert($newImage, 'center');
            $canvas->stream();


            $folderPath = 'clients/'.$subDomain.'/patient/avatar/'.$fileFolderSize.'/';
            $upload = Storage::disk('spaces')->put($folderPath.$uuidName, $canvas->__toString(), 'public');

            if ($patient->avatar) {
                $originalName = $folderPath.$patient->avatar;
                array_push($deleteAvatar, $originalName);
            }
        }

        $folderPath = 'clients/'.$subDomain.'/patient/avatar/original/';
        $upload = Storage::disk('spaces')->put($folderPath.$uuidName, file_get_contents($avatar), 'public');

        if ($patient->avatar) {
            $originalName = $folderPath.$patient->avatar;
            array_push($deleteAvatar, $originalName);
        }

        Storage::disk('spaces')->delete($deleteAvatar);
        
        return tap($patient)->update(['avatar' => $uuidName]);
    }

    /**
     * show the procedure for the specific procedure
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function procedure(Request $request)
    {
        $user = Sentinel::getUser();

        return Procedure::where([
            'patient_id' => $request->id,
            'branch_id' => $user->branch_id
        ])
            ->paginate(20);
    }

    /**
     * show the procedure for the specific procedure
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function files(Request $request)
    {
        $user = Sentinel::getUser();

        return ClientFile::where([
            'patient_id' => $request->id,
            'branch_id' => $user->branch_id
        ])
            ->paginate(20);
    }
}
