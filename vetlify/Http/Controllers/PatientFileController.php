<?php

namespace Vetlify\Http\Controllers;

use DB;
use File;
use Image;
use Storage;
use Sentinel;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\File as ClientFile;
use Vetlify\Http\Requests\Http\IdCheckerRequest;

class PatientFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();

        return ClientFile::paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $subDomain = $request->route('account');
        $files = $request->file('files');
        
        $name = $files->getClientOriginalName();
        $extension = File::extension($name);
        $uuidName = Uuid::uuid4().'.'.$extension;

        $newName = 'clients/'.$subDomain.'/patient/files/'.$uuidName;
        $upload = Storage::disk('spaces')->put($newName, file_get_contents($files));
        
        $data = [
            'name' => $request['name'],
            'file_name' => $uuidName,
            'veterinarian' => $request['veterinarian'],
            'patient_id' => $request->route('id'),
            'extension' => $extension,
            'branch_id' => $user->branch_id
        ];
        
        return $patient = ClientFile::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $file = ClientFile::where([
            ['id', '=', $request->route('fileId')],
            ['branch_id', '=', $user->branch_id],
        ]);
        if ($file->count()) {
            $file = $file->first();
            $file->fill($request->all());
            $file->save();
            return $file;
        }
        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return ClientFile::where([
            ['id', '=', $request->route('fileId')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
