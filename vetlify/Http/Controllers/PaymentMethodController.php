<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\PaymentMethod;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Settings\BranchStoreRequest;
use Vetlify\Http\Requests\Settings\BranchUpdateRequest;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branches = Branch::query();

        return PaymentMethod::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BranchStoreRequest $branchStoreRequest)
    {
        $branch = Branch::create($request->all());
        return Branch::find($branch->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, BranchUpdateRequest $branchUpdateRequest)
    {
        $branch = Branch::find($request->route('id'));
        $branch->fill($request->all());
        $branch->save();
        return $branch;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $branch = Branch::where([
            'id' => $request->route('id'),
            'is_primary' => 0
        ]);

        if ($branch->first()) {
            $branch->delete();
        } else {
            return response()->json([
                'message' => 'The given data was invalid',
                ['id' => ['The id must not a primary branch']]
            ], 422);
        }
    }
}
