<?php

namespace Vetlify\Http\Controllers;

use Mail;
use Config;
use Redirect;
use Session;
use GuzzleHttp\Client;
use Sentinel as UserSentinel;
use Illuminate\Http\Request;
use Vetlify\Admin\Models\Account;
use Vetlify\Admin\Models\AccountUser;
use Vetlify\Admin\Models\Database;
use Vetlify\Admin\Models\SpamType;
use Vetlify\Admin\Models\SpamEntry;
use Vetlify\Client\Models\Branch;
use App\Http\Controllers\Controller;
use Vetlify\Http\Mails\WelcomeClient;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;
use Vetlify\Http\Requests\Accounts\AccountCreateRequest;

class RegisterAccountController extends Controller
{
    private $spamLimit = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerView(Request $request)
    {
        $totalEntry = $this->totalEntry($request);

        if ($totalEntry >= 5) {
            Session::flash('addCaptcha', 'captcha');
        }
        return view('Vetlify::pages.authentication.register');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return view('Vetlify::pages.authentication.login_home');
    }

    /**
     * Authenticate the user
     *
     * @param Request $request
     */
    public function loginProcess(Request $request, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $accountUser = AccountUser::where('email', $request['email'])->first();

        if ($accountUser) {
            $account = Account::leftJoin('databases', 'accounts.database_id', '=', 'databases.id')
                ->where('accounts.id', $accountUser->account_id)
                ->select('accounts.server as database', 'databases.host', 'databases.port')
                ->first();

            $vetlifyDatabase->changeDefaultConfigConnection($account->toArray());
            $auth = false;

            if (is_null($request['remember'])) {
                $auth = UserSentinel::authenticate($request->all());
            } else {
                $auth = UserSentinel::authenticateAndRemember($request->all());
            }

            if ($auth) {
                return $this->successAuthenticate($account->database);
            }
        }

        return $this->failedAuthenticate();
    }

    /**
     * Process if user authentication is success
     *
     * @param array $auth
     * @return void
     */
    private function successAuthenticate($subdomain)
    {
        $cookie = \Cookie::get();
        return redirect(env('VETLIFY_SSL_PAGE_URL').'://'.$subdomain.env('VETLIFY_SUB_PAGE_URL').Config::get('vetlify.app_route_prefix').'/appointments')
            ->withCookie(cookie('XSRF-TOKEN', $cookie['XSRF-TOKEN'], 60 * 60 * 24, '/', '.vetlify.local'))
            ->withCookie(cookie('laravel_session', $cookie['laravel_session'], 60 * 60 * 24, '/', '.vetlify.local'));
    }

    /**
     * Process if user authentication is failed
     *
     * @param void
     */
    private function failedAuthenticate()
    {
        return Redirect::back()->withErrors(['msg', 'Username or password is incorrect']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request, AccountCreateRequest $accountCreateRequest, VetlifyDatabaseContract $vetlifyDatabase)
    {
        if (!$this->checkSpam($request)) {
            return redirect()->back()->with('captcha_error', ['Invalid captcha. Try again.']);
        }

        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $db = Database::orderBy('id', 'desc')->first();
        $account = Account::create([
            'name' => $request['name'],
            'server' => $request['server'],
            'database_id' => $db->id,
            'account_type_id' => 2,
            'account_status_id' => 1,
            'expired_at' => date('Y-m-d H:i:s', strtotime('+30 day', time()))
        ]);

        AccountUser::create([
            'account_id' => $account->id,
            'email' => $request['email']
        ]);

        $account = Account::leftJoin('databases', 'accounts.database_id', '=', 'databases.id')
            ->where('accounts.id', $account->id)
            ->select('accounts.server AS database', 'databases.host', 'databases.port')
            ->first()->toArray();

        $vetlifyDatabase->createClientDatabase($account);
        $vetlifyDatabase->migrateClientMigration($account);
        
        $this->createAccountUser($request, $vetlifyDatabase, $account);

        // Change the connetion to admin
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $this->addSpamEntries($request);
        // $this->sendEmail($request);
        

        return Redirect::to('http://'.$request['server'].Config::get('vetlify.main_sub_url'));
    }

    /**
     * Create account's user
     *
     * @param Illuminate\Http\Request $request
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract $vetlifyDatabase
     * @param Vetlify\Admin\Models\Account $account
     * @return void
     */
    private function createAccountUser(Request $request, VetlifyDatabaseContract $vetlifyDatabase, $account)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $account['database']]);
        $branch = Branch::create([
            'name' => $request['name'],
            'address_one' => $request['address_one'],
            'phone_no' => $request['phone_no'],
            'is_primary' => 1
        ]);

        $user = UserSentinel::registerAndActivate([
            'email' 	 => $request['email'],
            'password'   => $request['password'],
            'first_name' => $request['first_name'],
            'last_name'  => $request['last_name'],
            'branch_id'  => $branch->id,
            'user_status_id' => 1
        ]);

        $adminRole = UserSentinel::findRoleByName('Admin');
        $adminRole->users()->attach($user);
    }

    /**
     * Send new welcome email
     *
     * @param Illuminate\Http\Request $request
     * @return void
     */
    private function sendEmail(Request $request)
    {
        $mail = Mail::to($request['email'])
            ->send(new WelcomeClient($request));
    }

    /**
     * Add new spam entry
     *
     * @param Illuminate\Http\Request $request
     * @return void
     */
    private function addSpamEntries(Request $request)
    {
        $spamType = SpamType::where('name', 'Account Registration')->first();
        $expireEntry = date('Y-m-d H:i:s', strtotime('+1 days'));
        return SpamEntry::create([
            'ip_addr' => $request->ip(),
            'spam_type_id' => $spamType->id,
            'expired_at' => $expireEntry
        ]);
    }

    /**
     * Validate the recaptcha
     *
     * @param Illuminate\Http\Request $request
     * @return bool
     */
    private function checkSpam(Request $request)
    {
        $dateExpired = date('Y-m-d H:i:s', strtotime('-1 days'));
        SpamEntry::whereDate('expired_at', '<=', $dateExpired)->delete();

        $totalEntry = $this->totalEntry($request);

        if ($totalEntry >= $this->spamLimit) {
            $client = new Client();
            $res = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => env('CAPTCHA_KEY_SECRET', null),
                    'response' => $request['g-recaptcha-response'],
                    'remoteip' => $request->ip()
                ]
            ]);
            $body = json_decode($res->getBody());

            if ($body->success == false) {
                return false;
            }
        }
        return true;
    }

    private function totalEntry(Request $request)
    {
        $spamType = SpamType::where('name', 'Account Registration')->first();
        return SpamEntry::where('ip_addr', $request->ip())
            ->where('spam_type_id', $spamType->id)
            ->get()->count();
    }
}
