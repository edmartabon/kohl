<?php

namespace Vetlify\Http\Controllers;

use DB;
use Sentinel;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Patient;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Category;
use Vetlify\Client\Models\Supplier;
use Vetlify\Client\Models\Product;
use Vetlify\Client\Models\PaymentMethod;
use Vetlify\Client\Models\PatientBlood;
use Vetlify\Client\Models\PatientGender;
use Vetlify\Client\Models\Procedure;
use Vetlify\Client\Models\Announcement;
use Vetlify\Client\Models\AppointmentStatus;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Clients\ClientIndexRequest;

class TemplateController extends Controller
{
    /**
     * Show the client page
     *
     * @param \Vetlify\Http\Requests\Clients\ClientIndexRequest $clientIndexRequest
     * @return \Illuminate\Http\Response
     */
    public function patientList(ClientIndexRequest $clientIndexRequest)
    {
        $user = Sentinel::getUser();
        $patientBloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        return view('Vetlify::pages.admin.patients.patient', [
            'genders' => $genders,
            'bloods' => $patientBloods,
            'branch' => $branch,
            'paymentMethod' => $paymentMethod,
        ]);
    }

    /**
     * Show the client page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfile(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.profile', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileProcedure(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.procedure', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileProcedureCreate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.procedure-create', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileProcedureUpdate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $procedure = Procedure::where([
            ['procedures.id', '=', $request->route('procedureId')],
            ['procedures.branch_id', '=', $user->branch_id],
        ])
            ->leftJoin('users', 'users.id', '=', 'procedures.veterinarian')
            ->select([DB::raw('procedures.*'), 'users.id AS user_id', 'users.first_name', 'users.last_name'])
            ->first();

        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
        
        if ($procedure) {
            return view('Vetlify::pages.admin.patients.procedure-update', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'procedures' => $procedure,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the profile treatment page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileTreatment(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.treatment', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileTreatmentPlanCreate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.treatment-create', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileTreatmentPlanUpdate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.treatment-update', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the profile treatment page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfilePayment(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
 
        if ($patient) {
            return view('Vetlify::pages.admin.patients.payment', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }
 
    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfilePaymentCreate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
 
        if ($patient) {
            return view('Vetlify::pages.admin.patients.payment-create', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }
 
    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfilePaymentUpdate(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
            
        if ($patient) {
            return view('Vetlify::pages.admin.patients.payment-update', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }
    

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileFiles(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');

        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
        
        return view('Vetlify::pages.admin.patients.files', [
            'bloods' => $bloods,
            'genders' => $genders,
            'patient' => $patient,
            'branch' => $branch,
            'paymentMethod' => $paymentMethod,
        ]);
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfilePrescription(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.treatment-create', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileAppointment(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');

        $patient = Patient::leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->where([
            ['patients.id', '=', $request->route('id')],
            ['patients.branch_id', '=', $user->branch_id],
        ])
            ->select(
                'patients.*',
                DB::raw('CASE when clients.email = \'\' OR clients.email IS NULL then patients.email else clients.email end as email'),
                DB::raw('CASE when clients.phone_no = \'\' OR clients.phone_no IS NULL then patients.phone_no else clients.phone_no end as phone_no')
            )
            ->first();


        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();
        
        return view('Vetlify::pages.admin.patients.appointment', [
            'bloods' => $bloods,
            'genders' => $genders,
            'patient' => $patient,
            'branch' => $branch,
            'paymentMethod' => $paymentMethod,
        ]);
    }

    public function appointment(Request $request)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $appointmentStatus = AppointmentStatus::all();
        return view('Vetlify::pages.admin.appointments.appointment', ['appointmentStatus' => $appointmentStatus]);
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function patientProfileSetting(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($patient) {
            return view('Vetlify::pages.admin.patients.setting', [
                'bloods' => $bloods,
                'genders' => $genders,
                'patient' => $patient,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page for the owner
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function clientProfileSetting(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $client = Client::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        if ($client) {
            return view('Vetlify::pages.admin.patients.profile-setting', [
                'bloods' => $bloods,
                'genders' => $genders,
                'client' => $client,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the client page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function clientProfile(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');
        $client = Client::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        $bloods = PatientBlood::all();
        $genders = PatientGender::all();
        $branch = Branch::find($user->branch_id);
        $paymentMethod = PaymentMethod::all();

        $clientPatient = Patient::leftJoin('clients', 'patients.id', '=', DB::raw('ANY(clients.patient_ids)'))
            ->leftJoin('files', 'patients.id', '=', 'files.patient_id')
            ->leftJoin('procedures', 'patients.id', '=', 'procedures.patient_id')
            ->leftJoin('transactions', 'patients.id', '=', 'transactions.patient_id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transaction_id')
            ->where('patients.branch_id', $user->branch_id)
            ->where('clients.id', $request->route('id'))
            ->select(
                'patients.id',
                'patients.first_name',
                'patients.last_name',
                'patients.avatar',
                'patients.created_at',
                DB::raw('COUNT(DISTINCT files.id) AS total_file'),
                DB::raw('COUNT(DISTINCT procedures.id) AS total_procedure'),
                DB::raw('COUNT(DISTINCT payments.id) AS total_payment')
            )
            ->groupBy('files.id', 'patients.id')
            ->get();

        if ($client) {
            return view('Vetlify::pages.admin.patients.client', [
                'bloods' => $bloods,
                'genders' => $genders,
                'client' => $client,
                'branch' => $branch,
                'paymentMethod' => $paymentMethod,
                'patients' => $clientPatient
            ]);
        }
        return view('Vetlify::pages.errors.404');
    }

    /**
     * Show the setting page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function employeeTimeIn(Request $request)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');

        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        
                
        return view('Vetlify::pages.admin.employee.employee', [
            
        ]);
    }
    
    /**
     * Show the invoice page
     *
     * @param \Illuminate\Http\Request $request
     * @param \Vetlify\Http\Requests\Http\IdCheckerRequest $idCheckerRequest
     * @return \Illuminate\Http\Response
     */
    public function invoice(Request $request)
    {
        $user = Sentinel::getUser();
        $id = $request->route('id');

        $patient = Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->first();
        
                
        return view('Vetlify::pages.admin.invoice.invoice', [
            
        ]);
    }

    /**
     * Show the product list page
     *
     * @return \Illuminate\Http\Response
     */
    public function inventoryProductList()
    {
        $user = Sentinel::getUser();
        $categories = Category::where('branch_id', $user->branch_id)->get();

        return view('Vetlify::pages.admin.inventory.product', ['categories' => $categories]);
    }

    /**
     * Show the product list page
     *
     * @return \Illuminate\Http\Response
     */
    public function inventorySupplier()
    {
        return view('Vetlify::pages.admin.inventory.supplier');
    }

    /**
     * Show the product list page
     *
     * @return \Illuminate\Http\Response
     */
    public function inventoryCategory()
    {
        return view('Vetlify::pages.admin.inventory.category');
    }

    /**
     * Show the product list page
     *
     * @return \Illuminate\Http\Response
     */
    public function inventoryStockControl()
    {
        $user = Sentinel::getUser();
        $product = Product::where('branch_id', $user->branch_id)->get();
        $suppliers = Supplier::where('branch_id', $user->branch_id)->get();
        return view('Vetlify::pages.admin.inventory.stock', ['products' => $product, 'suppliers' => $suppliers]);
    }

    /**
     * Show the product list page
     *
     * @return \Illuminate\Http\Response
     */
    public function announcement()
    {
        $annoucement = Announcement::all();
        return view('Vetlify::pages.admin.announcement.announcement');
    }

    /**
     * Show the branch list page
     *
     * @return \Illuminate\Http\Response
     */
    public function settingBranch()
    {
        return view('Vetlify::pages.admin.settings.branch');
    }

    /**
     * Show the branch list page
     *
     * @return \Illuminate\Http\Response
     */
    public function settingBranchInfo()
    {
        $user = Sentinel::getUser();

        return view('Vetlify::pages.admin.settings.branch-info', ['branchId' => $user->id]);
    }

    /**
     * Show the user list page
     *
     * @return \Illuminate\Http\Response
     */
    public function settingUser()
    {
        $roles = Sentinel::getRoleRepository()->get();
        return view('Vetlify::pages.admin.settings.user', ['roles' => $roles]);
    }

    /**
     * Show the user list page
     *
     * @return \Illuminate\Http\Response
     */
    public function settingCurrentUser()
    {
        $user = Sentinel::getUser();

        return view('Vetlify::pages.admin.settings.profile', ['users' => $user]);
    }

}
