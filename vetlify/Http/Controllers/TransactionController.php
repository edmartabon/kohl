<?php

namespace Vetlify\Http\Controllers;

use DB;
use File;
use Image;
use Storage;
use Sentinel;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Patient;
use App\Http\Controllers\Controller;
use Vetlify\Client\Models\Transaction;
use Vetlify\Client\Models\TreatmentPlan;
use Vetlify\Client\Models\TransactionItem;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Patients\ClientIndexRequest;
use Vetlify\Http\Requests\Patients\PatientStoreRequest;
use Vetlify\Http\Requests\Patients\PatientUpdateRequest;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        $user = Sentinel::getUser();
        $treatmentPlan = TreatmentPlan::leftJoin('patients', 'patients.id', '=', 'treatment_plans.patient_id')
            ->leftJoin('users', 'users.id', '=', 'treatment_plans.user_id')
            ->where('treatment_plans.branch_id', $user->branch_id);

        if ($request->patient) {
            $treatmentPlan = $treatmentPlan->where('patients.id', $request->patient);
        }

        return $treatmentPlan
            ->select(
                'treatment_plans.*',
                'users.first_name AS user_first_name',
                'users.last_name AS user_last_name',
                'patients.first_name AS patient_first_name',
                'patients.last_name AS patient_last_name'
            )
            ->orderBy('treatment_plans.id', 'desc')
            ->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $data = $request->all();
        
        $data['user_id'] = $user->id;
        $data['branch_id'] = $user->branch_id;
        $data['reference_no'] = time() . rand(10*45, 100*98);

        $transaction = Transaction::create($data);

        $data['transaction_id'] = $transaction->id;


        TreatmentPlan::create($data);

        foreach ($data['transaction_item'] as $value) {
            TransactionItem::create([
                'price' => $value['price'],
                'quantity' => $value['quantity'],
                'discount' => $value['discount'],
                'product_id' => $value['product_id'],
                'transaction_id' => $transaction->id,
                'tax' => 0,
            ]);
        }

        return $transaction;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();


        $transaction = Transaction::leftJoin('payments', 'payments.transaction_id', '=', 'transactions.id')
            ->where('transactions.id', $request->route('id'))
            ->select('transactions.*', DB::raw('NULLIF(payments.id, 0) AS has_payment'))
            ->first();

        $transactionItem = Transaction::leftJoin('patients', 'patients.id', '=', 'transactions.patient_id')
            ->leftJoin('users', 'users.id', '=', 'transactions.veterinarian')
            ->leftJoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftJoin('products', 'products.id', '=', 'transaction_items.product_id')
            ->where('transactions.id', $request->route('id'))
            ->where('transactions.branch_id', $user->branch_id)
            ->select(
                'transactions.*',
                'transaction_items.id AS transaction_item_id',
                'transaction_items.price',
                'transaction_items.quantity',
                'transaction_items.discount',
                'transaction_items.tax',
                'transaction_items.product_id',
                'products.name AS product_name',
                DB::raw('CONCAT(users.first_name, \' \', users.last_name) AS veterinarian'),
                'users.id AS veterinarian_id'
            )
            ->get();

        $transaction->items = $transactionItem;
        return $transaction;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, PatientUpdateRequest $patientUpdateRequest)
    {
        $user = Sentinel::getUser();
        $data = $request->all();
        
        
        Transaction::find($request->route('id'))
            ->update($data);

        $listTransactionItem = TransactionItem::where('transaction_id', $request->route('id'))
            ->select('id')
            ->get();

        $transactionItemId = [];

        foreach ($listTransactionItem as $value) {
            array_push($transactionItemId, $value->id);
        }

        foreach ($data['transaction_item'] as $value) {
            if (isset($value['transaction_item_id'])) {
                TransactionItem::find($value['transaction_item_id'])
                    ->update([
                        'price' => $value['price'],
                        'quantity' => $value['quantity'],
                        'discount' => $value['discount'],
                        'product_id' => $value['product_id'],
                        'tax' => 0,
                    ]);
                
                if (($key = array_search($value['transaction_item_id'], $transactionItemId)) !== false) {
                    unset($transactionItemId[$key]);
                }
            } else {
                TransactionItem::create([
                    'price' => $value['price'],
                    'quantity' => $value['quantity'],
                    'discount' => $value['discount'],
                    'product_id' => $value['product_id'],
                    'transaction_id' => $request->route('id'),
                    'tax' => 0,
                ]);
            }
        }

        foreach ($transactionItemId as $transactionItemId) {
            TransactionItem::find($transactionItemId)->delete();
        }

        return Transaction::leftJoin('patients', 'patients.id', '=', 'transactions.patient_id')
        ->leftJoin('users', 'users.id', '=', 'transactions.veterinarian')
        ->leftJoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
        ->leftJoin('products', 'products.id', '=', 'transaction_items.product_id')
        ->where('transactions.id', $request->route('id'))
        ->where('transactions.branch_id', $user->branch_id)
        ->select(
            'transactions.*',
            'transaction_items.id AS transaction_item_id',
            'transaction_items.price',
            'transaction_items.quantity',
            'transaction_items.discount',
            'transaction_items.tax',
            'transaction_items.product_id',
            'products.name AS product_name',
            DB::raw('CONCAT(users.first_name, \' \', users.last_name) AS veterinarian'),
            'users.id AS veterinarian_id'
        )
        ->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest)
    {
        $user = Sentinel::getUser();
        return Patient::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ])->delete();
    }
}
