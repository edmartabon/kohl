<?php

namespace Vetlify\Http\Controllers;

use File;
use Image;
use Storage;
use Sentinel;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\User;
use App\Http\Controllers\Controller;
use Vetlify\Admin\Models\Account;
use Vetlify\Admin\Models\AccountUser;
use Vetlify\Http\Requests\Http\IdCheckerRequest;
use Vetlify\Http\Requests\Settings\UserStoreRequest;
use Vetlify\Http\Requests\Settings\UserUpdateRequest;
use Vetlify\Http\Requests\Settings\UserAvatarRequest;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query();

        $users = $users->leftJoin('role_users', 'users.id', '=', 'role_users.user_id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('users.*', 'role_users.role_id', 'roles.name AS role_name')
            ->orderBy('users.id', 'desc');

        if ($request->search) {
            $search = strtolower($request->search);

            $users = $users->where(function ($query) use ($search) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(email) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(last_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(address_one) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(address_two) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(phone_no) LIKE ?', "%$searchItem%");
                }
            });
        }
        return $users->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserStoreRequest $userStoreRequest, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $user = Sentinel::getUser();
        $userData = array_merge($request->all(), ['branch_id' => $user->branch_id]);
        $userData['user_status_id'] = 1;

        $role = Sentinel::findRoleById($request['role_id']);
        $newUser = Sentinel::registerAndActivate($userData);
        
        if ($role) {
            $role->users()->attach($newUser);
            $newUser->role_id = $request['role_id'];
        }

        $this->addAccountUser($user, $request->all(), $vetlifyDatabase);
        return $newUser;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IdCheckerRequest $idCheckerRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdCheckerRequest $idCheckerRequest, UserUpdateRequest $userUpdateRequest, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $user = Sentinel::getUser();
        $updateUser = User::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);

        $oldUser = $updateUser->first();
        
        $productData = array_merge($request->all(), ['branch_id' => $user->branch_id]);
        
        if ($updateUser->count()) {
            if ($productData['password'] == '') {
                unset($productData['password']);
            }

            $updateUser = Sentinel::findUserById($request->route('id'));
            $role = Sentinel::findRoleById($request['role_id']);
            
            
            if ($role) {
                $updateUser->roles()->detach();
                $role->users()->attach($updateUser);
            }

            Sentinel::update($updateUser, $productData);

            $queryUser = User::where('users.id', $request->route('id'))
                ->leftJoin('role_users', 'users.id', '=', 'role_users.user_id')
                ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
                ->select('users.*', 'role_users.role_id', 'roles.name AS role_name')->first();

            $this->updateAccountUser($oldUser, $productData, $vetlifyDatabase);

            return $queryUser;
        }


        return response()->json(['errors' => 'Unauthenticated.'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, IdCheckerRequest $idCheckerRequest, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $user = Sentinel::getUser();
        $deletUser = User::where([
            ['id', '=', $request->route('id')],
            ['branch_id', '=', $user->branch_id],
        ]);

        if ($deletUser->first()) {
            $oldUser = $deletUser->first();
            $deletUser->delete();
            $this->deleteAccountUser($oldUser->email, $vetlifyDatabase);
        } else {
            return response()->json([
                'message' => 'The given data was invalid',
                ['id' => ['The id must not a primary branch']]
            ], 422);
        }
    }

    /**
     * Update the information of current user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function current(Request $request)
    {
        $currentUser = Sentinel::getUser();
        $user = User::find($currentUser->id);

        $this->validate($request, [
            'email' => 'sometimes|required|email|unique:users,email,'.$currentUser->id,
            'first_name' => 'sometimes|required',
            'last_name' => 'sometimes|required'
        ]);
        
        $productData = array_merge($request->all(), ['branch_id' => $currentUser->branch_id]);

        if (isset($productData['password'])) {
            $productData['password'] = bcrypt($productData['password']);
        }

        return tap($user)->update($productData);
    }

    /**
     * Update the information of current user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function currentAvatar(Request $request, UserAvatarRequest $userAvatarRequest)
    {
        $currentUser = Sentinel::getUser();

        $subDomain = $request->route('account');
        $avatar = $request->file('avatar');
        $user = User::find($currentUser->id);
        $deleteAvatar = [];
        
        $name = $avatar->getClientOriginalName();
        $extension = File::extension($name);
        $uuidName = Uuid::uuid4().'.'.$extension;

        $sizes = [
            'thumbnail' => ['width' => 50, 'height' => 50],
            'medium' => ['width' => 300, 'height' => 300]
        ];

        foreach ($sizes as $fileFolderSize => $size) {
            $img = Image::make($avatar->getPathName());
            $canvas = Image::canvas($size['width'], $size['height']);
            $newImage = $img->resize($size['width'], $size['height'], function ($constraint) {
                $constraint->aspectRatio();
            });

            $canvas->insert($newImage, 'center');
            $canvas->stream();

            $folderPath = 'clients/'.$subDomain.'/avatar/'.$fileFolderSize.'/';
            $upload = Storage::disk('spaces')->put($folderPath.$uuidName, $canvas->__toString(), 'public');
            if ($user->avatar) {
                $originalName = $folderPath.$user->avatar;
                array_push($deleteAvatar, $originalName);
            }
        }

        $folderPath = 'clients/'.$subDomain.'/avatar/original/';
        $upload = Storage::disk('spaces')->put($folderPath.$uuidName, file_get_contents($avatar), 'public');

        if ($user->avatar) {
            $originalName = $folderPath.$user->avatar;
            array_push($deleteAvatar, $originalName);
        }
        
        Storage::disk('spaces')->delete($deleteAvatar);

        return tap($user)->update(['avatar' => $uuidName]);
    }

    /**
     * Create new account's user
     *
     * @param object $user
     * @param object $data
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    private function addAccountUser($user, $data, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $accountUser = AccountUser::where('email', $data['email'])->first();
        $account = \Config::get('vetlify.account');

        AccountUser::create([
            'email' => $data['email'],
            'account_id' => $account['id']
        ]);
    }

    /**
     * Update the account's user
     *
     * @param object $oldUser
     * @param object $newUser
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    private function updateAccountUser($oldUser, $newUser, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $account = \Config::get('vetlify.account');

        AccountUser::where([
            'email' => $oldUser->email,
            'account_id' => $account['id']
        ])
            ->update(['email' => $newUser['email']]);
    }

    private function deleteAccountUser($email, VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $account = \Config::get('vetlify.account');

        AccountUser::where([
            'email' => $email,
            'account_id' => $account['id']
        ])
            ->delete();
    }
}
