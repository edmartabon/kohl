<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/setting/announcements', 'TemplateController@announcement');
});
