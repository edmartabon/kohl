<?php

Route::group(['middleware' => ['SecurePage'], 'prefix' => 'api/accounts/{accounts}'], function () {
    Route::get('/patients', 'PatientController@index');
    Route::get('/patients/create', 'PatientController@create');
    Route::post('/patients', 'PatientController@store');
    Route::get('/patients/{id}', 'PatientController@show');
    Route::get('/patients/{id}/edit', 'PatientController@edit');
    Route::put('/patients/{id}', 'PatientController@update');
    Route::delete('/patients/{id}', 'PatientController@destroy');
    Route::post('/patients/{id}/avatar', 'PatientController@avatar');

    Route::get('/patients/{id}/procedures', 'PatientController@procedure');

    Route::get('/patients/{id}/files', 'PatientController@files');
    Route::post('/patients/{id}/files', 'PatientFileController@store');
    Route::put('/patients/{id}/files/{fileId}', 'PatientFileController@update');
    Route::delete('/patients/{id}/files/{fileId}', 'PatientFileController@destroy');

    Route::get('/patients/{id}/appointments', 'PatientAppointmentController@index');
    Route::get('/patients/{id}/appointments/create', 'PatientAppointmentController@create');
    Route::post('/patients/{id}/appointments', 'PatientAppointmentController@store');
    Route::get('/patients/{id}/appointments/{appointmentId}', 'PatientAppointmentController@show');
    Route::get('/patients/{id}/appointments/{appointmentId}/edit', 'PatientAppointmentController@edit');
    Route::put('/patients/{id}/appointments/{appointmentId}', 'PatientAppointmentController@update');
    Route::delete('/patients/{id}/appointments/{appointmentId}', 'PatientAppointmentController@destroy');

    Route::get('/appointments', 'AppointmentController@index');

    Route::get('/transactions', 'TransactionController@index');
    Route::get('/transactions/create', 'TransactionController@create');
    Route::post('/transactions', 'TransactionController@store');
    Route::get('/transactions/{id}', 'TransactionController@show');
    Route::get('/transactions/{id}/edit', 'TransactionController@edit');
    Route::put('/transactions/{id}', 'TransactionController@update');
    Route::delete('/transactions/{id}', 'TransactionController@destroy');

    Route::get('/treatment-plans', 'TransactionController@index');
    Route::get('/treatment-plans/create', 'TransactionController@create');
    Route::post('/treatment-plans', 'TransactionController@store');
    Route::get('/treatment-plans/{id}', 'TransactionController@show');
    Route::get('/treatment-plans/{id}/edit', 'TransactionController@edit');
    Route::put('/treatment-plans/{id}', 'TransactionController@update');
    Route::delete('/treatment-plans/{id}', 'TransactionController@destroy');
    
    Route::get('/breeds', 'BreedController@index');
    Route::get('/breeds/create', 'BreedController@create');
    Route::post('/breeds', 'BreedController@store');
    Route::get('/breeds/{id}', 'BreedController@show');
    Route::get('/breeds/{id}/edit', 'BreedController@edit');
    Route::put('/breeds/{id}', 'BreedController@update');
    Route::delete('/breeds/{id}', 'BreedController@destroy');
    
    Route::get('/species', 'SpeciesController@index');
    Route::get('/species/create', 'SpeciesController@create');
    Route::post('/species', 'SpeciesController@store');
    Route::get('/species/{id}', 'SpeciesController@show');
    Route::get('/species/{id}/edit', 'SpeciesController@edit');
    Route::put('/species/{id}', 'SpeciesController@update');
    Route::delete('/species/{id}', 'SpeciesController@destroy');

    Route::get('/payments', 'PaymentController@index');
    Route::get('/payments/create', 'PaymentController@create');
    Route::post('/payments', 'PaymentController@store');
    Route::get('/payments/{id}', 'PaymentController@show');
    Route::get('/payments/{id}/edit', 'PaymentController@edit');
    Route::put('/payments/{id}', 'PaymentController@update');
    Route::delete('/payments/{id}', 'PaymentController@destroy');

    Route::get('/clients', 'ClientController@index');
    Route::get('/clients/create', 'ClientController@create');
    Route::post('/clients', 'ClientController@store');
    Route::get('/clients/{id}', 'ClientController@show');
    Route::get('/clients/{id}/edit', 'ClientController@edit');
    Route::put('/clients/{id}', 'ClientController@update');
    Route::delete('/clients/{id}', 'ClientController@destroy');
    Route::post('/clients/{id}/avatar', 'ClientController@avatar');
    Route::post('/clients/{id}/invoice', 'ClientController@invoice');

    Route::get('/invoices', 'InvoiceController@index');
    Route::get('/invoices/create', 'InvoiceController@create');
    Route::post('/invoices', 'InvoiceController@store');
    Route::get('/invoices/{id}', 'InvoiceController@show');
    Route::get('/invoices/{id}/edit', 'InvoiceController@edit');
    Route::put('/invoices/{id}', 'Controller@update');
    Route::delete('/invoices/{id}', 'InvoiceController@destroy');
    Route::post('/invoices/{id}/avatar', 'InvoiceController@avatar');

    Route::get('/categories', 'InventoryCategoryController@index');
    Route::get('/categories/create', 'InventoryCategoryController@create');
    Route::post('/categories', 'InventoryCategoryController@store');
    Route::get('/categories/{id}', 'InventoryCategoryController@show');
    Route::get('/categories/{id}/edit', 'InventoryCategoryController@edit');
    Route::put('/categories/{id}', 'InventoryCategoryController@update');
    Route::delete('/categories/{id}', 'InventoryCategoryController@destroy');

    Route::get('/suppliers', 'InventorySupplierController@index');
    Route::get('/suppliers/create', 'InventorySupplierController@create');
    Route::post('/suppliers', 'InventorySupplierController@store');
    Route::get('/suppliers/{id}', 'InventorySupplierController@show');
    Route::get('/suppliers/{id}/edit', 'InventorySupplierController@edit');
    Route::put('/suppliers/{id}', 'InventorySupplierController@update');
    Route::delete('/suppliers/{id}', 'InventorySupplierController@destroy');

    Route::get('/products', 'InventoryProductController@index');
    Route::get('/products/create', 'InventoryProductController@create');
    Route::post('/products', 'InventoryProductController@store');
    Route::get('/products/{id}', 'InventoryProductController@show');
    Route::get('/products/{id}/edit', 'InventoryProductController@edit');
    Route::put('/products/{id}', 'InventoryProductController@update');
    Route::delete('/products/{id}', 'InventoryProductController@destroy');

    Route::get('/stocks', 'InventoryStockController@index');
    Route::get('/stocks/create', 'InventoryStockController@create');
    Route::post('/stocks', 'InventoryStockController@store');
    Route::get('/stocks/{id}', 'InventoryStockController@show');
    Route::get('/stocks/{id}/edit', 'InventoryStockController@edit');
    Route::put('/stocks/{id}', 'InventoryStockController@update');
    Route::delete('/stocks/{id}', 'InventoryStockController@destroy');

    Route::get('/branches', 'BranchController@index');
    Route::get('/branches/create', 'BranchController@create');
    Route::post('/branches', 'BranchController@store');
    Route::get('/branches/{id}', 'BranchController@show');
    Route::get('/branches/{id}/edit', 'BranchController@edit');
    Route::put('/branches/{id}', 'BranchController@update');
    Route::delete('/branches/{id}', 'BranchController@destroy');

    Route::get('/users', 'UserController@index');
    Route::get('/users/create', 'UserController@create');
    Route::post('/users', 'UserController@store');
    Route::get('/users/{id}', 'UserController@show');
    Route::get('/users/{id}/edit', 'UserController@edit');
    Route::put('/users/{id}', 'UserController@update');
    Route::delete('/users/{id}', 'UserController@destroy');
    Route::post('/users/current', 'UserController@current');
    Route::post('/users/current/avatar', 'UserController@currentAvatar');

    Route::get('/compliants', 'CompliantController@index');
    Route::get('/compliants/create', 'CompliantController@create');
    Route::post('/compliants', 'CompliantController@store');
    Route::get('/compliants/{id}', 'CompliantController@show');
    Route::get('/compliants/{id}/edit', 'CompliantController@edit');
    Route::put('/compliants/{id}', 'CompliantController@update');
    Route::delete('/compliants/{id}', 'CompliantController@destroy');

    Route::get('/notes', 'NoteController@index');
    Route::get('/notes/create', 'NoteController@create');
    Route::post('/notes', 'NoteController@store');
    Route::get('/notes/{id}', 'NoteController@show');
    Route::get('/notes/{id}/edit', 'NoteController@edit');
    Route::put('/notes/{id}', 'NoteController@update');
    Route::delete('/notes/{id}', 'NoteController@destroy');

    Route::get('/prescriptions', 'PrescriptionController@index');
    Route::get('/prescriptions/create', 'PrescriptionController@create');
    Route::post('/prescriptions', 'PrescriptionController@store');
    Route::get('/prescriptions/{id}', 'PrescriptionController@show');
    Route::get('/prescriptions/{id}/edit', 'PrescriptionController@edit');
    Route::put('/prescriptions/{id}', 'PrescriptionController@update');
    Route::delete('/prescriptions/{id}', 'PrescriptionController@destroy');

    Route::get('/recommendations', 'RecommendationController@index');
    Route::get('/recommendations/create', 'RecommendationController@create');
    Route::post('/recommendations', 'RecommendationController@store');
    Route::get('/recommendations/{id}', 'RecommendationController@show');
    Route::get('/recommendations/{id}/edit', 'RecommendationController@edit');
    Route::put('/recommendations/{id}', 'RecommendationController@update');
    Route::delete('/recommendations/{id}', 'RecommendationController@destroy');

    Route::get('/laboratories', 'LaboratoryController@index');
    Route::get('/laboratories/create', 'LaboratoryController@create');
    Route::post('/laboratories', 'LaboratoryController@store');
    Route::get('/laboratories/{id}', 'LaboratoryController@show');
    Route::get('/laboratories/{id}/edit', 'LaboratoryController@edit');
    Route::put('/laboratories/{id}', 'LaboratoryController@update');
    Route::delete('/laboratories/{id}', 'LaboratoryController@destroy');

    Route::get('/diagnoses', 'DiagnoseController@index');
    Route::get('/diagnoses/create', 'DiagnoseController@create');
    Route::post('/diagnoses', 'DiagnoseController@store');
    Route::get('/diagnoses/{id}', 'DiagnoseController@show');
    Route::get('/diagnoses/{id}/edit', 'DiagnoseController@edit');
    Route::put('/diagnoses/{id}', 'DiagnoseController@update');
    Route::delete('/diagnoses/{id}', 'DiagnoseController@destroy');

    Route::get('/investigations', 'InvestigationController@index');
    Route::get('/investigations/create', 'InvestigationController@create');
    Route::post('/investigations', 'InvestigationController@store');
    Route::get('/investigations/{id}', 'InvestigationController@show');
    Route::get('/investigations/{id}/edit', 'InvestigationController@edit');
    Route::put('/investigations/{id}', 'InvestigationController@update');
    Route::delete('/investigations/{id}', 'InvestigationController@destroy');

    Route::get('/procedures', 'ProcedureController@index');
    Route::get('/procedures/create', 'ProcedureController@create');
    Route::post('/procedures', 'ProcedureController@store');
    Route::get('/procedures/{id}', 'ProcedureController@show');
    Route::get('/procedures/{id}/edit', 'ProcedureController@edit');
    Route::put('/procedures/{id}', 'ProcedureController@update');
    Route::delete('/procedures/{id}', 'ProcedureController@destroy');

    Route::get('/veterinarians', 'VeterinarianController@index');
    Route::get('/veterinarians/create', 'VeterinarianController@create');
    Route::post('/veterinarians', 'VeterinarianController@store');
    Route::get('/veterinarians/{id}', 'VeterinarianController@show');
    Route::get('/veterinarians/{id}/edit', 'VeterinarianController@edit');
    Route::put('/veterinarians/{id}', 'VeterinarianController@update');
    Route::delete('/veterinarians/{id}', 'VeterinarianController@destroy');

    Route::get('/employees', 'EmployeeController@index');
    Route::get('/employees/create', 'EmployeeController@create');
    Route::post('/employees', 'EmployeeController@store');
    Route::get('/employees/{id}', 'EmployeeController@show');
    Route::get('/employees/{id}/edit', 'EmployeeController@edit');
    Route::put('/employees/{id}', 'EmployeeController@update');
    Route::delete('/employees/{id}', 'EmployeeController@destroy');

    Route::get('/doctors', 'VeterinarianController@index');
    Route::get('/doctors/create', 'VeterinarianController@create');
    Route::post('/doctors', 'VeterinarianController@store');
    Route::get('/doctors/{id}', 'VeterinarianController@show');
    Route::get('/doctors/{id}/edit', 'VeterinarianController@edit');
    Route::put('/doctors/{id}', 'VeterinarianController@update');
    Route::delete('/doctors/{id}', 'VeterinarianController@destroy');

    Route::get('/announcements', 'AnnouncementController@index');
    Route::get('/announcements/create', 'AnnouncementController@create');
    Route::post('/announcements', 'AnnouncementController@store');
    Route::get('/announcements/{id}', 'AnnouncementController@show');
    Route::get('/announcements/{id}/edit', 'AnnouncementController@edit');
    Route::put('/announcements/{id}', 'AnnouncementController@update');
    Route::delete('/announcements/{id}', 'AnnouncementController@destroy');

    Route::get('/payment-methods', 'PaymentMethodController@index');
});
