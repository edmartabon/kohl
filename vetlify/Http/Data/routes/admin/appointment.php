<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/appointments', 'TemplateController@appointment');
});
