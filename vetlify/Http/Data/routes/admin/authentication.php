<?php

Route::group(['middleware' => ['LoginChecker']], function () {
    Route::get('/login', 'AuthenticationController@loginPage');
    Route::get('/register', 'AuthenticationController@registerPage');

    Route::post('/login', 'AuthenticationController@loginProcess');
    Route::post('/register', 'AuthenticationController@registerProcess');
});

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/logout', 'AuthenticationController@logout');
});
