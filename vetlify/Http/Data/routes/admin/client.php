<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/patients', 'TemplateController@patientList')->middleware(['ValidateRole:sec.read, vet.read']);
    Route::get('/patients/profile/{id}', 'TemplateController@patientProfile')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/procedure', 'TemplateController@patientProfileProcedure')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/procedure/create', 'TemplateController@patientProfileProcedureCreate')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/procedure/{procedureId}', 'TemplateController@patientProfileProcedureUpdate')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/treatments', 'TemplateController@patientProfileTreatment')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/treatments/create', 'TemplateController@patientProfileTreatmentPlanCreate')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/treatments/{transactionId}', 'TemplateController@patientProfileTreatmentPlanUpdate')->middleware('ValidateRole:sec.read');

    Route::get('/patients/{id}/payments', 'TemplateController@patientProfilePayment')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/payments/create', 'TemplateController@patientProfilePaymentCreate')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/payments/{transactionId}', 'TemplateController@patientProfilePaymentUpdate')->middleware('ValidateRole:sec.read');

    Route::get('/patients/{id}/prescriptions', 'TemplateController@patientProfilePrescription')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/files', 'TemplateController@patientProfileFiles')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/billings', 'TemplateController@patientProfileBilling')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/appointments', 'TemplateController@patientProfileAppointment')->middleware('ValidateRole:sec.read');
    Route::get('/patients/{id}/setting', 'TemplateController@patientProfileSetting')->middleware('ValidateRole:sec.read');

    Route::get('/clients/profile/{id}', 'TemplateController@clientProfile')->middleware('ValidateRole:sec.read');
    Route::get('/clients/{id}/setting', 'TemplateController@clientProfileSetting')->middleware('ValidateRole:sec.read');
});
