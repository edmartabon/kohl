<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/', function () {
        return view('Vetlify::pages.admin.dashboard');
    });
});
