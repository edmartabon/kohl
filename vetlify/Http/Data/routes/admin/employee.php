<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/employees', 'TemplateController@employeeTimeIn');
});
