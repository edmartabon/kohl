<?php

Route::get('/', function () {
    return view('Vetlify::layouts.master1');
});

Route::get('/app', function() {
    return redirect('/app/login');
});

Route::get('/login', function() {
    return redirect('/app/login');
});

Route::group(['prefix' => 'ext'], function() {
    Route::get('/api/branches', 'BranchController@index')->middleware('cors');
    Route::get('/api/veterinarians', 'VeterinarianController@index')->middleware('cors');
    Route::get('/api/announcements', 'AnnouncementController@index')->middleware('cors');
});
