<?php

Route::group(['middleware' => ['SecurePage'], 'prefix' => 'inventory'], function () {
    Route::get('/products', 'TemplateController@inventoryProductList');
    Route::get('/suppliers', 'TemplateController@inventorySupplier');
    Route::get('/categories', 'TemplateController@inventoryCategory');
    Route::get('/stocks-control', 'TemplateController@inventoryStockControl');
});
