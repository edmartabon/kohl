<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/invoices', 'TemplateController@invoice');
});
