<?php

Route::group(['middleware' => ['SecurePage'], 'prefix' => 'setting'], function () {
    Route::get('/branches', 'TemplateController@settingBranch');
    Route::get('/branch-info', 'TemplateController@settingBranchInfo');
    Route::get('/users', 'TemplateController@settingUser');
    Route::get('/profile', 'TemplateController@settingCurrentUser');
});
