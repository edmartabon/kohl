<?php

Route::get('/', function () {
    // return view('Vetlify::pages.pages.index');
});

Route::get('login', 'RegisterAccountController@login');
Route::post('/login/home', 'RegisterAccountController@loginProcess');
Route::get('register', 'RegisterAccountController@registerView');
Route::post('register', 'RegisterAccountController@register');
