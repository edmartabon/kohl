<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ Config::get('vetlify.page_title') }} - @yield('page_title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/css/font-awesome.min.css">
  <!-- Daterangerpicker -->
  <link rel="stylesheet" href="/css/daterangepicker.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/css/ionicons.min.css">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  @yield('stylesheet')
  <!-- Theme style -->
  <link rel="stylesheet" href="/css/adminlte.min.css">
  <link rel="stylesheet" href="/css/skin-blue.min.css">

  <!-- Vetlify style -->
  <link rel="stylesheet" href="/css/style.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

@isset ($expiredNotification)
<div class="alert alert-warning fade in alert-dismissible expired-subscription-alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Warning!</strong> Your Vetlify account will expired on {{ $expiredNotification['expired_at'] }}. 
</div>
@endisset

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="/{{ Config::get('vetlify.app_route_prefix') }}/appointments" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>V</b>et</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>V</b>etlify</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger appointment-notification"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"></li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu notification-item">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ $user->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/avatar/medium/'.$user->avatar : '/images/avatar/thumbnail/noavatar.png' }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ $user->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/avatar/medium/'.$user->avatar : '/images/avatar/thumbnail/noavatar.png' }}" class="img-circle" alt="User Image">
                
                <p>
                  {{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}
                  <small>{{ !empty($user->roles) ? $user->roles[0]->name : 'No Role' }}</small>
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="row">
                  <div class="col-xs-4">
                    <a href="/{{ Config::get('vetlify.app_route_prefix') }}/setting/profile" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <!-- <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-branch-select">Clinic</a> -->
                  </div>
                  <div class="col-xs-4">
                    <a href="/{{ Config::get('vetlify.app_route_prefix') }}/logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="/{{ Config::get('vetlify.app_route_prefix') }}/appointments">
            <i class="fa fa-calendar"></i> <span>Appointment</span>
          </a>
        </li>
        <li>
          <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">
            <i class="fa fa-wheelchair"></i>
            <span>Patients</span>
          </a>
        </li>
        <li>
          <a href="/{{ Config::get('vetlify.app_route_prefix') }}/invoices">
            <i class="fa fa-shopping-cart"></i>
            <span>Invoice</span>
          </a>
        </li>
        <!-- <li>
          <a href="/{{ Config::get('vetlify.app_route_prefix') }}/employees">
            <i class="fa fa-clock-o"></i>
            <span>Employee</span>
          </a>
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i> <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/products"><i class="fa fa-circle-o"></i> Products</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/suppliers"><i class="fa fa-circle-o"></i> Supplier</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/categories"><i class="fa fa-circle-o"></i> Category</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/stocks-control"><i class="fa fa-circle-o"></i> Stock Control</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!-- <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/sms"><i class="fa fa-circle-o"></i> SMS</a></li> -->
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/setting/users"><i class="fa fa-circle-o"></i> Users</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/setting/branch-info"><i class="fa fa-circle-o"></i> Clinic</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/setting/announcements"><i class="fa fa-circle-o"></i> Announcements</a></li>
            <!-- <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/billings"><i class="fa fa-circle-o"></i> Billing</a></li> -->
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright &copy; 2018 <a href="http://vetlify.com">Vetlify.com</a>.</strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-category-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Category</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control modal-category-create">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-category-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-supplier-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Supplier</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="company_name" class="col-sm-2">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="code" class="col-sm-2">Supplier Code</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="code" name="code" placeholder="Supplier Code...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="description" class="col-sm-2">Description</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
            </div>
          </div>

          <div class="form-group">
            <label for="first_name" class="col-sm-2">Contact Name</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
            </div>

            <div class="col-sm-5">
              <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="description" class="col-sm-2">Description</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
            </div>
          </div>

          <div class="form-group">
            <label for="address" class="col-sm-2">Address</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address" name="address" placeholder="Address...">
            </div>
          </div>

          <div class="form-group">
            <label for="email" class="col-sm-2">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="email" name="email" placeholder="Email...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="phone" class="col-sm-2">Contact</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone...">
            </div>

            <div class="col-sm-5">
              <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile...">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-supplier-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-procedure-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Procedure</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="name" class="col-sm-2">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="name" name="name" placeholder="Product Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="retail_price" class="col-sm-2">Price</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="retail_price" name="retail_price" placeholder="Retail Price...">
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-procedure-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-product-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Product</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="name" class="col-sm-2">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="name" name="name" placeholder="Product Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="item_code" class="col-sm-2">Item Code</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="item_code" name="item_code" placeholder="Item Code...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="reorder_point" class="col-sm-2">Reorder Point</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="reorder_point" name="reorder_point" placeholder="Reorder Point...">
            </div>
          </div>

          <div class="form-group">
            <label for="retail_price" class="col-sm-2">Retail Price</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="retail_price" name="retail_price" placeholder="Retail Price...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="description" class="col-sm-2">Description</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
            </div>
          </div>

          <div class="form-group">
            <label for="inventory_count" class="col-sm-2">Inventory Count</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inventory_count" name="inventory_count" placeholder="Inventory Count...">
            </div>
          </div>

          <div class="form-group">
            <label for="category_id" class="col-sm-2">Category</label>

            <div class="col-sm-10">
              <select class="form-control" id="category_id" name="category_id">
              </select>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-product-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-announcement-create">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Announcement</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="name" class="col-sm-2">Title</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="title" placeholder="Title...">
            </div>
          </div>

          <div class="form-group">
            <label for="reorder_point" class="col-sm-2">Address</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="address" placeholder="Address...">
            </div>
          </div>
 
          <div class="form-group">
            <label for="item_code" class="col-sm-2">Content</label>
            <div class="col-sm-10">
              <textarea class="form-control content-announcement" name="body" placeholder="Content..."></textarea>
            </div>
          </div>          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-announcement-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-client-create" style="z-index:1500">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Client</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
        <div class="form-group">
            <label for="first_name" class="col-sm-2">First Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="last_name" class="col-sm-2">Last Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="phone_no" class="col-sm-2">Mobile No.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Mobile No...">
            </div>
          </div>

          <div class="form-group">
            <label for="telephone_no" class="col-sm-2">Telephone No</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="telephone_no" name="telephone_no" placeholder="Telephone No...">
            </div>
          </div>

          <div class="form-group">
            <label for="address_one" class="col-sm-2">Address</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address_one" name="address_one" placeholder="Address...">
            </div>
          </div>

          <div class="form-group hide">
            <label for="address_two" class="col-sm-2">Address Two</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address_two" name="address_two" placeholder="Address Two...">
            </div>
          </div>

          <div class="form-group">
            <label for="rfid" class="col-sm-2">RFID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="rfid" name="rfid" placeholder="RFID...">
            </div>
          </div>          
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-client-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-patient-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Patient</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="first_name" class="col-sm-2">Pet's Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
            </div>
          </div>

          <div class="form-group hide">
            <label for="first_name" class="col-sm-2">Last Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="patient_record_id" class="col-sm-2">Patient Record No.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="patient_record_id" name="patient_record_id" placeholder="Patient Record No...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="rfid" class="col-sm-2">RFID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="rfid" name="rfid" placeholder="RFID...">
            </div>
          </div>

          <div class="form-group">
            <label for="age" class="col-sm-2">Age</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="age" name="age" placeholder="Age...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="client_id" class="col-sm-2">Owner</label>

            <div class="col-sm-10">
              <select class="form-control" id="client_id" name="client_id" style="width: 100%"></select>
              <div class="form-horizontal">
              &nbsp; <a href="#" class="add-client" data-toggle="modal" data-target="#modal-client-create"> Add Client</a>
              </div>
            </div>
          </div>

          <div class="form-group hide">
            <label for="rfid" class="col-sm-2">Birth Date</label>
            <div class="col-sm-10">
              <input type="text" class="form-control create-patient-birthdate" id="birth_date" name="birth_date" />
            </div>
          </div>

          <div class="form-group hide">
            <label for="patient_blood_id" class="col-sm-2">Blood</label>

            <div class="col-sm-10">
              <select class="form-control" id="patient_blood_id" name="patient_blood_id">
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="rfid" class="col-sm-2">Color</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="color" name="color" placeholder="Color...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="rfid" class="col-sm-2">Weight</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight...">
            </div>

            <div class="col-sm-5">
              <select class="form-control" name="weight_unit">
                <option value="Lbs">lbs</option>
                <option value="Kg">kg</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="rfid" class="col-sm-2">Markings</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="markings" name="markings" placeholder="Markings...">
            </div>
          </div>

          <div class="form-group">
            <label for="patient_gender_id" class="col-sm-2">Species</label>

            <div class="col-sm-10">
              <select class="form-control" id="species_id" name="species_id" style="width: 100%">
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="patient_gender_id" class="col-sm-2">Breed</label>

            <div class="col-sm-10">
              <select class="form-control" id="breed_id" name="breed_id" style="width: 100%">
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="patient_gender_id" class="col-sm-2">Gender</label>

            <div class="col-sm-10">
              <select class="form-control" id="patient_gender_id" name="patient_gender_id">
              </select>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-patient-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-stock-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Stock</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="product_id" class="col-sm-2">Product</label>
            <div class="col-sm-10">
              <select class="form-control" id="product_id" name="product_id">
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="quantity" class="col-sm-2">Quantity</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="batch" class="col-sm-2">Batch</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="batch" name="batch" placeholder="Batch...">
            </div>
          </div>

          <div class="form-group">
            <label for="unit_cost" class="col-sm-2">Unit Cost</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="unit_cost" name="unit_cost" placeholder="Unit Cost...">
            </div>
          </div>

          <div class="form-group">
            <label for="supplier_id" class="col-sm-2">Supplier</label>

            <div class="col-sm-10">
              <select class="form-control" id="supplier_id" name="supplier_id">
              </select>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-stock-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-branch-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Branch</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Branch Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="address_one" class="col-sm-2">Address</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address_one" name="address_one" placeholder="Address...">
                </div>
              </div>
              
              <div class="form-group hide">
                <label for="address_two" class="col-sm-2">Address Two</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address_two" name="address_two" placeholder="Address Two...">
                </div>
              </div>

              <div class="form-group">
                <label for="contact_no" class="col-sm-2">Contact No</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="phone_no" class="col-sm-2">Mobile No.</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No...">
                </div>
              </div>
              
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-branch-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-user-create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create User</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="first_name" class="col-sm-2">First Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
            </div>
          </div>

          <div class="form-group">
            <label for="last_name" class="col-sm-2">Last Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
            </div>
          </div>
          
          <div class="form-group">
            <label for="address_one" class="col-sm-2">Address</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address_one" name="address_one" placeholder="Address...">
            </div>
          </div>

          <div class="form-group hide">
            <label for="address_two" class="col-sm-2">Address Two</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="address_two" name="address_two" placeholder="Address Two...">
            </div>
          </div>

          <div class="form-group">
            <label for="phone_no" class="col-sm-2">Mobile No</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No...">
            </div>
          </div>

          <div class="form-group">
            <label for="telephone_no" class="col-sm-2">Telephone No.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="telephone_no" name="telephone_no" placeholder="Telephone No....">
            </div>
          </div>

          <div class="form-group">
            <label for="role_id" class="col-sm-2">Role</label>

            <div class="col-sm-10">
              <select class="form-control" id="role_id" name="role_id">
              </select>
            </div>
          </div>

          <hr class="divider">

          <div class="form-group">
            <label for="email" class="col-sm-2">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="email" name="email" placeholder="Email...">
            </div>
          </div>

          <div class="form-group">
            <label for="password" class="col-sm-2">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="password" name="password" placeholder="Password...">
            </div>
          </div>
          
        </form>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-user-create">Create</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-branch-select">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Switch Clinic</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="name" class="col-sm-2">Name</label>
            <div class="col-sm-10">
              <select class="form-control" id="name" name="name">
              </select>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-md-select-create">Select</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="show-payment">
  <div class="modal-dialog modal-lg">
      <section class="invoice patient-payment">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> AdminLTE, Inc.
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #007612</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped transaction-item">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Product/Procedure</th>
              <th>Cost</th>
              <th>Discount</th>
              <th>Total</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <select class="form-control method-type">
            
          </select>

        </div>
        <!-- /.col -->
        <div class="col-xs-6 amount-total">
          <p class="lead">Amount</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td class="amt-subtotal">0.00</td>
              </tr>
              <tr>
                <th>Tax</th>
                <td class="amt-tax">0.00</td>
              </tr>
              <tr>
                <th>Discount:</th>
                <td class="amt-discount">0.00</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td class="amt-total">0.00</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">
            <i class="fa fa-remove"></i> Close
          </button>
          <button type="button" class="btn btn-default print-model-payment"><i class="fa fa-print"></i> Print</button>
          <button type="button" class="btn btn-success pull-right submit-payment" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right generate-payment" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
  </div>
  <!-- /.modal-content -->
</div>

<div class="modal fade" id="show-invoice">
  <div class="modal-dialog modal-lg">
    <section class="invoice patient-invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> AdminLTE, Inc.
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive invoice-item-list">
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <select class="form-control method-type">
            
          </select>

        </div>
        <!-- /.col -->
        <div class="col-xs-6 amount-total">
          <p class="lead">Amount</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td class="amt-subtotal">0.00</td>
              </tr>
              <tr>
                <th>Tax</th>
                <td class="amt-tax">0.00</td>
              </tr>
              <tr>
                <th>Discount:</th>
                <td class="amt-discount">0.00</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td class="amt-total">0.00</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">
            <i class="fa fa-remove"></i> Close
          </button>
          <button type="button" class="btn btn-default print-model-invoice"><i class="fa fa-print"></i> Print</button>
          <button type="button" class="btn btn-success pull-right submit-invoice" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <!-- <button type="button" class="btn btn-primary pull-right generate-payment" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button> -->
        </div>
      </div>
    </section>
  </div>
  <!-- /.modal-content -->
</div>

<script type="text/javascript">
var globalConfig = {
  csrf: "{{ csrf_token() }}",
  linkApp: "{{ Config::get('vetlify.app_route_prefix') }}",
  user: {!! $user !!},
  awsPUrl: "{{ Config::get('vetlify.aws_public_url') }}",
  appName: "{{ Request::route('account') }}",
};
</script>
<!-- jQuery 3 -->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!-- Moment 2.18.1 -->
<script type="text/javascript" src="/js/moment.min.js"></script>
<!-- Moment 2.15.1 -->
<script type="text/javascript" src="/js/daterangepicker.min.js"></script>
<!-- FastClick -->
<script type="text/javascript" src="/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="/js/adminlte.min.js"></script>
<!-- Bootstrap Notify -->
<script type="text/javascript" src="/js/bootstrap-notify.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="/js/demo.js"></script>
<!-- Vetlify script -->
<script type="text/javascript" src="/js/admin.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

@yield('script')
</body>
</html>
