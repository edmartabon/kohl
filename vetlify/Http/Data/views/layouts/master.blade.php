<!-- 
======================================================= 

    Proudly hand coded by Bizwex!
    
    "It's better to create a single, high-quality
    beautiful product that a thousand people
    will love, rather than create many
    mediocre products that a thousand 
    people will hate."

                                    - Your's truly
    
    http://www.bizwex.com

=======================================================
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <meta name="robots" content="index">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="A web-based veterinary clinic management program designed to make managing your veterinary clinic easy, intuitive and fun.">

        <meta property="og:title" content="Vetlify - Veterinary Clinic Management System">
        <!-- <meta property="og:type" content=""> -->
        <meta property="og:url" content="http://www.vetlify.com">
        <meta property="og:image" content="images/vt.svg">
        <meta property="og:site_name" content="Vetlify">
        <meta property="og:description" content="A web-based veterinary clinic management program designed to make managing your veterinary clinic easy, intuitive and fun.">

        <title>@yield('page-title')</title>
        
        <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
        <link rel="manifest" href="images/favicons/manifest.json">
        <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- 
        ======================================================= 
            Google Fonts
        =======================================================
        -->
        <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light+Two" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        
        <!-- 
        ======================================================= 
            Mobile menu button styles
        =======================================================
        -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.3/hamburgers.min.css">

        <!-- 
        ======================================================= 
            Icon Fonts
        =======================================================
        -->
        <link href="https://file.myfontastic.com/rDWav9abomVBLk97x2kJcM/icons.css" rel="stylesheet">

        <!-- 
        ======================================================= 
            Owl Carousel Styles
        =======================================================
        -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

        <!-- 
        ======================================================= 
            Animate CSS
        =======================================================
        -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

        <!-- 
        ======================================================= 
            Magnific Popup
        =======================================================
        -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js">

        <!-- 
        ======================================================= 
            Page Styles
        =======================================================
        -->
        <link rel="stylesheet" type="text/css" href="css/home.css">

        <!-- 
        ======================================================= 
            Responsive Styles
        =======================================================
        -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
        
    </head>
    <body>
        <div class="page-container">

            <!-- 
            ======================================================= 
                Header/Sidebar Navigation
            =======================================================
            -->
            <header class="header-nav">

                <!-- 
                ======================================================= 
                    Mobile Navigation Button
                =======================================================
                -->
                <button id="mobile-nav-btn" class="hamburger hamburger--slider" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
                
                <!-- 
                ======================================================= 
                    Website Logo
                =======================================================
                -->
                <div class="logo">
                    <img src="images/vt.svg" alt="Vetlify Logo">
                    <p>Veterinary Clinic Management Simplified!</p>
                </div>
                
                <!-- 
                ======================================================= 
                    Main Navigation Links
                =======================================================
                -->
                <nav id="main-nav">
                    <ul>
                        <li><a id="intro-link" href="#introducing" class="current">Home page <i class="icon-angle-right"></i></a></li>
                        <li><a id="why-link" href="#features">Features <i class="icon-angle-right"></i></a></li>
                        <li><a id="screenshot-link" href="#screenshots">Screenshots <i class="icon-angle-right"></i></a></li>
                        <li><a id="contact-link" class="contact-link" href="#contact-us">Contact <i class="icon-angle-right"></i></a></li>
                        <li><a href="/register">Register <i class="icon-angle-right"></i></a></li>
                        <li><a href="/login" class="login">Login <i class="icon-lock-locker-streamline"></i></a></li>
                    </ul>
                </nav>
                
                <!-- 
                ======================================================= 
                    Copyright Stuff
                =======================================================
                -->
                <div class="copyright">
                    <p>Copyright &copy; 2017 <a href="http://bizwex.com">Bizwex</a>.</p>
                </div>
            </header>

            <!-- 
            ======================================================= 
                Page Body
            =======================================================
            -->
            <section class="body">
                @yield('contents')
            </section>

            <!-- 
            ======================================================= 
                Back to Top Link
            =======================================================
            -->
            <a href="#" id="top"><i class="icon-angle-up"></i></a>
        </div>

        <!-- 
        ======================================================= 
            Javascripts
        =======================================================
        -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.compatibility.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/noframework.waypoints.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.1/masonry.pkgd.min.js"></script>
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyAgdj241ot74QjBaOQ49sUgmCmko022uYk"></script>
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dynamics.js/1.1.5/dynamics.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cyclejs-core/7.0.0/cycle.min.js"></script>

        <!-- 
        ======================================================= 
            Main Page Script
        =======================================================
        -->
        <script src="js/main.js"></script>

        <!-- 
        ======================================================= 
            Google Map Script
        =======================================================
        -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js"></script>

        <!-- 
        ======================================================= 
            Add your script here...
        =======================================================
        -->
    </body>
</html>
