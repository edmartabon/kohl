@extends('Vetlify::layouts.admin')

@section('page_title', 'Announcements')
@section('stylesheet')
  <link href="/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Announcement
        <small>List of announcements</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/announcement">Announcement</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-lg-8 col-centered">
          <div class="box">
            <div class="box-header with-border">
              <div class="pull-left">
                <h3 class="box-title">Announcement List</h3>
              </div>
              <div class="pull-right">
                <a href="#" class="btn btn-primary btn-block margin-bottom" data-toggle="modal" data-target="#modal-announcement-create">Create Announcement</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <div class="loader"></div>
              <div class="table-responsive">
                <table class="table table-bordered table-view">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Address</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="5">No announcement to show.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-announcement-update">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Announcement</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal update-announcement-form">
              <div class="form-group">
                <label for="name" class="col-sm-2">Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="title" placeholder="Title...">
                </div>
              </div>

              <div class="form-group">
                <label for="reorder_point" class="col-sm-2">Address</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="address" placeholder="Address...">
                </div>
              </div>
    
              <div class="form-group">
                <label for="item_code" class="col-sm-2">Content</label>
                <div class="col-sm-10">
                  <textarea class="form-control content-announcement" name="body" placeholder="Content..."></textarea>
                </div>
              </div>          
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="btn-md-announcement-update">Create</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-edit-aanouncement">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Product</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Product Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="item_code" class="col-sm-2">Item Code</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="item_code" name="item_code" placeholder="Item Code...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="reorder_point" class="col-sm-2">Reorder Point</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="reorder_point" name="reorder_point" placeholder="Reorder Point...">
                </div>
              </div>

              <div class="form-group">
                <label for="retail_price" class="col-sm-2">Retail Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="retail_price" name="retail_price" placeholder="Retail Price...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
                </div>
              </div>

              <div class="form-group">
                <label for="inventory_count" class="col-sm-2">Inventory Count</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inventory_count" name="inventory_count" placeholder="Inventory Count...">
                </div>
              </div>

              <div class="form-group">
                <label for="category_id" class="col-sm-2">Category</label>

                <div class="col-sm-10">
                  <select class="form-control" id="category_id" name="category_id">
                  </select>
                </div>
              </div>
              
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  function Product() {
    this.categories = {};
  }

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/announcements',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');
    if (data && data.length > 0) {

      data.forEach(item => {
        delete item['body'];
        viewTable.append(`
          <tr>
            <td id="title">` + item.title + `</td>
            <td id="address">` +item.address + `</td>
            <td style="display: inline">
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-announcement-update"><i class="fa fa-eye"></i> Edit</button>
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-reset', function() {
    filterItem = {};
    filter();
  });

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '.filter-gender', function() {
    filterItem['category'] = $(this).data('id');
    filter();
  });

  $(document).on('click', '#btn-md-announcement-create', function() {
    vetlify.announcementCreate(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td id="name">` + data.title + `</td>
          <td id="reorder_point">` + data.address + `</td>
          <td style="display: inline">
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-announcement-update"><i class="fa fa-eye"></i> Edit</button>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {

    })
  });
  
  $(document).on('click', '.btn-edit', function() {
    var content = $(this).data('obj');
    var index = $(this).parent().parent().index();
    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/announcements/' +content.id, 
      success: function(data) {
        var par = $('.update-announcement-form')
        par.find('input[name="title"]').val(data.title)
        par.find('input[name="address"]').val(data.address)
        par.find('.content-announcement').parent().html(`<textarea class="form-control content-announcement" name="body" placeholder="Content..."></textarea>`)
        par.find('.content-announcement').val(data.body)
        par.find('.content-announcement').wysihtml5();
        $('#btn-md-announcement-update').data('obj', content)
        $('#btn-md-announcement-update').data('index', index)
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
    
    $('.btn-edit-confirm').data('index', $(this).parent().parent().index());
  });

  $(document).on('click', '#btn-md-announcement-update', function() {
    var _this = $(this).data('obj');
    var button = vetlify.button('.btn-edit-confirm');
    var index = $(this).data('index');
    var formData = $('.update-announcement-form');
    var formRequest = {}
    button.enableButton(true)
      .loadingButton(true);

    formRequest = {
      title: formData.find('input[name="title"]').val(),
      address: formData.find('input[name="address"]').val(),
      body: formData.find('textarea[name="body"]').val(),
    }

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/announcements/' + _this.id,
      data: formRequest,
      success: function(data) {
        delete data['body'];
        button.enableButton(false)
          .loadingButton(false);

        vetlify.tableAutoFill('.table-view', data, index);
        $('#modal-edit-product').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj'));
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/announcements/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  var product = new Product();
  filter();

  $('.content-announcement').wysihtml5();
  
});
</script>
@endsection