@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Files')

@section('stylesheet')

@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employee
        <small>List of information about the employee</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/employee">Employee</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Employee List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <div class="loader"></div>
              <table class="table table-hover custom-table table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Patient Name</th>
                    <th>Owner</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="5">No client to show.</td>
                    </tr>
                </tbody>
              </table>
            </div>

            <div class="box-footer clearfix">
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </section>
    <!-- /.content -->

@endsection


@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  var bloodLists = {},
    genderLists = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/employees',
      data: $.param(filterItem),
      success: function(data) {
        console.log(data)
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>${(data.per_page *(data.current_page-1)+(num)+ 1)}.</td>
            <td class="patient_record_id">${item.patient_record_id}</td>
            <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/${item.patient_id}/procedure">${item.patient_first_name} ${item.patient_last_name}</a></td>
            <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/profile/${item.client_id}">${item.client_first_name} ${item.client_last_name}</a></td>
            <td>
              <button type="button" data-id="${item.patient_id}" class="btn btn-primary btn-sm btn-view"><i class="fa fa-eye"></i> View</button>
              <button type="button" data-id="${item.patient_id}" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
          
        num++;
      });
    }
    
  }

  const datePicker = () => {
    var start = moment().subtract(29, 'days');
    var end = moment();

    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end
    }, (start, end) => {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      filterItem['start'] = start.format('YYYY-MM-DD');
      filterItem['end'] = end.format('YYYY-MM-DD');
      filter();
    });
  } 

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  datePicker();
  filter();
});
</script>
@endsection