@extends('Vetlify::layouts.admin')

@section('page_title', 'Categories')

@section('content')
<!-- Content Header (Page header) -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <section class="content-header">
        <h1>
            Categories
            <small>List of categories</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/catergories">Categories</a></li>
        </ol>
        </section>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6 col-md-offset-3">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-category-create">Create Category</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="loader"></div>
              <table class="table table-bordered table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="2">No category to show.</td>
                    </tr>
                </tbody>
              </table>

              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal-edit">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Category</h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control modal-edit-input">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this categories?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/categories',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td>` + item.name + `</td>
            <td>
              <button type="button" data-id="` + item.id + `" data-name="` + item.name + `" class="btn btn-primary btn-sm btn-view" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-eye"></i> Edit</button>
              <button type="button" data-id="` + item.id + `" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '#btn-md-category-create', function() {
    vetlify.invCreateCategory(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');

      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td>${data.name}</td>
          <td>
            <button type="button" data-id="${data.id}" data-name="${data.name}" class="btn btn-primary btn-sm btn-view" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-eye"></i> Edit</button>
            <button type="button" data-id="${data.id}" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {

    })
  });

  $(document).on('click', '.btn-view', function() {
    $('.modal-edit-input').val($(this).data('name'));
    $('.btn-edit-confirm').data('id', $(this).data('id'));
    $('.btn-edit-confirm').data('index', $(this).parent().parent().index());
  });

  $(document).on('click', '.btn-edit-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-edit-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/categories/' + $(this).data('id'),
      data: 'name=' + $('.modal-edit-input').val(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).find('td:nth-child(2)').text($('.modal-edit-input').val());
        $('.table-view tbody tr').eq(_this.data('index')).find('td:nth-child(3) .btn-view').data('name', $('.modal-edit-input').val());
        
        $('#modal-edit').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('id'));
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/categories/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  filter();
});
</script>
@endsection