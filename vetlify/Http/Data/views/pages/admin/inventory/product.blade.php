@extends('Vetlify::layouts.admin')

@section('page_title', 'Products')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products
        <small>List of products</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/products">Products</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Item List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <div class="loader"></div>
              <div class="table-responsive">
                <table class="table table-bordered table-view">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Item Code</th>
                      <th>Reorder Point</th>
                      <th>Retail Price</th>
                      <th>Description</th>
                      <th>Inventory Count</th>
                      <th>Category</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="5">No client to show.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block margin-bottom" data-toggle="modal" data-target="#modal-product-create">Create Product</a>
          
          <div class="box box-solid">
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-reset"><i class="fa fa-undo text-blue"></i> Clear Search</a></li>
              </ul>
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-body no-padding">
              <div class="col-md-10 no-padding">
                <input type="text" class="form-control filter-search-value" placeholder="Search...">
              </div>
              <div class="col-md-2 no-padding">
                <button type="button" class="btn btn-info filter-search">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </div>
            </div>
          </div>

          <div class="box box-solid">
            
          </div>

          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Category</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                @forelse ($categories as $category)
                  <li><a href="#" class="filter-gender" data-id="{{ $category->id }}"><i class="fa fa-circle-o text-red"></i> {{ $category->name }}</a></li>
                @empty
                  <li>No Category.</li>
                @endforelse                
              </ul>
            </div>
            <!-- /.box-body -->
          </div>

          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-edit-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Product</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Product Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="item_code" class="col-sm-2">Item Code</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="item_code" name="item_code" placeholder="Item Code...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="reorder_point" class="col-sm-2">Reorder Point</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="reorder_point" name="reorder_point" placeholder="Reorder Point...">
                </div>
              </div>

              <div class="form-group">
                <label for="retail_price" class="col-sm-2">Retail Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="retail_price" name="retail_price" placeholder="Retail Price...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
                </div>
              </div>

              <div class="form-group">
                <label for="inventory_count" class="col-sm-2">Inventory Count</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inventory_count" name="inventory_count" placeholder="Inventory Count...">
                </div>
              </div>

              <div class="form-group">
                <label for="category_id" class="col-sm-2">Category</label>

                <div class="col-sm-10">
                  <select class="form-control" id="category_id" name="category_id">
                  </select>
                </div>
              </div>
              
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  function Product() {
    this.categories = {!! $categories !!};
  }

  Product.prototype.init = function() {
    vetlify.selectAutoFill('#modal-product-create #category_id', this.categories);
    vetlify.selectAutoFill('#modal-edit-product #category_id', this.categories);
  }

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/products',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td id="name">` + item.name + `</td>
            <td id="item_code">` +item.item_code + `</td>
            <td id="reorder_point">` + item.reorder_point + `</td>
            <td id="retail_price">` + item.retail_price + `</td>
            <td id="description">` + item.description + `</td>
            <td id="inventory_count">` + item.inventory_count + `</td>
            <td id="category_name">` + item.category_name + `</td>      
            <td style="display: inline">
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-edit-product"><i class="fa fa-eye"></i> Edit</button>
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-reset', function() {
    filterItem = {};
    filter();
  });

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '.filter-gender', function() {
    filterItem['category'] = $(this).data('id');
    filter();
  });

  $(document).on('click', '#btn-md-product-create', function() {
    vetlify.invCreateProduct(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td id="name">` + data.name + `</td>
          <td id="item_code">` + data.item_code + `</td>
          <td id="reorder_point">` + data.reorder_point + `</td>
          <td id="retail_price">` + data.retail_price + `</td>
          <td id="description">` + data.description + `</td>
          <td id="inventory_count">` + data.inventory_count + `</td>
          <td id="category_name">` + data.category_name + `</td>          
          <td style="display: inline">
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-edit-product"><i class="fa fa-eye"></i> Edit</button>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {

    })
  });
  
  $(document).on('click', '.btn-edit', function() {
    var content = $(this).data('obj');
    vetlify.formAutoFill('#modal-edit-product', content);
    
    $('.btn-edit-confirm').data('id', content.id);
    $('.btn-edit-confirm').data('index', $(this).parent().parent().index());
  });

  $(document).on('click', '.btn-edit-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-edit-confirm');
    var index = _this.data('index');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/products/' + $(this).data('id'),
      data:_this.parent().parent().find('form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);

        vetlify.tableAutoFill('.table-view', data, index);        
        $('#modal-edit-product').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/products/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  var product = new Product();
  product.init();
  filter();
  
});
</script>
@endsection