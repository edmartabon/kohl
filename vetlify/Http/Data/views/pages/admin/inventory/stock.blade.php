@extends('Vetlify::layouts.admin')

@section('page_title', 'Stock Control')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stock Control
        <small>List of Stock Control</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/stocks-control">Stock Control</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Stock Control List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <div class="loader"></div>
              <div class="table-responsive">
                <table class="table table-bordered table-view">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Supplier</th>
                      <th>Quantity</th>
                      <th>Used </th>
                      <th>Batch</th>
                      <th>Date</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="5">No stock to show.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block margin-bottom" data-toggle="modal" data-target="#modal-stock-create">Create Stock</a>

          <div class="box box-solid">
            <div class="box-body no-padding">
              <div class="col-md-10 no-padding">
                <input type="text" class="form-control filter-search-value" placeholder="Search...">
              </div>
              <div class="col-md-2 no-padding">
                <button type="button" class="btn btn-info filter-search">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </div>
            </div>
          </div>

          <div class="box box-solid">
            
          </div>

          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-edit-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Product</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Product Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="item_code" class="col-sm-2">Item Code</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="item_code" name="item_code" placeholder="Item Code...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="reorder_point" class="col-sm-2">Reorder Point</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="reorder_point" name="reorder_point" placeholder="Reorder Point...">
                </div>
              </div>

              <div class="form-group">
                <label for="retail_price" class="col-sm-2">Retail Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="retail_price" name="retail_price" placeholder="Retail Price...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
                </div>
              </div>

              <div class="form-group">
                <label for="inventory_count" class="col-sm-2">Inventory Count</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inventory_count" name="inventory_count" placeholder="Inventory Count...">
                </div>
              </div>

              <div class="form-group">
                <label for="category_id" class="col-sm-2">Category</label>

                <div class="col-sm-10">
                  <select class="form-control" id="category_id" name="category_id">
                  </select>
                </div>
              </div>
              
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  function Stock() {
    this.products = {!! $products !!};
    this.suppliers = {!! $suppliers !!};
  }

  Stock.prototype.init = function() {
    vetlify.selectAutoFill('#modal-stock-create #supplier_id', this.suppliers, { name: 'company_name' });
    vetlify.selectAutoFill('#modal-stock-create #product_id', this.products);
  }
  
  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/stocks',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td id="name">` + item.name + `</td>
            <td id="supplier_name">` +item.supplier_name + `</td>
            <td id="quantity">` + item.quantity + `</td>
            <td id="used">` + item.used + `</td>
            <td id="batch">` + item.batch + `</td>            
            <td id="created_date">` + item.created_date + `</td>
            <td>
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '#btn-md-stock-create', function() {
    vetlify.invCreateStock(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td id="name">` + data.name + `</td>
          <td id="supplier_name">` +data.supplier_name + `</td>
          <td id="quantity">` + data.quantity + `</td>
          <td id="used">` + data.used + `</td>
          <td id="batch">` + data.batch + `</td>          
          <td id="created_date">` + data.created_date + `</td>   
          <td>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {
      
    })
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/stocks/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  var stock = new Stock();
  stock.init();
  filter();
  
});
</script>
@endsection