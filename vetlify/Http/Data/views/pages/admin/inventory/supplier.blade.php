@extends('Vetlify::layouts.admin')

@section('page_title', 'Suppliers')

@section('content')
<!-- Content Header (Page header) -->
    <div class="row">
        <div class="col-md-12">
        <section class="content-header">
        <h1>
            Suppliers
            <small>List of suppliers</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
            <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/inventory/suppliers">Suppliers</a></li>
        </ol>
        </section>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-supplier-create">Create Supplier</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="loader"></div>
              <table class="table table-bordered table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>First Name</th>
                    <th>last Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Mobile</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="2">No supplier to show.</td>
                    </tr>
                </tbody>
              </table>

              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-edit-supplier">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create Supplier</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="company_name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="code" class="col-sm-2">Supplier Code</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="code" name="code" placeholder="Supplier Code...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-2">Contact Name</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
                </div>

                <div class="col-sm-5">
                  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
                </div>
              </div>

              <div class="form-group">
                <label for="address" class="col-sm-2">Address</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address" name="address" placeholder="Address...">
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-sm-2">Email</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email...">
                </div>
              </div>
              
              <div class="form-group">
                <label for="phone" class="col-sm-2">Contact</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone...">
                </div>

                <div class="col-sm-5">
                  <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile...">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Supplier</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this supplier?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/suppliers',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td id="company_name">` + item.company_name + `</td>
            <td id="code">` + item.code + `</td>
            <td id="first_name">` + item.first_name + `</td>
            <td id="last_name">` + item.last_name + `</td>
            <td id="address">` + item.address + `</td>
            <td id="email">` + item.email + `</td>
            <td id="phone">` + item.phone + `</td>
            <td id="mobile">` + item.mobile + `</td>            
            <td>
              <button type="button" data-obj='` + JSON.stringify(item) + `' class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modal-edit-supplier"><i class="fa fa-eye"></i> Edit</button>
              <button type="button" data-obj='` + JSON.stringify(item) + `' class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const viewNullable = (data) => {

  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '#btn-md-supplier-create', function() {
    vetlify.invCreateSupplier(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td id="company_name">` + data.company_name + `</td>
          <td id="code">` + data.code + `</td>
          <td id="first_name">` + data.first_name + `</td>
          <td id="last_name">` + data.last_name + `</td>
          <td id="address">` + data.address + `</td>
          <td id="email">` + data.email + `</td>
          <td id="phone">` + data.phone + `</td>
          <td id="mobile">` + data.mobile + `</td>            
          <td>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modal-edit-supplier"><i class="fa fa-eye"></i> Edit</button>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {

    })
  });

  $(document).on('click', '.btn-edit', function() {
    var content = $(this).data('obj');
    vetlify.formAutoFill('#modal-edit-supplier', content);
    
    $('.btn-edit-confirm').data('id', content.id);
    $('.btn-edit-confirm').data('index', $(this).parent().parent().index());
  });

  $(document).on('click', '.btn-edit-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-edit-confirm');
    var index = _this.data('index');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/suppliers/' + $(this).data('id'),
      data:_this.parent().parent().find('form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        
        vetlify.tableAutoFill('.table-view', data, index);        
        $('#modal-edit-supplier').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/suppliers/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  filter();
});
</script>
@endsection