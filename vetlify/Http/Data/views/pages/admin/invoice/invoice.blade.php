@extends('Vetlify::layouts.admin')

@section('page_title', 'Invoice')

@section('stylesheet')

@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>List of all invoices</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/employee">Invoice</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Invoices List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <div class="loader"></div>
              <table class="table table-hover custom-table table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Owner</th>
                    <th>Total</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="5">No client to show.</td>
                    </tr>
                </tbody>
              </table>
            </div>

            <div class="box-footer clearfix">
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </section>
    <!-- /.content -->

@endsection


@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  var bloodLists = {},
    genderLists = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/invoices',
      data: $.param(filterItem),
      success: function(data) {
       if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>${(data.per_page *(data.current_page-1)+(num)+ 1)}.</td>
            <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/payments/${item.id}">${item.first_name} ${item.last_name}</a></td>
            <td class="patient_record_id">${parseFloat(item.total_price - item.total_discount).toFixed(2)}</td>
            <td>
              <button type="button" data-id="${item.id}" class="btn btn-primary btn-sm btn-view view-invoice" data-toggle="modal" data-target="#show-invoice"><i class="fa fa-eye"></i> View</button>
            </td>
          </tr>
        `);
          
        num++;
      });
    }
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  const invoice = function(elem, id) {
    $.ajax({
      type: 'GET',
      url: `/{{ Config::get('vetlify.app_route_prefix') }}/api/invoices/${id}`,
      success: function(data) {

        $('.submit-invoice').data('obj', data);

        function toNumberComma(num) {
          return num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function convertToSlug(Text) {
          return Text
            .toLowerCase()
            .replace(/ /g,'-')
            .replace(/[^\w-]+/g,'')
            ;
        }

        elem.find('.invoice-item-list').html('');
        for(var i in data.items) {
          var id = convertToSlug(i);
          elem.find('.invoice-item-list').append(`
          <h4>${i}</h4>
          <table class="table table-striped transaction-item" id="${id}">
            <thead>
            <tr>
              <th style="width: 15%">Qty</th>
              <th style="width: 40%">Product/Procedure</th>
              <th style="width: 15%">Cost</th>
              <th style="width: 15%">Discount</th>
              <th style="width: 15%">Total</th>
            </tr>
            </thead>
            <tbody></tbody>
          </table>
          <hr />
          `)

          for(var i of data.items[i]) {
            
            if (i.name) {
              $(`#${id}`).find('tbody').append(`
                <tr>
                  <td>${toNumberComma(i.quantity)}</td>
                  <td>${i.name}</td>
                  <td>${toNumberComma(parseFloat(i.price).toFixed(2))}</td>
                  <td>${toNumberComma(parseFloat(i.discount).toFixed(2))}</td>
                  <td>${toNumberComma(parseFloat((i.price * i.quantity) - i.discount).toFixed(2))}</td>
                </tr>
              `);
            }
          }
        }

        elem.find('.page-header').html(`
          <i class="fa fa-globe"></i> ${data.branch.name}
          <small class="pull-right">Date: ${moment(data.created_at).format('M/D/YYYY')}</small>
        `);

        elem.find('.invoice-info .invoice-col:first-child').html(`
          From
          <address>
            <strong>${data.branch.name}</strong><br>
            ${data.branch.address_one}<br>
            ${data.branch.contact_no}
          </address>
        `);

        elem.find('.invoice-info .invoice-col:nth-child(2)').html(`
          To
          <address>
            <strong>${data.client.first_name} ${data.client.last_name}</strong><br>
            ${data.client.address_one || ''}<br>
            ${data.client.phone_no || data.client.telephone_no || ''}
          </address>
        `);

      elem.find('.amt-subtotal').text(data.sub_total);
      elem.find('.amt-tax').text(data.tax);
      elem.find('.amt-discount').text(data.discount);
      elem.find('.amt-total').text(data.total);
      }
    });
  }

  const paymentMethod = function(elem, id) {
    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/payment-methods',
      data: $.param(filterItem),
      success: function(data) {
        for(var i of data) {
          $('#show-invoice .method-type').append($('<option>', {
            value: i.id,
            text: i.name
          }));
        }

      }
    });
  }

  $(document).on('click', '.view-invoice', function() {
    var id = $(this).data('id')
    invoice($('#show-invoice'), id);
  });

  $(document).on('click', '.submit-invoice', function() {
    var obj = $(this).data('obj');
    var paymentMethod = $('.patient-invoice .method-type').val();
    var button = vetlify.button('.submit-invoice');
    button.enableButton(true)
      .loadingButton(true);

    obj.payment_method = paymentMethod;
    console.log(obj)
    $.ajax({
      type: 'POST',
      contentType:'application/json',
      url: `/{{ Config::get('vetlify.app_route_prefix') }}/api/clients/${obj.client.id}/invoice`,
      dataType: 'json',
      data: JSON.stringify(obj),
      success: function(data) {
        $('#show-invoice').modal('hide');
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  paymentMethod();  
  filter();
});
</script>
@endsection