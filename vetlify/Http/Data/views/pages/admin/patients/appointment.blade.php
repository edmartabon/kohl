@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Files')

@section('stylesheet')
  <link rel="stylesheet" href="/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="/css/fullcalendar.min.css">
  <link rel="stylesheet" href="/css/fullcalendar.print.min.css" media="print">
  <link rel="stylesheet" href="/css/fastselect.min.css">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Files</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])

        </div>
        <!-- /.col -->
        <div class="col-md-9 no-padding">    
          <section class="content-header">
            <div class="box">
              <div id="profile-calendar"></div>
            </div>
          </section>

        </div>
          
      </div>
    </section>
    <!-- /.content -->


  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-add-appointment">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Create Appointment</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="client_id" class="col-sm-2">Veterinarian</label>

              <div class="col-sm-10">
                <select class="form-control js-states" id="veterinarian_id" name="veterinarian_id" style="width: 100%"></select>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Email</label>

              <div class="col-sm-10">
                <input type="text" class="form-control email" name="email">
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Phone</label>

              <div class="col-sm-10">
                <input type="text" class="form-control phone_no" name="phone_no">
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Date</label>

              <div class="col-sm-10">
                <input type="text" class="form-control date-start" name="date_start" autocomplete="off"/>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Time</label>

              <div class="col-sm-5">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="start-date" type="text" class="form-control input-small" name="start_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
              </div>
              
              <div class="col-sm-5">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="end-date" type="text" class="form-control input-small" name="end_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Description</label>

              <div class="col-sm-10">
                <textarea style="resize:none" class="form-control" rows="3" id="comment" name="description"></textarea>
              </div>
            </div>

            <hr class="separator">

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Notify At</label>

              <div class="col-sm-10">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="notify-at" type="text" class="form-control" aria-describedby="helpBlock2" name="notify_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> Notify before</span>
                  <span id="helpBlock2" class="help-block"></span>
                </div>
              </div>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success btn-create-confirm">Create</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-update-appointment">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Update Appointment</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="client_id" class="col-sm-2">Veterinarian</label>

              <div class="col-sm-10">
                <select class="form-control js-states veterinarian_id" name="veterinarian_id" style="width: 100%"></select>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Email</label>

              <div class="col-sm-10">
                <input type="text" class="form-control email" name="email">
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Phone</label>

              <div class="col-sm-10">
                <input type="text" class="form-control phone_no" name="phone_no">
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Date</label>

              <div class="col-sm-10">
                <input type="text" class="form-control date_start" name="date_start" autocomplete="off"/>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Time</label>

              <div class="col-sm-5">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="start-date" type="text" class="form-control input-small start_at" name="start_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
              </div>
              
              <div class="col-sm-5">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="end-date" type="text" class="form-control input-small end_at" name="end_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Description</label>

              <div class="col-sm-10">
                <textarea style="resize:none" class="form-control description" rows="3" id="comment" name="description"></textarea>
              </div>
            </div>

            <hr class="separator">

            <div class="form-group">
              <label for="client_id" class="col-sm-2">Notify At</label>

              <div class="col-sm-10">
                <div class="input-group bootstrap-timepicker timepicker">
                  <input id="notify-at" type="text" class="form-control notify_at" aria-describedby="helpBlock2" name="notify_at" autocomplete="off">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> Notify before</span>
                  <span id="helpBlock2" class="help-block"></span>
                </div>
              </div>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          <button type="button" class="btn btn-success btn-update-confirm">Update</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

    
@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods
])

<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var updateAppointmentEvent = null;

  $('#profile-calendar').fullCalendar({
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : 'createAppointment month,agendaWeek,agendaDay,listWeek'
    },
    buttonText: {
      today: 'today',
      month: 'month',
      week : 'week',
      day  : 'day',
      listWeek: 'list'
    },
    events: function(start, end, timezone, callback) {
      $.ajax({
        url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/appointments/',
        dataType: 'json',
        data: {
          start: start.format('YYYY-MM-DD'),
          end: end.format('YYYY-MM-DD')
        },
        success: function(data) {
          var events = [];
          data.forEach(function(elem) {
            events.push(elem)
          });
          callback(events);
        }
      });
    },
    eventClick: updateAppointment,
    customButtons: {
      createAppointment: {
        text: 'Create Appointment',
        click: createAppointment
      }
    },
    dayClick: createAppointmentByDate,
    editable  : true,
    droppable : true,
    eventLimit: true
  });

  $('#start-date, #start_date').timepicker();
  $('#end-date, #end_date').timepicker();
  $('#notify-at, #notify_at').timepicker();

  const datePicker = () => {
    var start = moment().subtract(29, 'days');
    var end = moment();


    $('.date-start, .date_start').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: parseInt(moment().format('YYYY'),10)
    }, function(start, end, label) {
      // var years = moment().diff(start, 'years');
      // alert("You are " + years + " years old!");
    });
  }

  var select2Option = {
    ajax: {
      url: `/${globalConfig.linkApp}/api/veterinarians`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.first_name + ' ' + i.last_name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  };

  function createAppointment(date) {
    var dayClick = typeof date.format === 'function' ? date.format()  : '';
    $('#modal-add-appointment').modal('show');
    $('#modal-add-appointment .email').val(profile.userSetting.email);
    $('#modal-add-appointment .phone_no').val(profile.userSetting.phone_no);
  }

  function createAppointmentByDate(dateSelect){
    createAppointment(dateSelect);
    $('#modal-add-appointment .date-start').val(dateSelect.format('M/D/YYYY'));
  }

  function updateAppointment(event) {
    var parent = $('#modal-update-appointment');
    var select2 = Object.assign({}, select2Option);
    updateAppointmentEvent = event;
    parent.modal('show');
    
    
    parent.find('.phone_no').val(event.phone_no);
    parent.find('.email').val(event.email);
    parent.find('.description').val(event.description);
    parent.find('.date_start').val(moment(event.start_at).format('M/D/YYYY'));
    parent.find('.start_at').val(moment(event.start_at).format('hh:mm A'));
    parent.find('.end_at').val(moment(event.end_at).format('hh:mm A'));
      
    $('.veterinarian_id').html('');
    $('.veterinarian_id').select2(select2);
    var $newOption = $("<option></option>").val(event.veterinarian_id).text(event.title)
    $(".veterinarian_id").append($newOption).trigger('change');
    $('.btn-update-confirm').data('apppointment_id', event.id);
    $('.btn-delete-confirm').data('calendar_id', event._id);
    $('.btn-delete-confirm').data('apppointment_id', event.id);
  }

  $(document).on('click', '.btn-create-confirm', function() {
    alert('test');
    var button = vetlify.button('.btn-create-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'POST',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/appointments',
      data: $('#modal-add-appointment form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);

        data.start = moment(data.start);
        data.end = moment(data.end);        

        $('#profile-calendar').fullCalendar('renderEvent', data);
        $('#modal-add-appointment').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-update-confirm', function() {
    var button = vetlify.button('.btn-update-confirm');
    var appointmentId = $(this).data('apppointment_id');
    var parent = $('#modal-update-appointment form');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/appointments/' + appointmentId,
      data: $('#modal-update-appointment form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);   

        updateAppointmentEvent.veterinarian_id = parent.find('.veterinarian_id').val();
        updateAppointmentEvent.email = data.email;
        updateAppointmentEvent.phone_no = data.phone_no;
        updateAppointmentEvent.title = data.title;
        updateAppointmentEvent.start_at = data.start_at
        updateAppointmentEvent.end_at = data.end_at
        updateAppointmentEvent.start = moment(data.start)
        updateAppointmentEvent.end = moment(data.end)
        updateAppointmentEvent.notify_at = data.notify_at

        $('#profile-calendar').fullCalendar('updateEvent', updateAppointmentEvent);
        $('#modal-update-appointment').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  })

  $(document).on('click', '.btn-delete-confirm', function() {
    var appointmentId = $(this).data('apppointment_id'),
      calendarId = $(this).data('calendar_id'),
      button = vetlify.button('.btn-update-confirm');

    button.enableButton(true)
      .loadingButton(true);
    
    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/appointments/' + appointmentId,
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);   

        $('#profile-calendar').fullCalendar('removeEvents', calendarId);
        $('#modal-update-appointment').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });

    
  });

  $('#veterinarian_id').select2(select2Option);
  datePicker();

});

</script>
@endsection