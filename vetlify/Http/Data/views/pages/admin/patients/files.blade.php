@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Files')

@section('stylesheet')

  <link rel="stylesheet" href="/css/fastselect.min.css">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Files</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])

        </div>
        <!-- /.col -->
        <div class="col-md-9 no-padding">    
          <section class="content-header">
            <h1>
              Files     
              <small>List of files about the patient</small>         
            </h1>     
            <div class="breadcrumb">
              <div class="upload-profile-file">
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-create-file">Upload File</button>
              </div>
            </div>
            

          </section> <br>
          
          <div class="list-files">
          </div>     
          <div class="clearfix"></div>
          <div class="pull-right">
            <nav aria-label="Page navigation">
              <ul class="pagination" id="pagination"></ul>
            </nav>
          </div>
        </div>
          
      </div>
    </section>
    <!-- /.content -->

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-create-file">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload Files</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Name...">
                </div>
              </div>

              <div class="form-group">
                <label for="veterinarian" class="col-sm-2">Veterinarian</label>

                <div class="col-sm-10">
                  <select class="form-control" id="veterinarian" name="veterinarian" style="width: 100%"></select>
                </div>
              </div>

              <div class="form-group">
                <label for="upload-file" class="col-sm-2">Files</label>

                <div class="col-sm-10">
                  <input type="file" id="upload-file" class="upload-file">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-create-confirm">Create</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-update-file">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Files</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="name" class="col-sm-2">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Name...">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-update-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-file-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete File</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this file?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods
])

<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/files',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.list-files');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        $('.list-files').prepend(listView(item));
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  const listView = (data) => {
    return `
          <div class="col-xs-6 col-md-3 list-item">
            <div class="thumbnail file-thumbnail">
              <div class="thumbnail-type">
                <i class="fa ${listType(data.extension)}"></i>
              </div>
              <div class="caption">
                <h5>${limitName(data.name)}</h5>
                <p class="text-center">
                  <a href="{{ Config::get('vetlify.aws_public_url').Request::route('account') }}/patient/files/${data.file_name}" class="btn btn-success btn-sm btn-download" data-toggle="modal" data-target="#modal-file-download" data-obj='${JSON.stringify(data)}' role="button"><i class="fa fa-download"></i></a> 
                  <a href="#" class="btn btn-primary btn-sm btn-update" data-toggle="modal" data-target="#modal-update-file" data-obj='${JSON.stringify(data)}' role="button"><i class="fa fa-pencil"></i></a> 
                  <a href="#" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-file-delete" data-obj='${JSON.stringify(data)}' role="button"><i class="fa fa-remove"></i></a>
                </p>                
              </div>
            </div>
          </div>
        `
  }

  const limitName = (name) => {
    var changeName = name.substring(0, 20);
    return changeName + (name.length > 20 ? '...': '');
  }

  const listType = (type) => {
    const listType = {
      'pdf': 'fa-file-pdf-o',
      'jpg': 'fa-file-image-o',
      'gif': 'fa-file-image-o',
      'jpeg': 'fa-file-image-o',
      'png': 'fa-file-image-o',
      'xls': 'fa-file-excel-o',
      'xlsx': 'fa-file-excel-o',
    }
    return listType[type] || 'fa-clone';
  }

  $('#veterinarian').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/veterinarians`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for(var i of data.data) {
          results.push({
            id: i.id,
            text: i.first_name + ' ' + i.last_name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $(document).on('click', '.btn-create-confirm', function() {

    if ($('#upload-file')[0].files.length == 0) return false;

    var fd = new FormData();   
    var files = $('#upload-file')[0].files[0];
    var button = vetlify.button('.btn-create-confirm');    

    fd.append('files', files);
    fd.append('veterinarian', $('#veterinarian').val());
    fd.append('patient_id', {{ Request::route('id') }});
    fd.append('name', $('#name').val());
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/files',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        button.enableButton(false)
          .loadingButton(false);
        
        $('#modal-create-file #name').val('');
        $('#modal-create-file #upload-file').val('');
        $('#modal-create-file').modal('hide');
        $('.list-files').prepend(listView(data));
      }
    });
  });

  $(document).on('click', '.btn-update', function() {
    var data = $(this).data('obj');
    var parent = $(this).parent().parent().parent().parent();
    
    $('#modal-update-file #name').val(data.name);
    $('#modal-update-file .btn-update-confirm').data('obj', data);
    $('#modal-update-file .btn-update-confirm').data('index', parent.index());
  });

  $(document).on('click', '.btn-update-confirm', function() {
    var _this = $(this);
    var fileData = $(this).data('obj');
    var index = $(this).data('index');
    var button = vetlify.button('.btn-update-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/files/' + fileData.id,
      data:_this.parent().parent().find('form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);

        $('.list-files .list-item').eq(_this.data('index')).find('.caption h5').text(limitName(data.name));
        $('#modal-edit-product').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });


  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent().parent().parent(),
      btnDelete = $('.btn-delete-confirm');
    
    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/files/'+_this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.list-files .list-item').eq(_this.data('index')).remove();

        $('#modal-file-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-download', function(e) {
    var data = $(this).data('obj'),
      file = $(this).attr('href');
  });

  filter();
});

</script>
@endsection