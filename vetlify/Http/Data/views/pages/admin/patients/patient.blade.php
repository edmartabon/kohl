@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patients
        <small>List of client and patients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patients</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Patient List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <div class="loader"></div>
              <table class="table table-hover custom-table table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Patient Name</th>
                    <th>Date Added</th>
                    <th>Owner</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="5">No client to show.</td>
                    </tr>
                </tbody>
              </table>
            </div>

            <div class="box-footer clearfix">
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block margin-bottom" data-toggle="modal" data-target="#modal-patient-create">Create Patient</a>
          
          <div class="box box-solid">
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-reset"><i class="fa fa-undo text-blue"></i> Clear Search</a></li>
              </ul>
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-body no-padding">
              <div class="col-md-10 no-padding">
                <input type="text" class="form-control filter-search-value" placeholder="Search...">
              </div>
              <div class="col-md-2 no-padding">
                <button type="button" class="btn btn-info filter-search">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </div>
            </div>
          </div>

          <div class="box box-solid">
            
          </div>

          <div class="box box-solid">          

            <div class="box-header with-border">
              <h3 class="box-title">Date</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li>
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar client-filter-date"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                  </div>
                  <div class="clearfix"></div>
                </li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d') }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-blue"></i> Today</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-1 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-yellow"></i> Yesterday</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-6 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-aqua"></i> Last 7 Days</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-29 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-light-blue"></i> Last 30 Days</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Gender</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-gender" data-id="1"><i class="fa fa-circle-o text-red"></i> Male</a></li>
                <li><a href="#" class="filter-gender" data-id="2"><i class="fa fa-circle-o text-yellow"></i> Female</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  var bloodLists = {!! $bloods !!},
    genderLists = {!! $genders !!};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        var patientAvatar = clientAvatar = '/images/avatar/thumbnail/noavatar.png';

        if (item.patient_avatar) {
          patientAvatar = '{{ env('VETLIFY_PUBLIC_AWS_URL') }}'+globalConfig.appName+'/patient/avatar/thumbnail/'+item.patient_avatar;
        }

        if (item.client_avatar) {
          clientAvatar = '{{ env('VETLIFY_PUBLIC_AWS_URL') }}'+globalConfig.appName+'/client/avatar/thumbnail/'+item.client_avatar;
        }

        viewTable.append(`
          <tr>
            <td>${(data.per_page *(data.current_page-1)+(num)+ 1)}.</td>
            <td class="patient_record_id">${item.patient_record_id}</td>
            <td class="avatar">
              <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/${item.patient_id}/procedure">
                <img src="${patientAvatar}">
                ${item.patient_first_name} ${item.patient_last_name}
              </a>
            </td>
            <td>${item.date_created}</td>
            <td class="avatar">
              <a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/profile/${item.client_id}">
                <img src="${clientAvatar}">
                ${item.client_first_name} ${item.client_last_name}
              </a>
            </td>
            <td>
              <i data-id="${item.patient_id}" class="btn-delete fa fa-trash table-remove-item" data-toggle="modal" data-target="#modal-delete" title="Delete"></i>
            </td>
          </tr>
        `);
          
        num++;
      });
    }
    
  }

  const datePicker = () => {
    var start = moment().subtract(29, 'days');
    var end = moment();

    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end
    }, (start, end) => {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      filterItem['start'] = start.format('YYYY-MM-DD');
      filterItem['end'] = end.format('YYYY-MM-DD');
      filter();
    });
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-reset', function() {
    filterItem = {};
    filter();
  });

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '.filter-date', function() {
    filterItem['start'] = $(this).data('start');
    filterItem['end'] = $(this).data('end');
    filter();
  });

  $(document).on('click', '.filter-gender', function() {
    filterItem['gender'] = $(this).data('id');
    filter();
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('id'));
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '.btn-view', function() {
    window.location = `/{{ Config::get('vetlify.app_route_prefix') }}/patients/${$(this).data('id')}/procedure`;
  });

  $(document).on('click', '#btn-md-client-create', function() {
    vetlify.invCreateClient(this, function(data) {   
        var $clientOptions = $("<option></option>").val(data.id).text(data.first_name + ' ' + data.last_name);
        $('#modal-patient-create #client_id').html($clientOptions).trigger('change');

    }, function(data) {

    })
  });

  $(document).on('click', '#btn-md-patient-create', function() {
    vetlify.invCreatePatient(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td class="patient_record_id">${data.patient_record_id}</td>
          <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/${data.patient_id}/procedure">${data.patient_first_name} ${data.patient_last_name}</a></td>
          <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/profile/${data.client_id}">${data.client_first_name} ${data.client_last_name}</a></td>
          <td>
            <button type="button" data-id="${data.patient_id}" class="btn btn-primary btn-sm btn-view"><i class="fa fa-eye"></i> View</button>
            <button type="button" data-id="${data.patient_id}" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);

    }, function(data) {

    })
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  vetlify.selectAutoFill('#modal-patient-create #patient_blood_id', bloodLists);
  vetlify.selectAutoFill('#modal-patient-create #patient_gender_id', genderLists);

  datePicker();
  filter();
});
</script>
@endsection