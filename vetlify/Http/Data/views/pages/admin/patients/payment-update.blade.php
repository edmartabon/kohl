@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Create Treatment Plan')

@section('stylesheet')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Create Treatment</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])
          
        </div>
        <!-- /.col -->
        <div class="col-md-9 no-padding">
          
          <div>
            <div class="col-md-12">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Payment</h3>
                <div class="box-tools">
                  <button class="btn btn-success btn-sm btn-view add-payment" >Add Payment</button>
                </div>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table compliant-table">
                  <thead>
                    <tr>
                      <th style="width: 40%">Procedure Name</th>
                      <th style="width: 15%">Cost</th>
                      <th style="width: 15%">Quantity</th>
                      <th style="width: 15%">Discount</th>
                      <th style="width: 15%">Total</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

          </div>


          <div class="col-md-12">
            <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Veterinarian</h3>
            </div>

            <div class="box-body">
              <select class="form-control js-states" id="veterinarian_id" name="veterinarian_id" style="width: 100%"></select>
            </div>
          
            </div>
            <!-- /.nav-tabs-custom -->
            
            <button class="btn btn-primary btn-delete save-payment">Update Payment</button>
            <button class="btn btn-success btn-delete print-payment" data-toggle="modal" data-target="#show-payment">Payment</button>
          </div>

          
        </div>
          
         
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>
    <!-- /.content -->

      
      <!-- /.modal-dialog -->
    </div>

@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods,
  'branch' => $branch,
  'paymentMethod' => $paymentMethod,
])
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js" integrity="sha384-THVO/sM0mFD9h7dfSndI6TS0PgAGavwKvB5hAxRRvc0o9cPLohB0wb/PTA7LdUHs" crossorigin="anonymous"></script>
<script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var productSelect2Option = {
    ajax: {
      url: `/${globalConfig.linkApp}/api/products`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { 
          search: params.term,
          procedure: true
        }

        if (params.page) {
          query.page = params.page
        }

        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        results.push({
          id: 0,
          text: 'Add New ...',
          price: 0,
        });

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name,
            price: i.retail_price
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      },
    }
  }

  var selectedNewProcedure = null;

  function generateTreatment() {
    $('.custom-table tbody').prepend(`
      <tr>
        <td><select class="form-control product-id" data-name="compliant" name="product-id" style="width: 100%"></select></td>
        <td><input type="number" class="form-control compute treatment-cost" placeholder="Cost..." value="0"></td>
        <td><input type="number" class="form-control compute treatment-quantity" placeholder="Quantity..." value="1"></td>
        <td><input type="number" class="form-control compute treatment-discount" placeholder="Discount..." value="0"></td>
        <td><input type="number" class="form-control treatment-total" disabled value="0"></td>
        <td>
          <button type="button" class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-remove"></i></button>
        </td>
      </tr>
    `)
    $('.product-id').select2(productSelect2Option);
    $('.product-id').on('select2:selecting', function(e) {
      var data = e.params.args.data;
      if (data.id !== 0) {
        var parent = $(this).parent().parent().find('td:nth-child(2) input');
        parent.val(data.price);
        parent.trigger('keyup');
      }
      
    });

    $('.product-id').on('select2:close', function(e) {
      var data = $(this).val();
      if (data == 0) {
        $('#modal-procedure-create').modal('show');
        selectedNewProcedure = $(this);
      }
    });

  }

  function tableData() {
    var parent = $('.custom-table tbody tr'),
      dataTable = {
        patient_id: {{ Request::route('id') }},
        veterinarian: $('#veterinarian_id').val(),
        transaction_item: []
      }

    for(var i of parent) {
      var newData = {
        product_id: $(i).find('td .product-id').val() || 0,
        price: $(i).find('td .treatment-cost').val() || 0,
        quantity: $(i).find('td .treatment-quantity').val() || 0,
        discount: $(i).find('td .treatment-discount').val() || 0,
      }

      if ($(i).find('td .transaction_item_id').length && $(i).find('td .transaction_item_id').val() !== 'null') {
        newData.transaction_item_id = $(i).find('td .transaction_item_id').val();
      }

      dataTable.transaction_item.push(newData)
    }

    return dataTable;
  }

  function getTransaction() {
    $.ajax({
      type: 'GET',
      url: `/${globalConfig.linkApp}/api/transactions/{{ Request::route('transactionId') }}`,
      dataType: 'json',
      success: function (data) {
        
        for(let i of data.items) {
          generateTreatment();
          let parent = $('.custom-table tbody tr:first-child');
          parent.find('.treatment-cost').val(i.price);
          parent.find('.treatment-quantity').val(i.quantity);
          parent.find('.treatment-discount').val(i.discount);
          parent.find('.treatment-cost').trigger('keyup');
          parent.append(`<td><input type="hidden" class="transaction_item_id" name="transaction_item_id" value="${i.transaction_item_id}"></td>`)

          var $productOption = $("<option></option>").val(i.product_id).text(i.product_name);
          var $veterinarianOption = $("<option></option>").val(i.veterinarian_id).text(i.veterinarian);
          parent.find('select').append($productOption).trigger('change');
          $('#veterinarian_id').html($veterinarianOption).trigger('change');

          if (data.has_payment !== null) {
            $('.patient-payment .submit-payment').remove();
            $('.save-payment').remove();
          }
        }

      },
      error: function (data) {
        
      }
    });
  }

$(document).on('click', '#btn-md-procedure-create', function() {
    vetlify.invCreateProduct(this, function(data) {
      selectedNewProcedure.html('');
      var $newOption = $("<option></option>").val(data.id).text(data.name);
      var price = parseFloat($('#modal-procedure-create #retail_price').val());
      var priceInput = selectedNewProcedure.parent().parent().find('.treatment-cost'); 
      selectedNewProcedure.append($newOption).trigger('change');
      priceInput.val(price);
      priceInput.trigger('keyup');
    }, function(data) {

    })
  });

  $('#veterinarian_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/veterinarians`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.first_name + ' ' + i.last_name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#product-id').select2();

  $(document).on('click', '.add-payment', function() {
    generateTreatment();
  });

  $(document).on('click', '.save-payment', function() {
    var _this = $(this);
    var button = vetlify.button('.save-payment');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/transactions/{{Request::route('transactionId')}}',
      contentType: 'application/json',
      data: JSON.stringify(tableData()),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        
        $('.custom-table tbody').html('');

        for(let i of data) {
          generateTreatment();
          let parent = $('.custom-table tbody tr:first-child');
          parent.find('.treatment-cost').val(i.price);
          parent.find('.treatment-quantity').val(i.quantity);
          parent.find('.treatment-discount').val(i.discount);
          parent.find('.treatment-cost').trigger('keyup');
          parent.append(`<td><input type="hidden" class="transaction_item_id" name="transaction_item_id" value="${i.transaction_item_id}"></td>`)

          var $productOption = $("<option></option>").val(i.product_id).text(i.product_name);
          var $veterinarianOption = $("<option></option>").val(i.veterinarian_id).text(i.veterinarian);
          parent.find('select').append($productOption).trigger('change');
          $('#veterinarian_id').html($veterinarianOption).trigger('change');
        }
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('keyup mouseup', '.compute', function() {

    var parent = $(this).parent().parent(),
      cost = parent.find('.treatment-cost').val() || 0,
      quantity = parent.find('.treatment-quantity').val() || 1,
      discount = parent.find('.treatment-discount').val() || 0;

    parent.find('.treatment-total').val(parseFloat((cost * quantity) - discount).toFixed(2));
  });

  $(document).on('click', '.btn-remove-item', function() {
    $(this).parent().parent().remove();
  });

  $('#show-payment').on('show.bs.modal', function() {
    vetlify.showInvoice($('.patient-payment'), {{Request::route('transactionId')}})
  })

  getTransaction();
});


</script>
@endsection