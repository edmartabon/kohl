@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Create Procedure')

@section('stylesheet')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Create Procedure</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])
          
        </div>
        <!-- /.col -->
        <div class="col-md-9 no-padding">
          
          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"></h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table compliant-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Purpose of Visit</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No purpose of visit.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="compliant_id" class="col-sm-12">Select purpose of visit:</label>

                <div class="col-sm-12">
                  <select class="form-control" id="compliant_id" data-name="compliant" name="compliant_id" style="width: 100%"></select>
                </div>
                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-compliants" placeholder="Create Compliant...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-compliant" data-type="compliants">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Physical Examination</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table investigation-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Physical Examination</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No physical examination found.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="investigation_id" class="col-sm-12">Select physical examination:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="investigation_id" data-name="investigation" name="investigation_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-investigations" placeholder="Create Investigation...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-investigation" data-type="investigations">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>


          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Recommendations</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table recommendation-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Recommendations</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No recommendation found.</td>
                    </tr>                    
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="diagnose_id" class="col-sm-12">Select recommendation:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="recommendation_id" data-name="recommendation" name="recommendation_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-recommendations" placeholder="Create recommendation...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-recommendation" data-type="recommendations">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Prescriptions</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table prescription-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Prescriptions</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No prescriptions found.</td>
                    </tr>                    
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="diagnose_id" class="col-sm-12">Select prescription:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="prescription_id" data-name="prescription" name="prescription_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-prescriptions" placeholder="Create prescription...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-prescription" data-type="prescriptions">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Laboratories</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table laboratory-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Laboratories</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No laboratories found.</td>
                    </tr>                    
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="diagnose_id" class="col-sm-12">Select laboratory:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="laboratory_id" data-name="laboratory" name="laboratory_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-laboratories" placeholder="Create laboratory...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-laboratory" data-type="laboratories">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tentative Diagnosis</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table diagnose-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Tentative Diagnosis</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No tentative diagnosis found.</td>
                    </tr>                    
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="diagnose_id" class="col-sm-12">Select tentative diagnosis:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="diagnose_id" data-name="diagnose" name="diagnose_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-diagnoses" placeholder="Create Diagnose...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-diagnose" data-type="diagnoses">Save</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Notes</h3>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table notes-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Notes</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">No notes found.</td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-4 no-padding">
              <div class="form-group">
                <label for="note_id" class="col-sm-12">Select Notes:</label>
    
                <div class="col-sm-12">
                  <select class="form-control" id="note_id" data-name="notes" name="note_id" style="width: 100%"></select>
                </div>

                <br><br><br><br>
                <div class="consultation-sub-create">

                  <div class="col-sm-8">
                    <input type="text" class="form-control create-notes" placeholder="Create Notes...">
                  </div>

                  <div class="col-sm-2 no-padding">
                    <button class="btn btn-primary btn-delete pull-left save-item-procedure" id="save-notes" data-type="notes">Save</button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="col-md-8 no-padding">
              <div class="box">
              <div class="box-header with-border">
                <h4 class="panel-title">
                  Image Mapping
                  <div class="btn-group pull-right">
                    <a href="#" class="btn btn-primary btn-sm show-image-mapping" data-toggle="modal" data-target="#show-image-map">Setup</a>
                  </div>
                </h4>
              </div>

              <div class="box-body table-responsive no-padding">
                <table class="table table-hover custom-table mapping-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="5">No mapping found.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            
              </div>
              <!-- /.nav-tabs-custom -->
            </div>

          </div>
          
          <div class="modal fade" data-backdrop="static" data-keyboard="false" id="show-image-map">
            <div class="modal-dialog show-image-map">
              <div class="modal-content">
                
                <div class="modal-body">
                  <form class="form-horizontal sidebar-mapping-form">
                    <div class="row">
                      <div class="col-xs-12 col-md-8 display-map">
                        <div class="tagged-handle" style="position: relative;">
                          <div id="tagged" class="tag">
                            <span id="span-id">0</span>
                          </div>

                          <img class="map-type" src="" onclick="vetlify.dotMap(event)">
                          <div class="map-handle-coordinates"></div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-4 display-map-content">
                        <div class="display-map-table">
                          <form class="form-inline">
                            <div class="form-group" style="width: 255px !important;margin-left: 1px;">
                              <label for="exampleInputName2">Type</label>
                              <select class="form-control" onchange="vetlify.showMap(this.value)">
                                <option value="canine" selected>Canine</option>
                                <option value="feline">Feline</option>
                              </select>
                            </div>
                            <div class="has-dotmap hidden">
                              <div class="form-group" style="width: 255px !important;margin-left: 1px;">
                                <label for="exampleInputName2">Title</label>
                                <input type="text" class="form-control title">
                              </div>

                              <div class="form-group" style="width: 255px !important;margin-left: 1px;">
                                <label for="exampleInputName2">Description</label>
                                <input type="text" class="form-control description">
                              </div>

                              <button type="button" class="btn btn-default" onclick="vetlify.cancelMap()">Cancel</button>
                              <button type="button" class="btn btn-primary" onclick="vetlify.createMap()">Add</button>
                            </div>

                            <div class="table-image-mapping">
                              <table class="table">
                                <thead>
                                  <th>#</th>
                                  <th>Title</th>
                                  <th>Description</th>
                                  <th>Action</th>
                                </thead>
                                <tbody></tbody>
                              </table>
                            </div>
                          </form>
                        </div>
                        <div class="text-right">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </form>
                  
                </div>
                
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>  

          <div class="col-md-8 no-padding">
            <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Veterinarian</h3>
            </div>

            <div class="box-body">
              <select class="form-control js-states" id="veterinarian_id" name="veterinarian_id" style="width: 100%">
                <option value="{{ $procedures->user_id }}" selected>{{ $procedures->first_name }} {{ $procedures->last_name }}</option>
              </select>
            </div>
          
            </div>
            <!-- /.nav-tabs-custom -->

            <button class="btn btn-success btn-delete pull-left update-procedure">Update Procedures</button>
          </div>

          
        </div>
          
         
        <!-- /.col -->
      </div>
      
      <!-- /.row -->

     

    </section>
    <!-- /.content -->

  <div id="profile-calendar"></div>

@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods
])
<script type="text/javascript">
var typeAnimal = '{{ $procedures->type_animal }}';
var procedures = {
  compliant: {!! json_encode($procedures->compliants) !!},
  investigation: {!! json_encode($procedures->investigations) !!},
  diagnose: {!! json_encode($procedures->diagnoses) !!},
  notes: {!! json_encode($procedures->notes) !!},
  diagnose: {!! json_encode($procedures->diagnoses) !!},
  recommendation: {!! json_encode($procedures->recommendations) !!},
  prescription: {!! json_encode($procedures->prescriptions) !!},
  laboratory: {!! json_encode($procedures->laboratories) !!},
  mapping: {!! json_encode($procedures->image_mappings) !!},
};


function updater() {
  for(var i in procedures) {
    var counter = 1;

    if (!procedures[i].length) {
      $(`.${i}-table tbody`).html(`<tr><td colspan="3">No ${i} found.</td></tr>`);
    }
    else $(`.${i}-table tbody`).html('');

    for(var o of procedures[i].slice(0)) {
     if (typeof o == 'object') {
        $(`.${i}-table tbody`).append(`
          <tr>
            <td>${counter}.</td>
            <td data-name="${i}" class="procedure-text">${o.title}</td>
            <td data-name="${i}" class="procedure-text">${o.description}</td>
            <td>
              <button class="btn btn-danger btn-xs btn-delete delete-item">Delete</button>
            </td>
          </tr>
        `);
      }
      else {
        $(`.${i}-table tbody`).append(`
          <tr>
            <td>${counter}.</td>
            <td data-name="${i}" class="procedure-text">${o}</td>
            <td>
              <button class="btn btn-danger btn-xs btn-delete delete-item">Delete</button>
            </td>
          </tr>
        `);
      }
      counter++;
    }
    vetlify.renderMapping();
  }
}


$(document).ready(function() {
  vetlify.showMap(typeAnimal);
  
  function insert(type, name) {
    if (procedures[type].indexOf(name) == -1) {
      procedures[type].push(name);
    }
    updater();
  }
  

  $(document).on('change', '#compliant_id, #diagnose_id, #investigation_id, #note_id, #recommendation_id, #prescription_id, #laboratory_id', function() {
    var name = $(this).text();
    var dataName = $(this).data('name');
    $(this).text('');

    if (procedures[dataName].indexOf(name) > -1) return false;
    procedures[dataName].push(name);

    updater();
  });

  $(document).on('click', '.delete-item', function() {
    var value = $(this).parent().parent().find('.procedure-text');
    var index = $(this).parent().parent().index();
    procedures[value.data('name')].splice(index, 1)

    console.log(procedures[value.data('name')], index)
    updater();
  });

  $(document).on('click', '.update-procedure', function() {
    var button = vetlify.button('.update-procedure');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: `/${globalConfig.linkApp}/api/procedures/{{ $procedures->id }}`,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify({
        patient_id: {{ Request::route('id') }},
        compliants: procedures.compliant,
        investigations: procedures.investigation,
        diagnoses: procedures.diagnose,
        prescriptions: procedures.prescription,
        recommendations: procedures.recommendation,
        laboratories: procedures.laboratory,
        notes: procedures.notes,
        veterinarian: $('#veterinarian_id').val(),
        image_mappings: procedures.mapping,
        type_animal: typeAnimal
      }),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    }); 
  });

  $(document).on('click', '.save-item-procedure', function() { 
    var type = $(this).data('type');
    var name = $(`.create-${type}`).val();
    var elem = $(this).attr('id').replace('save-', '');
    
    if (!name) return false;

    var button = vetlify.button('.update-procedure');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'POST',
      url: `/${globalConfig.linkApp}/api/${type}`,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify({ name: name }),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);

        insert(elem, name);

        updater();

      }
    }); 
  });

  $('#veterinarian_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/veterinarians`,
      minimumInputLength: 2,
      triggerChange: true,
      allowClear: true,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.first_name + ' ' + i.last_name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#compliant_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/compliants`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#investigation_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/investigations`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#diagnose_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/diagnoses`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#laboratory_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/laboratories`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#prescription_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/prescriptions`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#recommendation_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/recommendations`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });

  $('#note_id').select2({
    ajax: {
      url: `/${globalConfig.linkApp}/api/notes`,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var query = { search: params.term };

        if (params.page) {
          query.page = params.page
        }
        return query;
      },
      processResults: function (data, params) {
        params.page = params.page || 1;

        var results = [];

        for (var i of data.data) {
          results.push({
            id: i.id,
            text: i.name
          })
        }

        return {
          results: results,
          pagination: {
            more: (params.page * data.per_page) < data.total
          }
        };
      }
    }
  });


  $(document).on('submit', '.sidebar-mapping-form', function() {
    return false;
  })

  updater();
});


</script>
@endsection