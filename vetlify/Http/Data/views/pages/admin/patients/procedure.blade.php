@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Procedure')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Procedure</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Clinic notes</h3>
              <div class="box-tools">
                <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ Request::route('id') }}/procedure/create" class="btn btn-success btn-sm btn-view">Create Procedure</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover custom-table table-view">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Purpose of Visit</th>
                    <th>Investigations</th>
                    <th>Diagnoses</th>
                    <th>Notes</th>
                    <th>Date Created</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>

            <div class="box-footer clearfix">
              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

        </style>


              
            </div>
          
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      

    </section>
    <!-- /.content -->

  <div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Procedure</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this procedure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods
])
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/{{ Request::route('id') }}/procedures',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td>${item.compliants.join(', ')}</td>
            <td>${item.investigations.join(', ')}</td>
            <td>${item.diagnoses.join(', ')}</td>
            <td>${item.notes.join(', ')}</td>
            <td>${moment(item.created_at).format('MMMM DD, YYYY')}</td>
            <td>

              <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ Request::route('id') }}/procedure/${item.id}" data-obj='${JSON.stringify(item)}' class="btn btn-primary btn-xs btn-view">View</a>
              <a href="#" data-obj='${JSON.stringify(item)}' class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#modal-delete">Delete</a>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/procedures/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  filter();
  
});


</script>
@endsection