@extends('Vetlify::layouts.admin')

@section('page_title', 'Owner')
@section('stylesheet')
  <!-- Calendar -->
  <link rel="stylesheet" href="/css/fullcalendar.min.css">
  <link rel="stylesheet" href="/css/fullcalendar.print.min.css" media="print">
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Owner        
        <small>List of information about the owner</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Profile</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle patient-circle" src="{{ $client->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/client/avatar/medium/'.$client->avatar : '/images/avatar/thumbnail/noavatar.png' }}" alt="User profile picture">

              <h3 class="profile-username text-center"></h3>

              <input class="btn btn-primary btn-block hidden-upload-avatar hide" type="file">
              <a href="#" class="btn btn-primary btn-block upload-avatar"><b>Change Avatar</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Information</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/profile/{{ Request::route('id') }}"><i class="fa fa-paw"></i> Patient</a></li>
                <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/{{ Request::route('id') }}/setting"><i class="fa fa-hospital-o"></i> Owner Setting</a></li>
                <li><a href="#" data-toggle="modal" data-target="#modal-patient-create"><i class="fa fa-plus"></i> Add Patient</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          
        </div>

        
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Client Information</h3>
            </div>

            <div class="box-body">
                <div id="settings">
                    <div class="alert alert-success alert-setting-success collapse">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Success!</strong> Successfully update the client information
                        <ul></ul>
                    </div>

                    <form class="form-horizontal form-setting" method="post">
                        <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
                        </div>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">RFID</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rfid" name="rfid" placeholder="RFID...">
                        </div>
                        </div>

                        <div class="form-group">
                            <label for="patient_gender_id" class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="patient_gender_id" name="patient_gender_id"></select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">Contact</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Mobile No...">
                        </div>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="telephone_no" name="telephone_no" placeholder="Telephone No...">
                        </div>
                        </div>

                        <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success update-setting">Update Profile</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
          
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      

    </section>
    <!-- /.content -->

@endsection
@section('script')
<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  function Profile() {
    this.userTimeLine = {};
    this.userSetting = {!! $client !!};
    this.userGenderList = {!! $genders !!};
    this.userBloodList = {!! $bloods !!};
  }

  Profile.prototype.datePicker = function() {
    var start = moment(this.userSetting.birth_date);

    $('#client-birthdate span').html(start.format('MMMM D, YYYY'));

    $('#client-birthdate').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    }, (start, end) => {
      $('#client-birthdate span').html(start.format('MMMM D, YYYY'));
      $('#birth_date').val(start.format('YYYY-MM-DD 00:00:00'));
    });
  }

  Profile.prototype.setting = function() {
    var setting = this.userSetting;
    var fullname = `${setting.first_name} ${setting.last_name}`;

    for(var i in this.userSetting) {
      var tagName = $(`#${i}`).prop('tagName');
      var tagType = $(`#${i}`).attr('type');

      if ((tagName == 'INPUT' || tagName == 'SELECT') && tagType !== 'file') {
        $(`#${i}`).val(this.userSetting[i]);
      }
    }
    $('.profile-username').html(fullname);
    $('.box-profile .text-muted').html('Patient ID: ' + setting.patient_record_id);
  }

  Profile.prototype.loadList = function(elem, data) {
    for(var i=0; i<this[data].length; i++) {
      var item = this[data][i];
      $(elem).append($('<option>', { value: item.id, text: item.name }));
    }
  }

  Profile.prototype.init = function() {
    this.loadList('#patient_blood_id', 'userBloodList');
    this.loadList('#patient_gender_id', 'userGenderList');
    console.log(this.userSetting)
    this.datePicker();
    this.setting();

    var $newOption = $("<option></option>").val(this.userSetting.id).text(`${this.userSetting.first_name} ${this.userSetting.last_name}`);
    $("#client_id").append($newOption).trigger('change');
    $('#client_id').prop('disabled', 'disabled')
    $('.add-client').remove();
  }
  
  var profile = new Profile();
  profile.init();

  $(document).on('submit', '.form-setting', function() {
    var formData = $(this).serializeArray();
    var button = vetlify.button('.update-setting');
    var alertList = ['success', 'failed'];
    button.enableButton(true)
      .loadingButton(true);
    console.log(formData)
    for(var i in alertList) {
      $(`.alert-setting-${alertList[i]}`).hide();
    }

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/clients/' + profile.userSetting.id,
      data: formData,
      success: function (data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.alert-setting-success').show();

        profile.userSetting = data;
        profile.avatar();
        profile.setting();
      },
      error: function (resp, status, xhr) {
        button.enableButton(false)
          .loadingButton(false);

        if (resp.status == 422) {
          vetlify.errorHandler('.alert-setting-failed', resp.responseJSON.errors);
          $('.alert-setting-failed').show();
        }        
      }
    });

    return false;
  });

  $(document).on('click', '.upload-avatar', function() {
    if ($(this).find('.button-spinner').length) return false;

    $('.hidden-upload-avatar').trigger('click');
  });
  $(document).on('change', '.hidden-upload-avatar', function() {

    if ($(this)[0].files.length == 0) return false;

    var fd = new FormData();    
    var avatar = $(this)[0].files[0];
    var button = vetlify.button('.upload-avatar');
    fd.append('avatar', avatar);
    
    button.enableButton(true)
      .loadingButton(true);
    $.ajax({
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/clients/' + profile.userSetting.id + '/avatar',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        button.enableButton(false)
          .loadingButton(false);
        $('.patient-circle').attr('src', `${globalConfig.awsPUrl}${globalConfig.appName}/client/avatar/medium/${data.avatar}`);
      }
      error: function() {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('click', '#btn-md-patient-create', function() {
    vetlify.invCreatePatient(this, function(data) {
      
      $('.content .col-md-9').append(`
        <div class="col-md-6">
          <div class="box box-widget widget-user-2">
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="/images/avatar/thumbnail/noavatar.png" alt="User Avatar">
              </div>
              <h3 class="widget-user-username">${data.client_first_name}</h3>
              <h5 class="widget-user-desc">October 25, 2018</h5>
              <h6 class="pull-right" style="margin: -25px 0; font-size: 15px;">
                <a href="/app/patients/${data.patient_id}/procedure" target="_blank" style="color: #FFF !important">
                  <i class="fa fa-external-link" aria-hidden="true"></i>
                </a>
              </h6>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="/app/patients/${data.patient_id}/procedure">Procedure <span class="pull-right badge bg-blue">0</span></a></li>
                <li><a href="/app/patients/${data.patient_id}/files">Files <span class="pull-right badge bg-aqua">0</span></a></li>
                <li><a href="/app/patients/${data.patient_id}/payments">Payments <span class="pull-right badge bg-green">0</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
      `);

    }, function(data) {

    })
  });

});
</script>
@endsection