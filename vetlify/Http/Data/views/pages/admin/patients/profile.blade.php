@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients')
@section('stylesheet')
  <!-- Calendar -->
  <link rel="stylesheet" href="/css/fullcalendar.min.css">
  <link rel="stylesheet" href="/css/fullcalendar.print.min.css" media="print">
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Profile</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle patient-circle" src="{{ $patient->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/patient/avatar/medium/'.$patient->avatar : '/images/avatar/thumbnail/noavatar.png' }}" alt="User profile picture">

              <h3 class="profile-username text-center"></h3>

              <p class="text-muted text-center"></p>

              <input class="btn btn-primary btn-block hidden-upload-avatar hide" type="file">
              <a href="#" class="btn btn-primary btn-block upload-avatar"><b>Change Avatar</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Medical</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"><i class="fa fa-hospital-o"></i> Timeline</a></li>
                
                <li><a href="#"><i class="fa fa-plus-square"></i> Procedure
                  <span class="label label-primary pull-right">12</span></a>
                </li>
                <li><a href="#"><i class="fa fa-stethoscope"></i> Sent</a></li>
                <li><a href="#"><i class="fa fa-user-md"></i> Drafts</a></li>
                <li><a href="#"><i class="fa fa-wheelchair"></i> Junk <span class="label label-warning pull-right">65</span></a>
                </li>
                <li><a href="#"><i class="fa fa-heartbeat"></i> Others</a></li>
                <li><a href="#"><i class="fa fa-cart-arrow-down"></i> Invoice</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">About</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Calendar</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Settings</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li><a href="#activity" data-toggle="tab">Activity</a></li>
              <li class="active"><a href="#calendar" data-toggle="tab">Calendar</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="activity">
               


              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="calendar">
                <div class="row">
                  <div class="col-sm-12 pull-right">
                    <button type="submit" class="btn btn-success update-setting">Update Profile</button>
                  </div>
                </div>
                <div id="profile-calendar"></div>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">

                <form class="form-horizontal form-setting" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
                    </div>

                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name...">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="patient_record_id" class="col-sm-2 control-label">Patient ID</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="patient_record_id" name="patient_record_id" placeholder="Patient record ID...">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">RFID</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="rfid" name="rfid" placeholder="RFID...">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="patient_gender_id" class="col-sm-2 control-label">Gender</label>

                    <div class="col-sm-10">
                      <select class="form-control" id="patient_gender_id" name="patient_gender_id"></select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="patient_blood_id" class="col-sm-2 control-label">Blood</label>

                    <div class="col-sm-10">
                      <select class="form-control" id="patient_blood_id" name="patient_blood_id"></select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="patient_blood_id" class="col-sm-2 control-label">Birth Date</label>

                    <div class="col-sm-10">
                      <input type="hidden" id="birth_date" name="birth_date">
                      <div id="client-birthdate" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%; border: 1px solid #CCC; width: 100%">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar client-filter-date"></i>&nbsp;
                        <span></span> <b class="caret"></b>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success update-setting">Update Profile</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      

    </section>
    <!-- /.content -->

  <div id="profile-calendar"></div>

@endsection
@section('script')
<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  function Profile() {
    this.userTimeLine = {};
    this.userSetting = {!! $patient !!};
    this.userGenderList = {!! $genders !!};
    this.userBloodList = {!! $bloods !!};
  }

  Profile.prototype.datePicker = function() {
    var start = moment(this.userSetting.birth_date);

    $('#client-birthdate span').html(start.format('MMMM D, YYYY'));

    $('#client-birthdate').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    }, (start, end) => {
      $('#client-birthdate span').html(start.format('MMMM D, YYYY'));
      $('#birth_date').val(start.format('YYYY-MM-DD 00:00:00'));
    });
  }

  Profile.prototype.setting = function() {
    var setting = this.userSetting;
    var fullname = `${setting.first_name} ${setting.last_name}`;

    for(var i in this.userSetting) {
      var tagName = $(`#${i}`).prop('tagName');
      var tagType = $(`#${i}`).attr('type');

      if ((tagName == 'INPUT' || tagName == 'SELECT') && tagType !== 'file') {
        $(`#${i}`).val(this.userSetting[i]);
      }
    }
    $('.profile-username').html(fullname);
    $('.box-profile .text-muted').html('Patient ID: ' + setting.patient_record_id);
  }

  Profile.prototype.loadList = function(elem, data) {
    for(var i=0; i<this[data].length; i++) {
      var item = this[data][i];
      $(elem).append($('<option>', { value: item.id, text: item.name }));
    }
  }

  Profile.prototype.init = function() {
    this.loadList('#patient_blood_id', 'userBloodList');
    this.loadList('#patient_gender_id', 'userGenderList');
    this.datePicker();
    this.setting();
  }
  
  var profile = new Profile();
  profile.init();

  $(document).on('submit', '.form-setting', function() {
    var formData = $(this).serializeArray();
    var button = vetlify.button('.update-setting');
    var alertList = ['success', 'failed'];
    button.enableButton(true)
      .loadingButton(true);

    for(var i in alertList) {
      $(`.alert-setting-${alertList[i]}`).hide();
    }

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/' + profile.userSetting.id,
      data: formData,
      success: function (data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.alert-setting-success').show();

        profile.userSetting = data;
        profile.avatar();
        profile.setting();
      },
      error: function (resp, status, xhr) {
        button.enableButton(false)
          .loadingButton(false);

        if (resp.status == 422) {
          vetlify.errorHandler('.alert-setting-failed', resp.responseJSON.errors);
          $('.alert-setting-failed').show();
        }        
      }
    });

    
    return false;
  });

  $(document).on('click', '.upload-avatar', function() {
    if ($(this).find('.button-spinner').length) return false;

    $('.hidden-upload-avatar').trigger('click');
  });

  $(document).on('change', '.hidden-upload-avatar', function() {

    if ($(this)[0].files.length == 0) return false;

    var fd = new FormData();    
    var avatar = $(this)[0].files[0];
    var button = vetlify.button('.upload-avatar');
    fd.append('avatar', avatar);
    
    button.enableButton(true)
      .loadingButton(true);
    $.ajax({
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/' + profile.userSetting.id + '/avatar',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        button.enableButton(false)
          .loadingButton(false);
        $('.patient-circle').attr('src', `${globalConfig.awsPUrl}${globalConfig.appName}/patient/avatar/thumbnail/${data.avatar}`);
      }
    });
  });

  $(function () {


    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#profile-calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      //Random default events
      events    : [
        {
          title          : 'All Day Event',
          start          : new Date(y, m, 1),
          backgroundColor: '#f56954', //red
          borderColor    : '#f56954' //red
        },
        {
          title          : 'Long Event',
          start          : new Date(y, m, d - 5),
          end            : new Date(y, m, d - 2),
          backgroundColor: '#f39c12', //yellow
          borderColor    : '#f39c12' //yellow
        },
        {
          title          : 'Meeting',
          start          : new Date(y, m, d, 10, 30),
          allDay         : false,
          backgroundColor: '#0073b7', //Blue
          borderColor    : '#0073b7' //Blue
        },
        {
          title          : 'Lunch',
          start          : new Date(y, m, d, 12, 0),
          end            : new Date(y, m, d, 14, 0),
          allDay         : false,
          backgroundColor: '#00c0ef', //Info (aqua)
          borderColor    : '#00c0ef' //Info (aqua)
        },
        {
          title          : 'Birthday Party',
          start          : new Date(y, m, d + 1, 19, 0),
          end            : new Date(y, m, d + 1, 22, 30),
          allDay         : false,
          backgroundColor: '#00a65a', //Success (green)
          borderColor    : '#00a65a' //Success (green)
        },
        {
          title          : 'Click for Google',
          start          : new Date(y, m, 28),
          end            : new Date(y, m, 29),
          url            : 'http://google.com/',
          backgroundColor: '#3c8dbc', //Primary (light-blue)
          borderColor    : '#3c8dbc' //Primary (light-blue)
        }
      ],
      editable  : true,
      droppable : true
    })

  })

});
</script>
@endsection