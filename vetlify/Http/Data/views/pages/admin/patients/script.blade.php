<script type="text/javascript">

function Profile() {
  this.userTimeLine = {};
  this.userSetting = {!! $patients !!};
  this.userGenderList = {!! $genders !!};
  this.userBloodList = {!! $bloods !!};
  this.userBranch = {!! $branch !!};
  this.paymentMethod = {!! $paymentMethod !!};
}

Profile.prototype.setting = function() {
  var setting = this.userSetting;
  var fullname = `${setting.first_name}`;

  for(var i in this.userSetting) {
    var tagName = $(`#${i}`).prop('tagName');
    var tagType = $(`#${i}`).attr('type');

    if ((tagName == 'INPUT' || tagName == 'SELECT') && tagType !== 'file') {
      $(`#${i}`).val(this.userSetting[i]);
    }
  }
  $('.profile-username').html(fullname);
  $('.box-profile .text-muted').html('Patient ID: ' + setting.patient_record_id);
}

Profile.prototype.loadList = function(elem, data) {
  for(var i=0; i<this[data].length; i++) {
    var item = this[data][i];
    $(elem).append($('<option>', { value: item.id, text: item.name }));
  }
}

Profile.prototype.init = function() {
  this.loadList('#patient_blood_id', 'userBloodList');
  this.loadList('#patient_gender_id', 'userGenderList');
  this.setting();
}

var profile = new Profile();
profile.init();

$(document).ready(function() {
  $(document).on('click', '.upload-avatar', function() {
    if ($(this).find('.button-spinner').length) return false;

    $('.hidden-upload-avatar').trigger('click');
  });

  $(document).on('change', '.hidden-upload-avatar', function() {

    if ($(this)[0].files.length == 0) return false;

    var fd = new FormData();    
    var avatar = $(this)[0].files[0];
    var button = vetlify.button('.upload-avatar');
    fd.append('avatar', avatar);
    
    button.enableButton(true)
      .loadingButton(true);
    $.ajax({
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/' + profile.userSetting.id + '/avatar',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        button.enableButton(false)
          .loadingButton(false);
        $('.patient-circle').attr('src', `${globalConfig.awsPUrl}${globalConfig.appName}/patient/avatar/thumbnail/${data.avatar}`);
      },
      error: function() {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });
});
</script>
