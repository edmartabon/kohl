@extends('Vetlify::layouts.admin')

@section('page_title', 'Patients / Setting')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
        <small>List of information about the patient</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients">Patient</a></li>
        <li><a href="#">Setting</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          @include('Vetlify::pages.admin.patients.sidebar', [
            'clientId' => Request::route('id')
          ])
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Patient Information</h3>
            </div>

            <div class="box-body">
                <div id="settings">
                    <form class="form-horizontal form-setting" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name...">
                        </div>

                        </div>

                        <div class="form-group">
                        <label for="patient_record_id" class="col-sm-2 control-label">Patient ID</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="patient_record_id" name="patient_record_id" placeholder="Patient record ID...">
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">RFID</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rfid" name="rfid" placeholder="RFID...">
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="patient_gender_id" class="col-sm-2 control-label">Gender</label>

                        <div class="col-sm-10">
                            <select class="form-control" id="patient_gender_id" name="patient_gender_id"></select>
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="patient_blood_id" class="col-sm-2 control-label">Blood</label>

                        <div class="col-sm-10">
                            <select class="form-control" id="patient_blood_id" name="patient_blood_id"></select>
                        </div>
                        </div>

                        <!-- <div class="form-group">
                        <label for="patient_blood_id" class="col-sm-2 control-label">Birth Date</label>

                        <div class="col-sm-10">
                            <input type="hidden" id="birth_date" name="birth_date">
                            <div id="client-birthdate" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%; border: 1px solid #CCC; width: 100%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar client-filter-date"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                            </div>
                        </div>
                        </div> -->

                        <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success update-setting">Update Profile</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
          
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      

    </section>
    <!-- /.content -->

  <div id="profile-calendar"></div>

@endsection
@section('script')
@include('Vetlify::pages.admin.patients.script', [
  'clientId' => Request::route('id'),
  'patients' => $patient,
  'genders' => $genders,
  'bloods' => $bloods
])
<script type="text/javascript">

  $(document).on('submit', '.form-setting', function() {
    var formData = $(this).serializeArray();
    var button = vetlify.button('.update-setting');
    var alertList = ['success', 'failed'];
    button.enableButton(true)
      .loadingButton(true);

    for(var i in alertList) {
      $(`.alert-setting-${alertList[i]}`).hide();
    }

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/patients/' + profile.userSetting.id,
      data: formData,
      success: function (data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.alert-setting-success').show();

        profile.userSetting = data;
        // profile.avatar();
        profile.setting();
      },
      error: function (resp, status, xhr) {
        button.enableButton(false)
          .loadingButton(false);

        if (resp.status == 422) {
          vetlify.errorHandler('.alert-setting-failed', resp.responseJSON.errors);
          $('.alert-setting-failed').show();
        }        
      }
    });

    return false;
  });

</script>
@endsection