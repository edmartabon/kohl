<div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle patient-circle" src="{{ $patient->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/patient/avatar/medium/'.$patient->avatar : '/images/avatar/thumbnail/noavatar.png' }}" alt="User profile picture">

              <h3 class="profile-username text-center"></h3>

              <p class="text-muted text-center"></p>

              <input class="btn btn-primary btn-block hidden-upload-avatar hide" type="file">
              <a href="#" class="btn btn-primary btn-block upload-avatar"><b>Change Avatar</b></a>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Medical</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <!-- <li><a href="#"><i class="fa fa-hospital-o"></i> Timeline</a></li> -->
                
                <li>
                  <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/procedure">
                    <i class="fa fa-plus-square"></i> Clinic Notes
                    <!-- <span class="label label-primary pull-right">12</span> -->
                  </a>
                </li>
                <!-- <li>
                  <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/treatments">
                    <i class="fa fa-stethoscope"></i> Treament Plan
                  </a>
                </li> -->

                <li>
                  <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/files">
                    <i class="fa fa-suitcase"></i> Files
                    <!-- <span class="label pull-right custom-green">12</span> -->
                  </a>
                </li>

                <!-- <li>
                  <a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/prescriptions">
                    <i class="fa fa-medkit"></i> Prescription
                  </a>
                </li> -->
              </ul>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Billing</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/payments"><i class="fa fa-shopping-cart"></i> Payment</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">About</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/appointments"><i class="fa fa-circle-o text-red"></i> Appointment</a></li>
                <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/patients/{{ $clientId }}/setting"><i class="fa fa-circle-o text-yellow"></i> Settings</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>