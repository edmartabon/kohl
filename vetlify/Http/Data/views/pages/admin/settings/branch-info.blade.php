@extends('Vetlify::layouts.admin')

@section('page_title', 'Update Branch')

@section('stylesheet')
  <link rel="stylesheet" href="/css/timepicker.min.css">
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Branch
        <small>Branch Information</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="/{{ Config::get('vetlify.app_route_prefix') }}/setting/branch-info">Branches</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Branch Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <form class="form-horizontal" method="post" id="update-form-submit">
                <div class="form-group">
                  <label for="name" class="col-sm-2">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Branch Name...">
                  </div>
                </div>

                <div class="form-group">
                  <label for="address_one" class="col-sm-2">Address One</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address_one" name="address_one" placeholder="Address One...">
                  </div>
                </div>
                
                <div class="form-group hide">
                  <label for="address_two" class="col-sm-2">Address Two</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address_two" name="address_two" placeholder="Address Two...">
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_no" class="col-sm-2">Contact No</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No...">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="phone_no" class="col-sm-2">Mobile No.</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No...">
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_no" class="col-sm-2">Working Days</label>
                  <div class="col-sm-5">
                    <select class="form-control" id="working_start_day">
                      <option value="monday">Monday</option>                    
                      <option value="tuesday">Tuesday</option>                    
                      <option value="wednesday">Wednesday</option>                    
                      <option value="thursday">Thursday</option>                    
                      <option value="friday">Friday</option>                    
                      <option value="saturday">Saturday</option>                    
                      <option value="sunday">Sunday</option>                    
                    </select>
                  </div>
                  
                  <div class="col-sm-5">
                    <select class="form-control" id="working_end_day">
                      <option value="monday">Monday</option>                    
                      <option value="tuesday">Tuesday</option>                    
                      <option value="wednesday">Wednesday</option>                    
                      <option value="thursday">Thursday</option>                    
                      <option value="friday">Friday</option>                    
                      <option value="saturday">Saturday</option>                    
                      <option value="sunday">Sunday</option>                    
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_no" class="col-sm-2">Working Hour</label>
                  <div class="col-sm-5">
                    <div class="input-group bootstrap-timepicker timepicker">
                      <input id="working_start_at" type="text" name="working_start_at" class="form-control input-small">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                  </div>
                  
                  <div class="col-sm-5">
                    <div class="input-group bootstrap-timepicker timepicker">
                      <input id="working_end_at" type="text" name="working_end_at" class="form-control input-small">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-10">
                    <input type="submit" class="btn btn-success update-branch-info" value="Update">
                  </div>

                </div>  
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modal-edit">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Branch</h4>
          </div>
          <div class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success btn-edit-confirm">Update</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/timepicker.min.js"></script>
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};
  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/branches/{{ $branchId }}',
      success: function(data) {
        var startDay = data.working_start_day ? moment(data.working_start_day).format('dddd').toString() : '',
          endDay = data.working_end_day ? moment(data.working_end_day).format('dddd').toString() : '';
          startAt = data.working_start_at ? moment(data.working_start_at).format('hh:mm A') : '';
          endAt = data.working_end_at ? moment(data.working_end_at).format('hh:mm A') : '';

        for(var i in data) {
          $(`#${i}`).val(data[i]);
        }

        $('#working_start_day').val(startDay.toLowerCase());
        $('#working_end_day').val(endDay.toLowerCase());
        $('#working_start_at').val(startAt);
        $('#working_end_at').val(endAt);
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>${(data.per_page *(data.current_page-1)+(num)+ 1)}.</td>
            <td id="name">${item.name}</td>
            <td id="address_one">${item.address_one}</td>
            <td id="contact_no">${item.contact_no}</td>  
            <td style="display: inline">
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-eye"></i> Edit</button>
              <button type="button" data-obj='${JSON.stringify(item)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '#btn-md-branch-create', function() {

    vetlify.settingCreateBranch(this, function(data) {
      var table = $('.table-view tbody'),
        totalTr = table.find('tr');
    
      table.append(`
        <tr>
          <td>${totalTr.length + 1}.</td>
          <td id="name">${data.name}</td>
          <td id="address_one">${data.address_one}</td>
          <td id="contact_no">${data.contact_no}</td>  
          <td style="display: inline">
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-primary btn-sm btn-edit pull-left" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-eye"></i> Edit</button>
            <button type="button" data-obj='${JSON.stringify(data)}' class="btn btn-danger btn-sm btn-delete " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
          </td>
        </tr>
      `);
    }, function(data) {

    })
  });
  
  $(document).on('click', '.btn-edit', function() {
    var content = $(this).data('obj');
    vetlify.formAutoFill('#modal-edit', content);
    
    $('.btn-edit-confirm').data('id', content.id);
    $('.btn-edit-confirm').data('index', $(this).parent().parent().index());
  });

  $(document).on('submit', '#update-form', function() {
    var _this = $(this);
    var button = vetlify.button('#udate-form');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/branches/{{ $branchId }}',
      data:_this.parent().parent().find('form').serialize(),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);

      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
    return false;
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('obj').id);
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    var button = vetlify.button('.btn-delete-confirm');
    button.enableButton(true)
      .loadingButton(true);

    $.ajax({
      type: 'DELETE',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/branches/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        button.enableButton(false)
          .loadingButton(false);
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  $(document).on('submit', '#update-form-submit', function() {
    var data = $(this).serialize();
    var workingAt = $('#working_start_at').val(),
      workingEndAt = $('#working_end_at').val();

    data = data + `&working_start_day=${moment().day($('#working_start_day').val()).format('YYYY-MM-DD 00:00:00')}`;
    data = data + `&working_end_day=${moment().day($('#working_end_day').val()).format('YYYY-MM-DD 00:00:00')}`;

    if (workingAt) {
      data = data + `&working_start_at=${moment(moment().format('YYYY-MM-DD ')+$('#working_start_at').val()).format('YYYY-MM-DD HH:mm:00')}`;
    }

    if (workingEndAt) {
      data = data + `&working_end_at=${moment(moment().format('YYYY-MM-DD ')+$('#working_end_at').val()).format('YYYY-MM-DD HH:mm:00')}`;
    }

    $.ajax({
      type: 'PUT',
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/branches/{{ $branchId }}',
      data: data,
      success: function(data) {
        var startDay = data.working_start_day ? moment(data.working_start_day).format('dddd').toString() : '',
          endDay = data.working_end_day ? moment(data.working_end_day).format('dddd').toString() : '';
          startAt = data.working_start_at ? moment(data.working_start_at).format('hh:mm A') : '';
          endAt = data.working_end_at ? moment(data.working_end_at).format('hh:mm A') : '';

        for(var i in data) {
          $(`#${i}`).val(data[i]);
        }

        $('#working_start_day').val(startDay.toLowerCase());
        $('#working_end_day').val(endDay.toLowerCase());
        $('#working_start_at').val(startAt);
        $('#working_end_at').val(endAt);
      }
    });
    return false;
  });

  filter();
  $('#working_start_at').timepicker(); 
  $('#working_end_at').timepicker(); 
});
</script>
@endsection