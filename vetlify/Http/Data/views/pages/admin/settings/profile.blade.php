@extends('Vetlify::layouts.admin')

@section('page_title', 'Use Profile')
@section('stylesheet')
  <!-- Calendar -->
  <link rel="stylesheet" href="/css/fullcalendar.min.css">
  <link rel="stylesheet" href="/css/fullcalendar.print.min.css" media="print">
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
        <small>Modify the information of current user</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/{{ $link->home }}"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="#">Setting</a></li>
        <li><a href="#">Profile</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{ $user->avatar ? Config::get('vetlify.aws_public_url').Request::route('account').'/avatar/medium/'.$user->avatar : '/images/avatar/thumbnail/noavatar.png' }}" alt="User profile picture">

              <h3 class="profile-username text-center"></h3>
              
              <input class="btn btn-primary btn-block hidden-upload-avatar hide" type="file">
              <a href="#" class="btn btn-primary btn-block upload-avatar"><b>Change Avatar</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <!-- general form elements -->
            <!-- form start -->

            <div class="col-md-8">
              <div class="alert alert-success alert-setting-success collapse">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> Successfully update the user information
                <ul></ul>
              </div>

              <form role="form" class="user-profile">
                <div class="box-body">
                  
                  <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                  </div>
                  
                  <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                  </div>

                  <div class="form-group">
                    <label for="last_name">Address</label>
                    <input type="text" class="form-control" id="address_one" name="address_one" placeholder="Address">
                  </div>

                  <div class="form-group hide">
                    <label for="last_name">Adress Two</label>
                    <input type="text" class="form-control" id="address_two" name="address_two" placeholder="Adress Two">
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="last_name">Mobile No.</label>
                        <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No.">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="last_name">Telephone No.</label>
                        <input type="text" class="form-control" id="telephone_no" name="telephone_no" placeholder="Telephone No.">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                  </div>
                    
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  </div>

                  <button type="submit" class="btn btn-success submit-update-profile">Update Profile</button>
                </div>
                <!-- /.box-body -->

                
              </form>
            </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      

    </section>
    <!-- /.content -->
@endsection
@section('script')
<script type="text/javascript">

var users = {!! $users  !!};

$(document).ready(function() {
  $(document).on('click', '.upload-avatar', function() {
    if ($(this).find('.button-spinner').length) return false;

    $('.hidden-upload-avatar').trigger('click');
  });

  $(document).on('change', '.hidden-upload-avatar', function() {

    if ($(this)[0].files.length == 0) return false;

    var fd = new FormData();    
    var avatar = $(this)[0].files[0];
    var button = vetlify.button('.upload-avatar');
    fd.append('avatar', avatar);
    
    button.enableButton(true)
      .loadingButton(true);
    $.ajax({
      url: '/{{ Config::get('vetlify.app_route_prefix') }}/api/users/current/avatar',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        button.enableButton(false)
          .loadingButton(false);
        $('.img-circle, .user-image, .profile-user-img').attr('src', `${globalConfig.awsPUrl}${globalConfig.appName}/avatar/thumbnail/${data.avatar}`);
        $('.profile-user-img').attr('src', `${globalConfig.awsPUrl}${globalConfig.appName}/avatar/medium/${data.avatar}`);
      },
      error: function () {
        button.enableButton(false)
          .loadingButton(false);
      }
    });
  });

  $(document).on('submit', '.user-profile', function() {
    var formData = $(this).serializeArray();
    var button = vetlify.button('.submit-update-profile');
    var alertList = ['success', 'failed'];
    button.enableButton(true)
      .loadingButton(true);

    for(var i in alertList) {
      $(`.alert-setting-${alertList[i]}`).hide();
    }

    $.ajax({
      type: 'POST',
      url: `/${globalConfig.linkApp}/api/users/current`,
      data: formData,
      success: function (data) {
        button.enableButton(false)
          .loadingButton(false);

        users = data;
      },
      error: function (resp, status, xhr) {
        button.enableButton(false)
          .loadingButton(false);

      }
    });

    return false;
  });

  vetlify.formAutoFill('.user-profile', users);
});
</script>
@endsection