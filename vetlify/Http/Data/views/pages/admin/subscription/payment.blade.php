@extends('Vetlify::layouts.login_registration')

@section('page_title', 'Payment subscription')

@section('page_type', 'register-page')

@section('contents')
<div class="register-box" style="margin-top: 25px">
  <div class="register-logo">
    <a href="{{ Config::get('vetlify.main_page_url') }}">{{ Config::get('vetlify.page_title') }}</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Payment for the subscription</p>

    <form action="/" method="post" id="register-account">
      {!! csrf_field() !!}

      <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
        <input type="Store" class="form-control" name="card_number" placeholder="Card Number" value="{{old('name')}}">
        <span class="fa fa-credit-card form-control-feedback"></span>
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
      </div>

     
      <div class="form-group has-feedback {{ $errors->has('first_name') ||  $errors->has('last_name') ? 'has-error' : '' }}">
        <div class="row">
          <div class="col-xs-6">
            <input type="text" class="form-control" name="expiry_date" value="{{old('first_name')}}" placeholder="Expiration Date">
          </div>
          <div class="col-xs-6">
            <input type="text" class="form-control" name="ccv" value="{{old('last_name')}}" placeholder="CCV">
          </div>
        </div>
      </div>
      
      <div class="row">
        <!-- <div class="col-xs-8">
          <img src="/images/cards_available.png" style="margin-top: -6px;">
        </div> -->
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Pay 2500 PHP</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.form-box -->
</div>

@endsection
@section('scripts')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $('#register-account').submit(function(e) {
    if(!$("#accept-condition").is(':checked')) {
      $("#accept-condition").parent().parent().parent().addClass('has-error')
      e.preventDefault();
    }    
  });
</script>
@endsection