@extends('Vetlify::layouts.login_registration')

@section('page_title', 'Register')

@section('page_type', 'register-page')

@section('contents')
<div class="register-box" style="margin-top: 25px">
  <div class="register-logo">
    <a href="{{ Config::get('vetlify.main_page_url') }}">{{ Config::get('vetlify.page_title') }}</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="/register" method="post" id="register-account">
      {!! csrf_field() !!}

      <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
        <input type="Store" class="form-control" name="name" placeholder="Clinic Name" value="{{old('name')}}">
        <span class="glyphicon glyphicon-home form-control-feedback"></span>
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('server') ? 'has-error' : '' }}">
        <div class="input-group">
          <input type="text" class="form-control" name="server" placeholder="Store_Name" value="{{old('server')}}">
          <span class="input-group-addon">.vetlify.com</span>
        </div>
        {!! $errors->first('server','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('address_one') ? 'has-error' : '' }}">
        <input type="Store" class="form-control" name="address_one" placeholder="Address" value="{{old('address_one')}}">
        <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
        {!! $errors->first('address_one','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('phone_no') ? 'has-error' : '' }}">
        <input type="Store" class="form-control" name="phone_no" placeholder="Phone No" value="{{old('phone_no')}}">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
        {!! $errors->first('phone_no','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('first_name') ||  $errors->has('last_name') ? 'has-error' : '' }}">
        <div class="row">
          <div class="col-xs-6">
            <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" placeholder="First Name">
          </div>
          <div class="col-xs-6">
            <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" placeholder="Last Name">
          </div>
        </div>
        @if(count($errors->get('first_name')) || count($errors->get('last_name')))
          <span class="help-block">First name and last name must fill.</span>
          @endif
      </div>

      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        {!! $errors->first('password','<span class="help-block">:message</span>') !!}
      </div>

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
        <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        {!! $errors->first('password_confirmation','<span class="help-block">:message</span>') !!}
      </div>

      @if (Session::has('addCaptcha'))
      <div class="row">
        <div class="col-xs-12 {{ \Session::has('captcha_error') ? 'has-error' : '' }}">
          <div class="g-recaptcha" data-sitekey="6LdQPHMUAAAAAIVz2gqGY5z7SCGmv7TT0LV0HQ_i"></div>
          {!! \Session::has('captcha_error') ? '<span class="help-block">Invalid captcha. Try again.</span>' : '' !!}
          <br>
        </div>
      </div>
      @endif
      
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck form-group">
            <label>
              <input type="checkbox" id="accept-condition"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.form-box -->
</div>
@endsection
@section('scripts')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $('#register-account').submit(function(e) {
    if(!$("#accept-condition").is(':checked')) {
      $("#accept-condition").parent().parent().parent().addClass('has-error')
      e.preventDefault();
    }    
  });
</script>
@endsection