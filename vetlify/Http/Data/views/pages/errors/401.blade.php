@extends('Vetlify::layouts.admin')

@section('page_title', 'Unauthorized')

@section('content')
<section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 401</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Whoops! You don't have permission to view this page.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="/app">return to dashboard</a>
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>

    
@endsection
