<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en">        <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en">               <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Vetlify | Sub Page</title>
    <link rel="icon" type="image/png" href="{{ url('image/icon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ url('image/icon/favicon-16x16.png') }}" sizes="16x16" />
    <!-- CSS -->
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
  </head>
  <body>
    <div class="main-body">
      <!--Navbar MOBILE-->
        <nav class="navbar mobile-nav" id="mobile-nav">

            <!-- Navbar brand -->
            <a class="navbar-brand" href="#">
              <img src="{{ url('img/logo/logo-mobile.png') }}" alt="Website Logo">
            </a>

            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent22" aria-controls="navbarSupportedContent22" aria-expanded="false" aria-label="Toggle navigation">
                <div class="animated-icon4">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
            </button>

            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent22">

                <!-- Links -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a href="#home" class="anchor">home</a>
                    </li>
                    <li class="nav-item">
                      <a href="#services" class="anchor">services</a>
                    </li>
                    <li class="nav-item">
                      <a href="#contact" class="anchor">contact</a>
                    </li>
                    <li class="nav-item">
                      <a href="/app/login">login</a>
                    </li>
                </ul>
                <!-- Links -->

            </div>
            <!-- Collapsible content -->

        </nav>
      <!--/.Navbar MOBILE-->
        <aside class="left-nav-container">
        <div class="logo-container">
          <img src="{{ url('img/logo/logo-desktop.png') }}" alt="Website Logo">
        </div><!--logo-container-->
        <div class="nav-cover">
          <nav>
            <ul>
              <li>
                <a href="#home" class="anchor">home</a>
              </li>
              <li>
                <a href="#services" class="anchor">services</a>
              </li>
              <li>
                <a href="#contact" class="anchor">contact</a>
              </li>
              <li>
                <a href="/app/login">login</a>
              </li>
            </ul>
          </nav>
        </div><!--nav-cover-->
        <div class="nav-footer">
          <a href="http://bizwex.com/" target="_blank"><span>designed by:</span><br>Bizwex Web Experts</a>
        </div><!--nav-footer-->
      </aside><!--left-nav-container-->

      <!----------------------------------------------->
      <!-- Main Content -->
      <!----------------------------------------------->
      <div class="main-content">
        <!----------------------------------------------->
        <!-- Banner Section -->
        <!----------------------------------------------->
        <section class="banner-section" id="home">
          <div class="container">
            <!----------------------------------------------->
            <!-- Contact -->
            <!----------------------------------------------->
            <div class="row">
              <div class="col-md-12">
                <div class="clinic-name">
                  <h1>Man's Best Friend Veterinary Clinic</h1>
                </div>

              </div>
              <div class="col-md-4">
                <div class="contact-container">
                  <div class="contact-logo">
                    <img src="{{ url('img/icons/scope-8.svg') }}" alt="">
                  </div>
                  <div class="contact-content">
                    <strong>Veterinarians</strong>
                    <ul>
                      <li>Dr. Inocencio Cruz III</li>
                      <li>Dr. Noeimi Tampoa</li>
                    </ul>
                  </div>
                </div><!--contact-container-->
              </div><!--col-md-4-->
              <div class="col-md-4">
                <div class="contact-container">
                  <div class="contact-logo">
                    <img src="{{ url('img/icons/time-8.svg') }}" alt="">
                  </div>
                  <div class="contact-content">
                    <strong>Working Time</strong>
                    <ul>
                      <li>Monday - Friday</li>
                      <li>8:00AM - 5:00PM</li>
                    </ul>
                  </div>
                </div><!--contact-container-->
              </div><!--col-md-4-->
              <div class="col-md-4">
                <div class="contact-container">
                  <div class="contact-logo">
                    <img src="{{ url('img/icons/clinic-8.svg') }}" alt="">
                  </div>
                  <div class="contact-content">
                    <ul>
                      <li>100 Lilac St, SSS Village, Marikina City</li>
                      <li>09177847770</li>
                    </ul>
                  </div>
                </div><!--contact-container-->
              </div><!--col-md-4-->
            </div><!--row-->
            <!----------------------------------------------->
            <!-- Slider -->
            <!----------------------------------------------->
            <div class="row">
              <div class="col-md-3">

              </div><!--col-md-4-->
              <div class="col-md-9">
                <div class="announcement-container">
                  <div class="announcement-cover" id="slick-slide">
                    <div class="item announcement">
                      <div class="title-cover">
                        <h3 class="title">Five Summer Dangers for Pets</h3>
                        <ul class="info">
                          <li>
                            <p>SAN JOSE, BALANGA CITY</p>
                          </li>
                          <li>
                            <p>JUNE 04, 2018</p>
                          </li>
                          <li>
                            <p>10:00AM - 11:00AM</p>
                          </li>
                        </ul>
                      </div>
                      <article class="caption">
                        <p>This clinic deserves all the stars. I’ve been here twice for general check-ups and vaccinations for my two foster kitties, and everyone came out happy. The place is spotless, well-furnished, all the staff members are pleasant and efficient, their email reminders are helpful, and my son and I got mini-cupcakes on our last visit!This clinic deserves all the stars. I’ve been here twice for general check-ups and vaccinations for my two foster kitties, and everyone came out happy. The place is spotless, well-furnished, all the staff members are pleasant and efficient, their email reminders are helpful, and my son and I got mini-cupcakes on our last visit!</p>
                      </article>
                      <a href="#" class="link-btn">view more <i class="fa fa-arrow-right"></i></a>
                    </div><!--item-->

                    <div class="item announcement">
                      <div class="title-cover">
                        <h3 class="title">Five Summer Dangers for Pets</h3>
                        <ul class="info">
                          <li>
                            <p>SAN JOSE, BALANGA CITY</p>
                          </li>
                          <li>
                            <p>JUNE 04, 2018</p>
                          </li>
                          <li>
                            <p>10:00AM - 11:00AM</p>
                          </li>
                        </ul>
                      </div>
                      <article class="caption">
                        <p>This clinic deserves all the stars. I’ve been here twice for general check-ups and vaccinations for my two foster kitties, and everyone came out happy. The place is spotless, well-furnished, all the staff members are pleasant and efficient, their email reminders are helpful, and my son and I got mini-cupcakes on our last visit!This clinic deserves all the stars. I’ve been here twice for general check-ups and vaccinations for my two foster kitties, and everyone came out happy. The place is spotless, well-furnished, all the staff members are pleasant and efficient, their email reminders are helpful, and my son and I got mini-cupcakes on our last visit!</p>
                      </article>
                      <a href="#" class="link-btn">view more <i class="fa fa-arrow-right"></i></a>
                    </div><!--item-->
                    <div class="item announcement">
                      <div class="title-cover">
                        <h3 class="title">Five Summer Dangers for Pets</h3>
                        <ul class="info">
                          <li>
                            <p>SAN JOSE, BALANGA CITY</p>
                          </li>
                          <li>
                            <p>JUNE 04, 2018</p>
                          </li>
                          <li>
                            <p>10:00AM - 11:00AM</p>
                          </li>
                        </ul>
                      </div>
                      <article class="caption">
                        <p>The place is spotless, well-furnished, all the staff members are pleasant and efficient, their email reminders are helpful, and my son and I got mini-cupcakes on our last visit!</p>
                      </article>
                      <a href="#" class="link-btn">view more <i class="fa fa-arrow-right"></i></a>
                    </div><!--item-->
                  </div>
                  <h1 class="type">ANNOUNCEMENT</h1>
                  <img src="{{ url("img/dog.png") }}" alt="Good Dog!" class="dog-img">
                </div>

              </div><!--col-md-9-->
            </div><!--row-->

          </div><!--container-->
        </section><!--banner-section-->
        <!----------------------------------------------->
        <!-- Services Section -->
        <!----------------------------------------------->
        <section class="services-section" id="services">

          <div class="container">
            <div class="row services-desktop">
              <div class="col-md-4">
                <div class="services-container">
                  <h1 class="h1">Services</h1>
                  <nav class="services">
                    <ul class="nav" id="nav-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#spays-and-nueters">Spays and Nueters <i class="fa fa-arrow-right"></i> </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#vaccinations">Vaccinations <i class="fa fa-arrow-right"></i> </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#surgery">Surgery <i class="fa fa-arrow-right"></i> </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#in-house-diagnostics">In-House Diagnostics <i class="fa fa-arrow-right"></i> </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#laboratory-diagnostics">Laboratory Diagnostics <i class="fa fa-arrow-right"></i> </a>
                      </li>
                    </ul>
                  </nav>
                </div><!--services-container-->
              </div><!--col-md-4-->
              <div class="col-md-8">
                <div class="services-container">
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div class="tab-pane container active" id="spays-and-nueters">
                      <div class="content">
                        <p>Man Best Friends Clinic recommends neutering your new puppy or kitten. For dogs or cats, both procedures are generally performed around six months of age. These procedures will result in your pet no longer being able to reproduce. Better overall health and behavior are key reasons to consider this procedure. In general, pets that are spayed (female) or neutered (male) live longer and reduce or eliminate the risk of serious disease. Many behavioral problems are reduced or eliminated, resulting in a calmer, better socialized companion.</p>
                        <p>Both procedures are abdominal surgeries performed under general anesthesia. Surgical time can range from 30 minutes to over an hour. All spays & neuters patients are discharged from our care on the same day. Full recovery will occur over the following 7 to 10 days following restricted activity and comfortable rest. A re-check appointment is booked to make sure the incision has healed and your furry friend is fully recovered.</p>
                      </div>
                    </div>
                    <div class="tab-pane container fade" id="vaccinations">
                      <div class="content">
                        <p>Vaccinations will prevent many major diseases from your pet. Boston Veterinary Clinic recommends creating and adhering to a customized vaccination schedule as part of a comprehensive wellness plan.</p>
                        <p>Vaccinations are sometimes referred to as “core”, which are recommended for all dogs & cats and “non-core”, which are recommended based on the individual pet and environmental risk factors.</p>
                      </div>
                    </div>
                    <div class="tab-pane container fade" id="surgery">
                      <div class="content">
                        <p>At Man Best Friends Clinic, we understand the many questions and concerns you have, and we want to place your mind at ease. Whether routine or complex, surgery can be a time filled with anxiety and uncertainty. We work hard to ensure that every surgery performed in our hospital is stress-free and as positive an experience as possible.</p>
                        <p>Our surgery department offers state-of-the-art facilities, fully equipped surgical suites and a skilled staff to ensure each patient receives the best care. From pre-op pet evaluation and procedure explanation to after-care instructions and a review of any findings, we’re with you every step of the way. Our care team remains on-hand – before, during and following surgery – to monitor your companion’s well being using the most modern equipment available. This translates to your pet’s comfort and safety at all times. We offer a variety of general surgical procedures performed to the highest standards of care.</p>
                      </div>
                    </div>
                    <div class="tab-pane container fade" id="in-house-diagnostics">
                      <div class="content">
                        <p>While person-to-person conversation helps a doctor understand a patient’s concerns, when it comes to our pets, we need to let something else do the talking. This is where veterinary diagnostics come into play. If your animal friend hasn’t been acting like his or her self and you suspect an underlying illness or injury as the cause, it might be time to do a diagnostic evaluation. This allows us to learn what’s happening inside your pet’s body, so that we can take the appropriate measures.</p>
                        <p>Boston Veterinary Clinic has invested over $250 million in state-of-the art diagnostic equipment to provide the best information to our veterinarians that reduces waiting time and improves treatment outcomes.</p>
                      </div>
                    </div>
                    <div class="tab-pane container fade" id="laboratory-diagnostics">
                      <div class="content">
                        <p>Man Best Friends Clinic uses IDEXX in-house analyzers and IDEXX Reference Laboratory to provide our patients with a complete diagnostic solution. As the industry-leader, IDEXX has led diagnostic innovation over its 30-year history, which means our veterinarians have access to the most accurate and comprehensive diagnostic results to better inform our diagnoses. Our equipment includes:</p>
                        <p>ProCyte DX: The first and only in-house hematology analyzer that combines three cutting-edge technologies for accurate results in just two minutes.</p>
                        <p>Catalyst One: In one run with one sample, we get chemistry and electrolyte results in only 8 minutes, total T4in as little as 15 minutes.</p>
                        <p>SediVue DX: The newest urine sediment analyzer that delivers consistent and accurate results in 3 minutes and produces high-resolution, high-contrast digital images that can be shared in real time.</p>
                      </div>
                    </div>
                  </div><!--tab-content-->
                </div><!--services-container-->
              </div><!--col-md-8-->
            </div><!--row-->
            <div class="row services-mobile">
              <div class="col-md-12">
                <div class="services-container">
                  <h1 class="h1">Services</h1>
                  <div id="accordion">
                      <div class="card">
                        <div class="card-header">
                          <a class="card-link" data-toggle="collapse" href="#spaysAndNueters">Spays and Nueters <i class="fa fa-plus float-right"></i> </a>
                        </div>
                        <div id="spaysAndNueters" class="collapse show" data-parent="#accordion">
                          <div class="card-body">
                              <p>Man Best Friends Clinic recommends neutering your new puppy or kitten. For dogs or cats, both procedures are generally performed around six months of age. These procedures will result in your pet no longer being able to reproduce. Better overall health and behavior are key reasons to consider this procedure. In general, pets that are spayed (female) or neutered (male) live longer and reduce or eliminate the risk of serious disease. Many behavioral problems are reduced or eliminated, resulting in a calmer, better socialized companion.</p>
                              <p>Both procedures are abdominal surgeries performed under general anesthesia. Surgical time can range from 30 minutes to over an hour. All spays & neuters patients are discharged from our care on the same day. Full recovery will occur over the following 7 to 10 days following restricted activity and comfortable rest. A re-check appointment is booked to make sure the incision has healed and your furry friend is fully recovered.</p>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">
                          <a class="collapsed card-link" data-toggle="collapse" href="#Vaccinations">Vaccinations <i class="fa fa-plus float-right"></i> </a>
                        </div>
                        <div id="Vaccinations" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                            <p>Vaccinations will prevent many major diseases from your pet. Boston Veterinary Clinic recommends creating and adhering to a customized vaccination schedule as part of a comprehensive wellness plan.</p>
                            <p>Vaccinations are sometimes referred to as “core”, which are recommended for all dogs & cats and “non-core”, which are recommended based on the individual pet and environmental risk factors.</p>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">
                          <a class="collapsed card-link" data-toggle="collapse" href="#Surgery">
                            Surgery <i class="fa fa-plus float-right"></i>
                          </a>
                        </div>
                        <div id="Surgery" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                            <p>At Man Best Friends Clinic, we understand the many questions and concerns you have, and we want to place your mind at ease. Whether routine or complex, surgery can be a time filled with anxiety and uncertainty. We work hard to ensure that every surgery performed in our hospital is stress-free and as positive an experience as possible.</p>
                            <p>Our surgery department offers state-of-the-art facilities, fully equipped surgical suites and a skilled staff to ensure each patient receives the best care. From pre-op pet evaluation and procedure explanation to after-care instructions and a review of any findings, we’re with you every step of the way. Our care team remains on-hand – before, during and following surgery – to monitor your companion’s well being using the most modern equipment available. This translates to your pet’s comfort and safety at all times. We offer a variety of general surgical procedures performed to the highest standards of care.</p>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">
                          <a class="collapsed card-link" data-toggle="collapse" href="#inHouseDiagnostics">
                            In-House Diagnostics <i class="fa fa-plus float-right"></i>
                          </a>
                        </div>
                        <div id="inHouseDiagnostics" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                            <p>While person-to-person conversation helps a doctor understand a patient’s concerns, when it comes to our pets, we need to let something else do the talking. This is where veterinary diagnostics come into play. If your animal friend hasn’t been acting like his or her self and you suspect an underlying illness or injury as the cause, it might be time to do a diagnostic evaluation. This allows us to learn what’s happening inside your pet’s body, so that we can take the appropriate measures.</p>
                            <p>Boston Veterinary Clinic has invested over $250 million in state-of-the art diagnostic equipment to provide the best information to our veterinarians that reduces waiting time and improves treatment outcomes.</p>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">
                          <a class="collapsed card-link" data-toggle="collapse" href="#laboratoryDiagnostics">
                            Laboratory Diagnostics <i class="fa fa-plus float-right"></i>
                          </a>
                        </div>
                        <div id="laboratoryDiagnostics" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                            <p>Man Best Friends Clinic uses IDEXX in-house analyzers and IDEXX Reference Laboratory to provide our patients with a complete diagnostic solution. As the industry-leader, IDEXX has led diagnostic innovation over its 30-year history, which means our veterinarians have access to the most accurate and comprehensive diagnostic results to better inform our diagnoses. Our equipment includes:</p>
                            <p>While person-to-person conversation helps a doctor understand a patient’s concerns, when it comes to our pets, we need to let something else do the talking. This is where veterinary diagnostics come into play. If your animal friend hasn’t been acting like his or her self and you suspect an underlying illness or injury as the cause, it might be time to do a diagnostic evaluation. This allows us to learn what’s happening inside your pet’s body, so that we can take the appropriate measures.</p>
                            <p>Boston Veterinary Clinic has invested over $250 million in state-of-the art diagnostic equipment to provide the best information to our veterinarians that reduces waiting time and improves treatment outcomes.</p>
                          </div>
                        </div>
                      </div>
                  </div>
                </div><!--services-container-->
              </div><!--col-md-4-->
            </div><!--row-->
          </div><!--container-->
        </section>
        <!----------------------------------------------->
        <!-- Contact Section -->
        <!----------------------------------------------->
        <section class="contact-section" id="contact">
          <div class="side-dog"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-7 col-md-8">
                <div class="contact-container">
                  <h1 class="h1">Contact</h1>
                  <div class="contact-cover">
                      <h3 class="contact-title">Schedule an Appointment:</h3>
                      <p class="description">Send your appointment date, and we will response Immediately via phone message. Thank you have a nice day!</p>
                      <form class="" action="" method="post" >
                        <div class="row">
                          <div class="form-group col-sm-6 col-xs-12">
                            <input type="text" name="name" value="" class="form-control " placeholder="Name">
                          </div>
                          <div class="form-group col-sm-6 col-xs-12">
                            <input type="text" name="contact-number" value="" class="form-control " placeholder="Contact Number">
                          </div>
                        </div><!--row-->
                        <div class="row">
                          <div class="form-group col-sm-6 col-xs-12">
                            <input type="email" name="email" value="" class="form-control " placeholder="Email Address">
                          </div>
                          <div class="form-group col-sm-6 col-xs-12">
                            <input type="date" name="date" value="" class="form-control " placeholder="Date">
                          </div>
                        </div><!--row-->
                        <div class="row">
                          <div class="form-group col-sm-12 col-xs-12">
                            <textarea name="name" rows="6" cols="80" class="form-control" placeholder="Note..."></textarea>
                          </div>
                        </div><!--row-->
                        <button type="button" name="button" class="btn">Send Now</button>
                      </form>
                  </div><!--contact-cover-->
                </div>
              </div>
              <div class="col-lg-5 col-md-4">
                <div class="contact-container">
                  <div class="social-links">
                    <h3 class="social-title">Follow Us:</h3>
                    <ul class="social-cover">
                      <li>
                        <a href="#">
                          <i class="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="legal text-center">
                  <p>Powered by: www.vetlify.com</p>
                </div>
              </div>
            </div>
          </div>
        </section>


      </div><!--main-content-->


    </div><!--main-body-->
  <!-- SCRIPT -->
  <script src="{{ url('js/app.js') }}"></script>
  <!-- SCRIPT -->
  </body>
</html>
