
<!DOCTYPE html>
<html lang="en">
<head>
<title>Vetlify</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" type="image/png" href="images/icons/favicon.ico" />

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/vendor/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/vendor/animate/animate.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/vendor/select2/select2.min.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/flipclock.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/css/util.css">
<link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/cs/comingsoon_06/css/main.css">

</head>
<body>
<div class="bg-img1 size1 overlay1 p-t-24" style="background-image: url('https://colorlib.com/etc/cs/comingsoon_06/images/bg01.jpg');">
<div class="flex-w flex-sb-m p-l-80 p-r-74 p-b-175 respon5">
<div class="wrappic1 m-r-30 m-t-10 m-b-10">
<a href="#" class="l1-txt3 p-b-20 respon2 respon4">Vetlify</a>
</div>

</div>
<div class="flex-w flex-sa p-r-200 respon1">
<div class="p-t-34 p-b-60 respon3">
<p class="l1-txt1 p-b-10 respon2">
Our website is
</p>
<h3 class="l1-txt2 p-b-45 respon2 respon4">
Coming Soon
</h3>
<div class="cd100"></div>
</div>
</div>
</div>

<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/jquery/jquery-3.2.1.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>

<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/bootstrap/js/popper.js" type="7da374893e70a2acfa889667-text/javascript"></script>
<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/bootstrap/js/bootstrap.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>

<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/select2/select2.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>

<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/flipclock.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>
<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/moment.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>
<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/moment-timezone.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>
<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/moment-timezone-with-data.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>
<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/countdowntime/countdowntime.js" type="7da374893e70a2acfa889667-text/javascript"></script>


<script src="https://colorlib.com/etc/cs/comingsoon_06/vendor/tilt/tilt.jquery.min.js" type="7da374893e70a2acfa889667-text/javascript"></script>


<script src="https://colorlib.com/etc/cs/comingsoon_06/js/main.js" type="7da374893e70a2acfa889667-text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="7da374893e70a2acfa889667-text/javascript"></script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="7da374893e70a2acfa889667-|49" defer=""></script></body>
</html>