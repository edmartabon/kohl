<?php

namespace Vetlify\Http\Mails;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeClient extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Request
     */
    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no_reply@vetlify.com', 'Vetlify.com')
            ->subject('Welcome to Vetlify')
            ->view('Vetlify::mails.welcome')
            ->with($this->request->all());
    }
}
