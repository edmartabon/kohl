<?php

namespace Vetlify\Http\Middlewares;

use View;
use Closure;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\Account;

class ExpiredSubscriptionMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $server = $request->route('account');
        $account = Account::where('server', $server);

        if ($account->count()) {
            $accountSelected = $account->first();

            $expiredDate = strtotime($accountSelected->expired_at);
            $dueDate = strtotime("5 days");

            if ($dueDate >= $expiredDate) {
                View::composer('*', function ($view) use ($accountSelected) {
                    $view->with($this->expiredNotification($accountSelected));
                });
            }
        }

        

        return $next($request);
    }

    private function expiredNotification($account)
    {
        return [
            'expiredNotification' => [
                'expired_at' => date_format(date_create($account->expired_at), 'F d, Y')
            ]
        ];
    }
}
