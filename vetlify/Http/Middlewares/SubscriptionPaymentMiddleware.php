<?php

namespace Vetlify\Http\Middlewares;

use View;
use Closure;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\Account;

class SubscriptionPaymentMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $server = $request->route('account');
        $account = Account::where('server', $server);

        if ($account->count()) {
            $accountSelected = $account->first();

            $expiredDate = strtotime($accountSelected->expired_at);
            $dueDate = strtotime(date('Y-m-d'));
            
            if ($dueDate >= $expiredDate) {
                return response(view('Vetlify::pages.admin.subscription.payment'));
            }
        }

        return $next($request);
    }
}
