<?php

namespace Vetlify\Http\Providers;

use View;
use Route;
use Config;
use Illuminate\Support\ServiceProvider;
use Vetlify\Http\Middlewares\LoginMiddleware;
use Vetlify\Http\Middlewares\ChangeDbMiddleware;
use Vetlify\Http\Middlewares\SecurePageMiddleware;
use Vetlify\Http\Middlewares\SubscriptionPaymentMiddleware;
use Vetlify\Http\Middlewares\ExpiredSubscriptionMiddleware;

class TemplateServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addMiddlewares();
        $this->loadViews();
        $this->loadRoute();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Add view from sprinel
     *
     * @return void
     */
    private function loadViews()
    {
        $this->loadViewsFrom($this->rootPath.'Data/views', 'Vetlify');
    }

    /**
     * Load account route
     *
     * @return void
     */
    private function loadRoute()
    {
        $namespace = '\Vetlify\\' . basename(realpath(__DIR__ . '/..'));
        $this->loadPageRoute($namespace);
        $this->loadAdminRoute($namespace);
        $this->loadAdminApiRoute($namespace);
    }

    /**
     * Load the admin route
     *
     * @param string $namespace
     * @return void
     */
    private function loadAdminRoute($namespace)
    {
        foreach (glob($this->rootPath . 'Data/routes/admin/*') as $filename) {
            Route::group([
                'domain' => '{account}.{domain}.{tld}',
                'namespace' => $namespace . '\Controllers',
                'middleware' => [
                    'web',
                    // SubscriptionPaymentMiddleware::class,
                    ExpiredSubscriptionMiddleware::class,
                    ChangeDbMiddleware::class,
                ],
            ], function () use ($filename) {
                if (!is_dir($filename)) {
                    if (basename($filename) !== 'index.php') {
                        Route::group([
                            'prefix' => Config::get('vetlify.app_route_prefix')
                        ], function () use ($filename) {
                            include $filename;
                        });
                    } else {
                        include $filename;
                    }
                }
            });
        }
    }

    /**
     * Load the api route
     *
     * @param string $namespace
     * @return void
     */
    private function loadAdminApiRoute($namespace)
    {
        foreach (glob($this->rootPath . 'Data/routes/admin/*') as $filename) {
            Route::group([
                'prefix' => 'api/accounts/{account}',
                'namespace' => $namespace . '\Controllers',
            ], function () use ($filename) {
                if (!is_dir($filename)) {
                    include $filename;
                }
            });
        }
    }

    /**
     * Load the page route
     *
     * @param string $namespace
     * @return void
     */
    private function loadPageRoute($namespace)
    {
        foreach (glob($this->rootPath . 'Data/routes/pages/*') as $filename) {
            Route::group([
                'domain' => '{account}.{domain}.{tld}',
                'namespace' => $namespace . '\Controllers',
                'middleware' => ['web'],
            ], function () use ($filename) {
                if (!is_dir($filename)) {
                    include $filename;
                }
            });
        }
    }

    /**
     * Add all middleware for admin
     *
     * @return void
     */
    private function addMiddlewares()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('LoginChecker', LoginMiddleware::class);
        $router->aliasMiddleware('SecurePage', SecurePageMiddleware::class);
    }
}
