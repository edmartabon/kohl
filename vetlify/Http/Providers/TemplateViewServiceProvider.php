<?php

namespace Vetlify\Http\Providers;

use View;
use Config;
use Sentinel;
use Illuminate\Support\ServiceProvider;

class TemplateViewServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addUser();
        $this->addLink();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Set the current user for global view
     *
     * @return void
     */
    private function addUser()
    {
        View::composer('*', function ($view) {
            $user = Sentinel::getUser();
            $view->with([
                'user' => $user,
            ]);
        });
    }

    /**
     * Add link to the global view
     *
     * @return void
     */
    private function addLink()
    {
        View::composer('*', function ($view) {
            $linkApp = Config::get('vetlify.app_route_prefix');
            $view->with('link', (object)[
                'home' => $linkApp.'/'
            ]);
        });
    }
}
