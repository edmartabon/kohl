<?php

namespace Vetlify\Http\Providers;

use Route;
use Config;
use Validator;
use Illuminate\Support\ServiceProvider;
use Vetlify\Http\Middlewares\LoginMiddleware;
use Vetlify\Http\Middlewares\ChangeDbMiddleware;
use Vetlify\Http\Middlewares\SecurePageMiddleware;

class ValidationServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addDateValidation();
        $this->addServerNameValidation();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Validate date
     *
     * @return void
     */
    private function addDateValidation()
    {
        Validator::extend('date_format', function ($attribute, $value, $formats) {
            foreach ($formats as $format) {
                $parsed = date_parse_from_format($format, $value);

                if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * Validate date
     *
     * @return void
     */
    private function addServerNameValidation()
    {
        Validator::extend('server_name', function ($attribute, $value, $formats) {
            if (is_numeric($value)) {
                return false;
            }
            return true;
        }, 'Must not numeric.');
    }
}
