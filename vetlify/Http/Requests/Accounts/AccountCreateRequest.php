<?php

namespace Vetlify\Http\Requests\Accounts;

use Illuminate\Foundation\Http\FormRequest;

class AccountCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'server' => 'required|server_name|alpha|unique:accounts,server',
            'address_one' => 'required',
            'phone_no' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email',
            'password' => 'min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        ];
    }
}
