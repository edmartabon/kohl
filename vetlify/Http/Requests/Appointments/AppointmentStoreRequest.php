<?php

namespace Vetlify\Http\Requests\Appointments;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|integer|exists:patients,id',
            'patient_id' => 'bail|required|integer|exists:patients,id',
            'veterinarian_id' => 'bail|required|integer|exists:users,id',
            'email' => 'required|email',
            // 'date_start' => 'required|date_format:m/dd/Y',
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return array_merge($this->request->all(), $this->route()->parameters());
    }
}
