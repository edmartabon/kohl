<?php

namespace Vetlify\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes|required',
            'last_name' => 'sometimes|required',
            // 'rfid' => 'sometimes|required',
            'birth_date' => 'sometimes|date_format:Y-m-d',
            'avatar' => 'bail|sometimes|required|mimes:jpeg,jpg,png,gif|max:1000'
        ];
    }
}
