<?php

namespace Vetlify\Http\Requests\Inventory;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'item_code' => 'sometimes|required',
            'retail_price' => 'required|integer',
            'category_id' => 'sometimes|required|exists:categories,id'
        ];
    }
}
