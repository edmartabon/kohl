<?php

namespace Vetlify\Http\Requests\Patients;

use Illuminate\Foundation\Http\FormRequest;

class ClientIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'sometimes|date_format:Y-m-d',
            'end' => 'sometimes|date_format:Y-m-d',
            'gender' => 'bail|sometimes|integer|required|exists:patient_genders,id',
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return array_merge($this->request->all(), $this->route()->parameters());
    }
}
