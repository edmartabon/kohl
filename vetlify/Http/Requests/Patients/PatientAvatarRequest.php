<?php

namespace Vetlify\Http\Requests\Patients;

use Illuminate\Foundation\Http\FormRequest;

class PatientAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|mimes:jpeg,jpg,png|max:1000',
        ];
    }
}
