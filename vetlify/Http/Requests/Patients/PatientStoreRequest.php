<?php

namespace Vetlify\Http\Requests\Patients;

use Illuminate\Foundation\Http\FormRequest;

class PatientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_record_id' => 'required|unique:patients,patient_record_id',
            'first_name' => 'required',
            'branch_id' => 'bail|sometimes|required|integer|exists:branches,id',
            'patient_blood_id' => 'bail|required|integer|exists:patient_bloods,id',
            'patient_gender_id' => 'bail|required|integer|exists:patient_genders,id',
            'client_id' => 'bail|integer|required|exists:clients,id',
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return array_merge($this->request->all(), $this->route()->parameters());
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'client_id' => 'Owner',
        ];
    }
}
