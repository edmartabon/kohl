<?php

namespace Vetlify\Http\Requests\Patients;

use Illuminate\Foundation\Http\FormRequest;

class PatientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_record_id' => 'bail|sometimes|required|unique:patients,patient_record_id,'.$this->route('id'),
            'first_name' => 'sometimes|required',
            // 'last_name' => 'sometimes|required',
            // 'rfid' => 'sometimes|required',
            'branch_id' => 'bail|sometimes|integer|required|exists:branches,id',
            'patient_blood_id' => 'bail|sometimes|integer|required|exists:patient_bloods,id',
            'patient_gender_id' => 'bail|sometimes|integer|required|exists:patient_genders,id',
            // 'birth_date' => 'sometimes|date_format:Y-m-d H:i:s',
            'avatar' => 'bail|sometimes|required|mimes:jpeg,jpg,png,gif|max:1000'
        ];
    }
}
